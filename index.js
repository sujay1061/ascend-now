const express = require('express')
const bodyParser = require('body-parser')
require ('dotenv').config({ path: ".env.dev"});
const cors = require ('cors');
const errorHandler = require('./src/_helpers/error-handler');
const morgan = require('morgan');
const winston = require('./src/_helpers/winston');
const { 
    student, 
    teacher,
    account,
    role,
    institute,
    course,
    topic,
    topic_assignment,
    account_active_expiration,
    course_schedule,
    batch_student,
    batch,
    news_and_event,
    class_activity,
    feedback_question,
    quiz_question,
    booked_classes
} = require('./src/routes/admin');

const { 
    teacher_class_attendance, 
    teacher_course_scheduled, 
    teacher_batch,
    teacher_topic,
    teacher_profile,
    teacher_class_activities,
    teacher_topic_feedback,
    teacher_student_point,
    teacher_booked_classes,
    teacher_student,
    teacher_course
} = require('./src/routes/teacher');

const { 
    student_course_scheduled, 
    student_profile,
    student_points,
    student_quiz,
    student_booked_classes,
    student_teacher,
    student_topic,
    student_activity
} = require('./src/routes/student');

const app =  express();

app.use(cors());
app.options('*', cors());
app.use(morgan('combined', { stream: winston.stream }));
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

// Routes
/**
 * Teacher API's are implemented 
 * /api/teacher/* are teacher API's
 */
app.use('/api/teacher/attendance', teacher_class_attendance);
app.use('/api/teacher/class', teacher_course_scheduled);
app.use('/api/teacher/batch', teacher_batch);
app.use('/api/teacher/topic', teacher_topic);
app.use('/api/teacher/profile', teacher_profile);
app.use('/api/teacher/activity', teacher_class_activities);
app.use('/api/teacher/topic_feedback', teacher_topic_feedback);
app.use('/api/teacher/points', teacher_student_point);
app.use('/api/teacher/booked_class', teacher_booked_classes);
app.use('/api/teacher/student', teacher_student);
app.use('/api/teacher/course', teacher_course);

/**
 * Student API's are implemented 
 * /api/student/* are student API's
 */
app.use('/api/student/class', student_course_scheduled);
app.use('/api/student/profile', student_profile);
app.use('/api/student/points', student_points);
app.use('/api/student/quiz', student_quiz);
app.use('/api/student/booked_class', student_booked_classes);
app.use('/api/student/teacher', student_teacher);
app.use('/api/student/topic', student_topic);
app.use('/api/student/activity', student_activity);

/**
 * Authentication API's are implemented 
 * /api/account/* are auth API's
 */
app.use('/api/account', account);

/**
 * Admin API's are implemented 
 * /api/* are admin API's
 */
app.use('/api/student', student);
app.use('/api/teacher', teacher);
app.use('/api/role', role);
app.use('/api/institute', institute);
app.use('/api/course', course);
app.use('/api/topic', topic);
app.use('/api/topic_assignment', topic_assignment);
app.use('/api/session', account_active_expiration);
app.use('/api/course-schedule', course_schedule);
app.use('/api/batch_student', batch_student);
app.use('/api/batch', batch);
app.use('/api/news', news_and_event);
app.use('/api/activity', class_activity);
app.use('/api/feedback_question', feedback_question);
app.use('/api/quiz', quiz_question);
app.use('/api/booked_class', booked_classes);

// Global Error handler
app.use(errorHandler);

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    // res.locals.error = req.app.get("env") === "development" ? err : {};
    
    // winston logging
    winston.error(`${err.message} - ${err.status || 500} - ${req.originalUrl} - ${req.body} - ${req.method} - ${req.ip}`);
  
  
    // render the error page
    res.status(err.status || 500);
    res.render("error");
});


// Start
app.listen(process.env.PORT || 3000, () =>
    console.log(`Example app listening on port ${process.env.PORT}!`),
);

