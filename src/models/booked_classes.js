const Sequelize = require('sequelize');
const { Account, Batch, Course, Topic, Teacher, Student, BookedClassTimeslot } = require('./index');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('booked_classes', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: Sequelize.STRING
    },
    is_requested: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
    is_rejected: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },
    is_accepted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },
    is_cancelled: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },
    is_completed: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },
    booked_by: {
      type: Sequelize.STRING
    },
    comment: {
      type: Sequelize.STRING,
      defaultValue: ""
    },

    attendee: {
      type: Sequelize.STRING
    },
    presenter: {
      type: Sequelize.STRING
    },
    host: {
      type: Sequelize.STRING
    },
    phone_attendee: {
      type: Sequelize.STRING
    },
    phone_presenter: {
      type: Sequelize.STRING
    },
    is_deleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },
    widget_id: {
      type: Sequelize.INTEGER
    },

    // Foreign Keys
    teacher_account_id: {
        type: Sequelize.INTEGER,
        references: Teacher,
        referencesKey: 'id'
    },
    student_account_id: {
        type: Sequelize.INTEGER,
        references: Student,
        referencesKey: 'id'
    },
    topic_id: {
        type: Sequelize.INTEGER,
        references: Topic,
        referencesKey: 'id'
    },
    created_by_account_id: {
        type: Sequelize.INTEGER,
        references: Account,
        referencesKey: 'id'
    },
    updated_by_account_id: {
        type: Sequelize.INTEGER,
        references: Account,
        referencesKey: 'id'
    },
  }, { timestamps: true });
};