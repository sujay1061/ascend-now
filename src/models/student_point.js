const Sequelize = require('sequelize');
const { Account, Student, Teacher } = require('./index');

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('student_point', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        points: {
            type: Sequelize.FLOAT,
            allowNull: false,
            require: true
        },
        title: {
            type: Sequelize.STRING,
            allowNull: true
        },
        is_deleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },

        // Foreign Keys
        student_account_id: {
            type: Sequelize.INTEGER,
            references: Student,
            referencesKey: 'id'
        },
        teacher_account_id: {
            type: Sequelize.INTEGER,
            references: Teacher,
            referencesKey: 'id'
        },
        created_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
        updated_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
    }, { timestamps: true });
};