const Sequelize = require('sequelize');
const { Account, Batch, FeedbackQuestion, Teacher } = require('./index');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('batch_feedback', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    rating: {
      type: Sequelize.INTEGER
    },
    feedback: {
        type: Sequelize.STRING,
        allowNull: false
    },
    is_deleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },

    // Foreign Keys
    account_id: {
        type: Sequelize.INTEGER,
        references: Teacher,
        referencesKey: 'id'
    },
    batch_id: {
        type: Sequelize.INTEGER,
        references: Batch,
        referencesKey: 'id'
    },
    feedback_question_id: {
        type: Sequelize.INTEGER,
        references: FeedbackQuestion,
        referencesKey: 'id'
    },
    created_by_account_id: {
        type: Sequelize.INTEGER,
        references: Account,
        referencesKey: 'id'
    },
    updated_by_account_id: {
        type: Sequelize.INTEGER,
        references: Account,
        referencesKey: 'id'
    },
  }, { timestamps: true });
};