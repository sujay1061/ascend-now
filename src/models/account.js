const Sequelize = require('sequelize');
const { Account } = require('./index');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('account', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    email: {
      type: Sequelize.STRING,
      unique:true,
      allowNull: false
    },
    secondary_email: {
      type: Sequelize.STRING,
      allowNull: true
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false
    },
    is_active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true    // changed to boolean from integer
    },
    is_deleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },
    created_by_account_id: {
        type: Sequelize.INTEGER,
        references: Account,
        referencesKey: 'id'
    },
    updated_by_account_id: {
        type: Sequelize.INTEGER,
        references: Account,
        referencesKey: 'id'
    },
  }, { timestamps: true });
};