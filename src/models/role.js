const Sequelize = require('sequelize');
const { Account } = require('./index');

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('role', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false,
            require: true,
            unique: true
        },
        is_deleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        created_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
        updated_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
    }, { timestamps: true });
};