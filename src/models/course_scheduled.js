const Sequelize = require('sequelize');
const { Account, Batch, Course, Topic, Teacher, BookedClasses } = require('./index');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('class_scheduled', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    scheduled_name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    schedule_datetime: {
      type: Sequelize.DATE,
      allowNull: false
    },
    is_deleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },
    is_completed: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },
    attendee: {
      type: Sequelize.STRING
    },
    presenter: {
      type: Sequelize.STRING
    },
    moderator: {
      type: Sequelize.STRING
    },
    host: {
      type: Sequelize.STRING
    },
    phone_attendee: {
      type: Sequelize.STRING
    },
    phone_presenter: {
      type: Sequelize.STRING
    },
    widget_id: {
      type: Sequelize.INTEGER
    },
    is_cancelled: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },
    is_quiz_enabled:{
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },
    delivery_channel:{
      type:Sequelize.STRING
    },
    time_zones: {
      type:Sequelize.STRING
    },
    date_time_schedule: {
      type:Sequelize.STRING
    },
  
      
    // Foreign Keys
    teacher_account_id: {
        type: Sequelize.INTEGER,
        references: Teacher,
        referencesKey: 'id'
    },
    batch_id: {
        type: Sequelize.INTEGER,
        references: Batch,
        referencesKey: 'id'
    },
    course_id: {
        type: Sequelize.INTEGER,
        references: Course,
        referencesKey: 'id'
    },
    topic_id: {
        type: Sequelize.INTEGER,
        references: Topic,
        referencesKey: 'id'
    },
    created_by_account_id: {
        type: Sequelize.INTEGER,
        references: Account,
        referencesKey: 'id'
    },
    updated_by_account_id: {
        type: Sequelize.INTEGER,
        references: Account,
        referencesKey: 'id'
    },
    booked_class_id:{
      type:Sequelize.INTEGER,
      references: BookedClasses,
      referencesKey: 'id'
    },
  }, { timestamps: true });
};