const Sequelize = require('sequelize');
const { Account, Student } = require('./index');

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('student_score_history', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        score: {
            type: Sequelize.FLOAT,
            allowNull: false,
            require: true
        },
        accumulated_by: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        is_deleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },

        // Foreign Keys
        account_id: {
            type: Sequelize.INTEGER,
            references: Student,
            referencesKey: 'id'
        },
        created_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
        updated_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
    }, { timestamps: true });
};