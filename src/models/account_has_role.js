const Sequelize = require('sequelize');
const { Account, Role } = require('./index');

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('account_has_role', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        is_deleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },

        // Foreign Keys
        account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
        role_id: {
            type: Sequelize.INTEGER,
            references: Role,
            referencesKey: 'id'
        },
        created_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
        updated_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },

    }, { timestamps: true });
};