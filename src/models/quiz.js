const Sequelize = require("sequelize");
const { Account, Topic } = require('./index');

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('quiz', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        is_deleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        quiz_type: {
            type: Sequelize.INTEGER,
            defaultValue: 1 // 0-> inclass 1-> postclass
        },
        is_published: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },

        // Foreign Keys
        topic_id: {
            type: Sequelize.INTEGER,
            references: Topic,
            referencesKey: 'id'
        },
        created_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
        updated_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
    }, { timestamps: true });
}