const Sequelize = require("sequelize");
const { Account, Student, Quiz } = require('./index');

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('student_quiz_answer', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        options_selected: {
            type: Sequelize.JSON,
            allowNull: false
        },
        is_submitted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        final_points: {
            type: Sequelize.FLOAT
        },
        is_deleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },

        // Foreign Keys
        quiz_id: {
            type: Sequelize.INTEGER,
            references: Quiz,
            referencesKey: 'id'
        },
        student_account_id: {
            type: Sequelize.INTEGER,
            references: Student,
            referencesKey: 'id'
        },
        created_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
        updated_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
    }, { timestamps: true });
}