const Sequelize = require("sequelize");
const { Account, Topic } = require('./index');

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('topic_assignment', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: Sequelize.STRING,
            unique: true,
            allowNull: false
        },
        description: {
            type: Sequelize.STRING
        },
        assg_type: {
            type: Sequelize.INTEGER
        },
        path: {
            type: Sequelize.STRING
        },
        url: {
            type: Sequelize.STRING
        },
        expiration_duration_in_mins: {
            type: Sequelize.INTEGER
        },
        max_marks: {
            type: Sequelize.FLOAT
        },
        is_deleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },

        // Foreign Keys
        topic_id: {
            type: Sequelize.INTEGER,
            references: Topic,
            referencesKey: 'id'
        },
        created_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
        // updated_by_account_id: {
        //     type: Sequelize.INTEGER,
        //     references: Account,
        //     referencesKey: 'id'
        // }
    }, { timestamps: true });
}