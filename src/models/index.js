const { Sequelize } = require('sequelize');

const { DB_USER, DB_PASSWORD, DB_HOST, DB_NAME, DB_PORT } = process.env;

// Option 1: Passing a connection URI
const sequelize = new Sequelize(`postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}`) 


// Import Schema Models
const Student = require('./student')(sequelize, Sequelize);
const Account = require('./account')(sequelize, Sequelize);
const AccountActiveExpiration = require('./account_active_expiration')(sequelize, Sequelize);
const AccountHasRole = require('./account_has_role')(sequelize, Sequelize);
const Institute = require('./institute')(sequelize, Sequelize);
const InstituteHasStudent = require('./institute_has_student')(sequelize, Sequelize);
const Role = require('./role')(sequelize, Sequelize);
const StudentScoreHistory = require('./student_score_history')(sequelize, Sequelize);
const Topic = require('./topic')(sequelize, Sequelize);
const Course = require('./course')(sequelize, Sequelize);
const CourseHasTeacher = require('./course_has_teacher')(sequelize, Sequelize);
const FeedbackQuestion = require('./feedback_question')(sequelize, Sequelize);
const Teacher = require('./teacher')(sequelize, Sequelize);
const TopicAssignment = require('./topic_assignment')(sequelize, Sequelize);
const Batch = require('./batch')(sequelize, Sequelize);
const CourseScheduled = require('./course_scheduled')(sequelize, Sequelize);
const BatchStudent = require('./batch_student')(sequelize, Sequelize);
const BatchStudentAssignment = require('./batch_student_assignment')(sequelize, Sequelize);
const BatchFeedback = require('./batch_feedback')(sequelize, Sequelize);
const NewsAndEvent = require('./news_and_event')(sequelize, Sequelize);
const ClassAttendance = require('./class_attendance')(sequelize, Sequelize);
const ClassActivity = require('./class_activities')(sequelize, Sequelize);
const TeacherTopicFeedback = require('./teacher_topic_feedback')(sequelize, Sequelize);
const StudentPoint = require('./student_point')(sequelize, Sequelize);
const Quiz = require('./quiz')(sequelize, Sequelize);
const QuizQuestion = require('./quiz_question')(sequelize, Sequelize);
const StudentQuizAnswer = require('./student_quiz_answer')(sequelize, Sequelize);
const BookedClasses = require('./booked_classes')(sequelize, Sequelize);
const BookedClassTimeslot = require('./booked_class_timeslot')(sequelize, Sequelize);

Account.hasOne(AccountHasRole, { foreignKey: 'account_id' });
AccountHasRole.belongsTo(Role, { foreignKey: 'role_id' });
Teacher.belongsTo(Account, { foreignKey: 'account_id' });
Student.belongsTo(Account, { foreignKey: 'account_id' });
Topic.belongsTo(Course, { foreignKey: 'course_id' });
Course.hasMany(Topic, { foreignKey: 'course_id' });
Topic.hasOne(TopicAssignment, { foreignKey: 'topic_id' });
TopicAssignment.belongsTo(Topic, { foreignKey: 'topic_id' });
Batch.hasMany(BatchStudent, { foreignKey: 'batch_id' });
BatchStudent.belongsTo(Student, { foreignKey: 'student_account_id' });
CourseScheduled.belongsTo(Course, { foreignKey: 'course_id' });
CourseScheduled.belongsTo(Batch, { foreignKey: 'batch_id' });
CourseScheduled.belongsTo(Teacher, { foreignKey: 'teacher_account_id' });
CourseScheduled.belongsTo(Topic, { foreignKey: 'topic_id' });
CourseScheduled.hasMany(ClassAttendance, { foreignKey: 'course_scheduled_id' });
ClassAttendance.belongsTo(Topic, { foreignKey: 'topic_id' });
ClassAttendance.belongsTo(CourseScheduled, { foreignKey: 'course_scheduled_id' });
ClassAttendance.belongsTo(Student, { foreignKey: 'student_id' });
Topic.hasMany(ClassAttendance, { foreignKey: 'topic_id' });
Student.hasOne(InstituteHasStudent, { foreignKey: 'student_account_id' });
InstituteHasStudent.belongsTo(Institute, { foreignKey: 'institute_id' });
CourseScheduled.belongsTo(CourseHasTeacher, { foreignKey: 'course_id'});
ClassActivity.belongsTo(Topic, { foreignKey: 'topic_id' });
AccountActiveExpiration.belongsTo(Account, { foreignKey: 'account_id' });
Topic.hasMany(TeacherTopicFeedback, { foreignKey: 'topic_id' });
Quiz.hasMany(QuizQuestion, { foreignKey: 'quiz_id' });
Quiz.belongsTo(Topic, { foreignKey: 'topic_id' });
Quiz.hasMany(StudentQuizAnswer, { foreignKey: 'quiz_id' });
BookedClasses.hasMany(BookedClassTimeslot, { foreignKey: 'booked_id' });
BookedClasses.belongsTo(Teacher, { foreignKey: 'teacher_account_id' });
BookedClasses.belongsTo(Topic, { foreignKey: 'topic_id' });
BookedClasses.belongsTo(Student, { foreignKey: 'student_account_id' });
Topic.hasMany(CourseScheduled, { foreignKey: 'topic_id' });
BookedClasses.hasOne(CourseScheduled, { foreignKey: 'booked_class_id' });

sequelize.authenticate()
  .then(() => console.log("db is connected"))
  .catch(err => console.log("errror" + err))

sequelize.sync({ force: false })
  .then(() => {
    console.log("database created");
  })

module.exports = {
  Student,
  Account,
  AccountActiveExpiration,
  AccountHasRole,
  Institute,
  InstituteHasStudent,
  Role,
  StudentScoreHistory,
  Topic,
  Course,
  CourseHasTeacher,
  FeedbackQuestion,
  Teacher,
  TopicAssignment,
  Batch,
  CourseScheduled,
  BatchStudent,
  BatchStudentAssignment,
  BatchFeedback,
  NewsAndEvent,
  ClassAttendance,
  ClassActivity,
  TeacherTopicFeedback,
  StudentPoint,
  Quiz,
  QuizQuestion,
  StudentQuizAnswer,
  BookedClasses,
  BookedClassTimeslot
}

