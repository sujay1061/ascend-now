const Sequelize = require('sequelize');
const { Account } = require('./index');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('student', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: Sequelize.STRING
    },
    code: {
      type: Sequelize.STRING,  // check for format
      unique: true
    },
    reg_number: {
      type: Sequelize.STRING
    },
    reg_type: {
      type: Sequelize.INTEGER
    },
    primary_email: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false
    },
    secondary_email: {
      type: Sequelize.STRING,
      allowNull: true
    },
    primary_number: {
      type: Sequelize.STRING,
      allowNull: false
    },
    secondary_number: {
      type: Sequelize.STRING,
      allowNull: true
    },
    lattitude: {
      type: Sequelize.FLOAT,
      allowNull: true
    },
    longitude: {
      type: Sequelize.FLOAT,
      allowNull: true
    },
    location: {
      type: Sequelize.JSON
    },
    Score: {
      type: Sequelize.FLOAT
    },
    grade: {
      type: Sequelize.STRING
    },
    imageUrl: {
      type: Sequelize.STRING
    },
    delivery_channel: {
      type: Sequelize.STRING,
      defaultValue: 'physical'
    },
    is_deleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },

    // Foreign Keys
    account_id: {
      type: Sequelize.INTEGER,
      references: Account,
      referencesKey: 'id'
    },
    created_by_account_id: {
      type: Sequelize.INTEGER,
      references: Account,
      referencesKey: 'id'
    },
    updated_by_account_id: {
      type: Sequelize.INTEGER,
      references: Account,
      referencesKey: 'id'
    },

  }, { timestamps: true });  // check for UTC timezone -- confirmed
};