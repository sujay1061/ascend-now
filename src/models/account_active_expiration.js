const Sequelize = require('sequelize');
const { Account, Role } = require('./index');

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('account_active_expiration', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        link: {
            type: Sequelize.STRING
        },
        duration_in_mins: {
            type: Sequelize.INTEGER
        },
        is_active: {
            type: Sequelize.BOOLEAN,
            defaultValue: true   // changed from INTEGER to BOOLEAN
        },
        no_tried: {
            type: Sequelize.INTEGER
        },
        last_try_date: {
            type: Sequelize.DATE
        },
        is_deleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },

        // Foreign Keys
        account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        }

    }, { timestamps: true });
};