const Sequelize = require("sequelize");
const { Account, Quiz } = require('./index');

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('quiz_question', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        question: {
            type: Sequelize.STRING,
            unique: true,
            allowNull: false
        },
        type: {
            type: Sequelize.INTEGER,
            defaultValue: 1
        },
        attachment_type: {
            type: Sequelize.STRING,
            defaultValue: 'none'
        },
        points: {
            type: Sequelize.FLOAT,
            allowNull: false
        },
        url: {
            type: Sequelize.STRING
        },
        options: {
            type: Sequelize.JSON,
            allowNull: false
        },
        answer: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        is_deleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },

        // Foreign Keys
        quiz_id: {
            type: Sequelize.INTEGER,
            references: Quiz,
            referencesKey: 'id'
        },
        created_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
        updated_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
    }, { timestamps: true });
}