const Sequelize = require('sequelize');
const { Account, Batch, Student, Course } = require('./index');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('batch_student', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    marks: {
      type: Sequelize.FLOAT,
      allowNull: true
    },
    marks_date: {
      type: Sequelize.DATE,
      allowNull: true
    },
    is_deleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },

    // Foreign Keys
    student_account_id: { 
        type: Sequelize.INTEGER,
        references: Student,
        referencesKey: 'id'
    },
    batch_id: { 
        type: Sequelize.INTEGER,
        references: Batch,
        referencesKey: 'id'
    },
    created_by_account_id: {
        type: Sequelize.INTEGER,
        references: Account,
        referencesKey: 'id'
    },
    updated_by_account_id: {
        type: Sequelize.INTEGER,
        references: Account,
        referencesKey: 'id'
    },
    course_id: { 
      type: Sequelize.INTEGER,
      references: Course,
      referencesKey: 'id'
  },
  }, { timestamps: true });
};