const Sequelize = require('sequelize');
const { Account, CourseScheduled, Topic } = require('./index');

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('class_activity', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false,
            required: true
        },
        link: {
            type: Sequelize.STRING
        },
        type: {
            type: Sequelize.STRING,
            defaultValue: "student"    // student and teacher are 2 types
        },
        subtype: {
            type: Sequelize.STRING
        },
        order: {
            type: Sequelize.INTEGER,
            defaultValue: 1
        },
        is_deleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },

        // Foreign Keys
        topic_id: {
            type: Sequelize.INTEGER,
            references: Topic,
            referencesKey: 'id'
        },
        course_scheduled_id: {
            type: Sequelize.INTEGER,
            references: CourseScheduled,
            referencesKey: 'id'
        },
        created_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
        updated_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },

    }, { timestamps: true });
};