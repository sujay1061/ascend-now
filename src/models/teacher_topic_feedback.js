const Sequelize = require('sequelize');
const { Account, Teacher, Student, TopicAssignment, Topic } = require('./index');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('teacher_topic_feedback', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    engagementInClass: {
      type: Sequelize.FLOAT
    },
    comment: {
        type: Sequelize.STRING,
        allowNull: false
    },
    understanding: {
        type: Sequelize.FLOAT    
    },
    focus: {
        type: Sequelize.FLOAT    
    },
    is_deleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },

    // Foreign Keys
    topic_id: {
        type: Sequelize.INTEGER,
        references: Topic,
        referencesKey: 'id'
    },
    student_account_id: {
        type: Sequelize.INTEGER,
        references: Student,
        referencesKey: 'id'
    },
    teacher_account_id: {
        type: Sequelize.INTEGER,
        references: Teacher,
        referencesKey: 'id'
    },
    created_by_account_id: {
        type: Sequelize.INTEGER,
        references: Account,
        referencesKey: 'id'
    },
    updated_by_account_id: {
        type: Sequelize.INTEGER,
        references: Account,
        referencesKey: 'id'
    },
  }, { timestamps: true });
};