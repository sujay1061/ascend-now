const Sequelize = require("sequelize");
const { Account, Course } = require('./index');

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('topic', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: Sequelize.STRING,
            unique: true,
            allowNull: false
        },
        code: {
            type: Sequelize.STRING,
            allowNull: true
        },
        description: {
            type: Sequelize.STRING
        },
        duration_in_mins: {
            type: Sequelize.INTEGER
        },
        sequence_no: {
            type: Sequelize.INTEGER
        },
        topic_attachment: {
            type: Sequelize.JSON
        },
        topic_script: {
            type: Sequelize.STRING
        },
        is_deleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        topic_difficulty: {
            type: Sequelize.STRING,
            defaultValue: 'hard'
        },
        topic_covered: {
            type: Sequelize.JSON
        },

        // Foreign Keys
        course_id: {
            type: Sequelize.INTEGER,
            references: Course,
            referencesKey: 'id'
        },
        created_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
        updated_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        }
    }, { timestamps: true });
}