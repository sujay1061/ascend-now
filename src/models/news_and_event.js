const Sequelize = require("sequelize");
const { Account } = require('./index');

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('news_and_event', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        title: {
            type: Sequelize.STRING,
            allowNull: false,
            required: true
        },
        description: {
            type: Sequelize.STRING
        },
        schedule_datetime: {
            type: Sequelize.DATE
        },
        is_deleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },

        // Foreign Keys
        created_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
        updated_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
    }, { timestamps: true });
}