const Sequelize = require('sequelize');
const { Account, Batch, Course, Topic, Teacher, Student, BookedClasses } = require('./index');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('booked_class_timeslot', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    slot_no: {
        type: Sequelize.INTEGER,
        allowNull: false,
        required: true
    },
    start_date: {
        type: Sequelize.DATE,
        allowNull: false,
        required: true
    },
    end_date: {
        type: Sequelize.DATE
    },
    hours: {
        type: Sequelize.FLOAT
    },
    is_confirmed: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    date_time_booked: {
        type:Sequelize.STRING
      },


    // Foreign Keys
    booked_id: {
        type: Sequelize.INTEGER,
        references: BookedClasses,
        referencesKey: 'id'
    },
    created_by_account_id: {
        type: Sequelize.INTEGER,
        references: Account,
        referencesKey: 'id'
    },
    updated_by_account_id: {
        type: Sequelize.INTEGER,
        references: Account,
        referencesKey: 'id'
    },
  }, { timestamps: true });
};