const Sequelize = require('sequelize');
const { Account, Role, Course, Teacher } = require('./index');

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('course_has_teacher', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        status: {
            type: Sequelize.INTEGER,
            defaultValue: 1  // 0 -> inactive 1-> active
        },
        is_deleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        teach_from: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.fn('now')
        },
        teach_end: {
            type: Sequelize.DATE
        },
        other_info: {
            type: Sequelize.JSON
        },

        // Foreign Keys
        account_id: {
            type: Sequelize.INTEGER,
            references: Teacher,
            referencesKey: 'id'
        },
        course_id: {
            type: Sequelize.INTEGER,
            references: Course,
            referencesKey: 'id'
        },
        created_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
        updated_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },

    }, { timestamps: true });
};