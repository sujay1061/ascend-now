const Sequelize = require('sequelize');
const { Account, Topic, Teacher, Student } = require('./index');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('batch_student_assignment', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    description: {
      type: Sequelize.STRING,
      allowNull: false
    },
    attachment: {
      type: Sequelize.JSON,
      allowNull: false
    },
    is_deleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },

    // Foreign Keys
    topic_assignment_id: {
        type: Sequelize.INTEGER,
        references: Topic,
        referencesKey: 'id'
    },
    teacher_account_id: {
        type: Sequelize.INTEGER,
        references: Teacher,
        referencesKey: 'id'
    },
    batch_student_id: {
        type: Sequelize.INTEGER,
        references: Student,
        referencesKey: 'id'
    },
    created_by_account_id: {
        type: Sequelize.INTEGER,
        references: Account,
        referencesKey: 'id'
    },
    updated_by_account_id: {
        type: Sequelize.INTEGER,
        references: Account,
        referencesKey: 'id'
    },
  }, { timestamps: true });
};