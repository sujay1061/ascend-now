const Sequelize = require('sequelize');
const { Institute, Account, Student } = require('./index');

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('institute_has_student', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        status: {
            type: Sequelize.STRING,
            allowNull: false,
            defaultValue: 'active'
        },
        date_start: {
            type: Sequelize.DATE,
            allowNull: true,
            defaultValue: Sequelize.fn('now')
        },
        date_end: {
            type: Sequelize.DATE,
            allowNull: true
        },
        is_deleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },

        // Foreign Keys
        institute_id: {
            type: Sequelize.INTEGER,
            references: Institute,
            referencesKey: 'id'
        },
        student_account_id: {
            type: Sequelize.INTEGER,
            references: Student,
            referencesKey: 'id'
        },
        created_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
        updated_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
    }, { timestamps: true });
};