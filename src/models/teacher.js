const Sequelize = require('sequelize');
const { Account, Course } = require('./index');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('teacher', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    code: {
      type: Sequelize.STRING,
      allowNull: false
    },
    primary_email: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false
    },
    secondary_email: {
      type: Sequelize.STRING,
      allowNull: true,
      unique: false,
      notEmpty: true,
    },
    primary_number: {
      type: Sequelize.STRING,
      allowNull: false
    },
    secondary_number: {
      type: Sequelize.STRING,
      allowNull: true
    },
    lattitude: {
      type: Sequelize.FLOAT,
      allowNull: true
    },
    longitude: {
      type: Sequelize.FLOAT,
      allowNull: true
    },
    location: {
      type: Sequelize.JSON,
      allowNull: true
    },
    imageUrl: {
      type: Sequelize.STRING
    },
    description: {
      type: Sequelize.STRING
    },
    delivery_channel: {
      type: Sequelize.STRING,
      defaultValue: 'physical'
    },
    is_deleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },

    // Foreign Keys
    account_id: {
      type: Sequelize.INTEGER,
      references: Account,
      referencesKey: 'id'
    },
    course_id: {
      type: Sequelize.INTEGER,
      references: Course,
      referencesKey: 'id'
    },
    created_by_account_id: {
      type: Sequelize.INTEGER,
      references: Account,
      referencesKey: 'id'
    },
    updated_by_account_id: {
      type: Sequelize.INTEGER,
      references: Account,
      referencesKey: 'id'
    },

  }, { timestamps: true });
};