const Sequelize = require('sequelize');
const { Account, Role, Course, CourseScheduled, Topic, Student } = require('./index');

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('class_attendance', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        attended: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        url: {
            type: Sequelize.STRING
        },
        marks: {
            type: Sequelize.FLOAT
        },
        assignment_feedback: {
            type: Sequelize.STRING
        },
        comments:{
            type: Sequelize.STRING
        },

        // Foreign Keys
        student_id: {
            type: Sequelize.INTEGER,
            references: Student,
            referencesKey: 'id'
        },
        topic_id: {
            type: Sequelize.INTEGER,
            references: Topic,
            referencesKey: 'id'
        },
        course_scheduled_id: {
            type: Sequelize.INTEGER,
            references: CourseScheduled,
            referencesKey: 'id'
        },
        created_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
        updated_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },

    }, { timestamps: true });
};