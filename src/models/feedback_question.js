const Sequelize = require("sequelize");
const { Account } = require('./index');

module.exports = (sequelize, DataTypes) => {
    return sequelize.define('feedback_question', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        questions: {
            type: Sequelize.STRING,
            unique: true,
            allowNull: false
        },
        type: {
            type: Sequelize.INTEGER
        },
        options: {
            type: Sequelize.JSON // need to check
        },
        is_deleted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },

        // Foreign Keys
        created_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
        updated_by_account_id: {
            type: Sequelize.INTEGER,
            references: Account,
            referencesKey: 'id'
        },
    }, { timestamps: true });
}