let ejs = require("ejs");
let pdf = require("html-pdf");
let path = require("path");
const { studentFeedback } = require("../notification/job-processors/teacher/teacher.processor");
exports.generatePdf = (studentPerformance,student,topic) =>{

    ejs.renderFile(path.join(__dirname, "pdfContent.ejs"), {studentPerformance: studentPerformance}, (err, data) => {
        if (err) {
            //   res.send(err);
              throw err
        } else {
            let options = {
                "height": "11.25in",
                "width": "8.5in",
                "header": {
                    "height": "20mm"
                },
                "footer": {
                    "height": "20mm",
                },
            };
          pdf.create(data, options).toFile(`report_${studentPerformance.student.name}_${studentPerformance.topic.id}.pdf`, function (err, data) {
                // /pdf/
                if (err) {
                    // res.send(err);
                    throw err
                } else {
                    console.log("created the files")
                    studentFeedback(studentPerformance.student,studentPerformance.teacher,studentPerformance.topic)
                    // res.send("File created successfully");
                }
            });
          
        }
    }); 
}

