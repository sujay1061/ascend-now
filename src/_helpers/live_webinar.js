
const request = require('request');
const { getFormattedDateTime } = require("../common-middleware/commons");

exports.getLiveWebinarAccessTokens = () => {
    const accessOptions = {
        'method': 'POST',
        'url': `${process.env.LIVE_WEBINAR_BASE_URL}/oauth/access_token`,
        'headers': {
          'Accept': 'application/vnd.archiebot.v1+json'
        },
        formData: {
          'grant_type': 'password',
          'client_id':  process.env.LIVE_WEBINAR_CLIENT_ID,
          'client_secret': process.env.LIVE_WEBINAR_CLIENT_SECRET,
          'username': process.env.LIVE_WEBINAR_USERNAME,
          'password': process.env.LIVE_WEBINAR_PASSWORD
        }
      };
    return new Promise((resolve, reject) => {
        request(accessOptions, function(error, response) {
            if(error) reject(error);
            var responseObj = JSON.parse(response.body);
            resolve(responseObj);
        })
    })
}

exports.createLiveWebinarClass = (responseObj, courseScheduled) => {
    var options = {
        'method': 'POST',
        'url': `${process.env.LIVE_WEBINAR_BASE_URL}/widgets`,
        'headers': {
          'Accept': 'application/vnd.archiebot.v1+json',
          'Authorization': "Bearer "+ responseObj.accessToken.access_token
        },
        formData: {
          'name': courseScheduled.scheduled_name,
          'password': '',
          'agenda': '',
          'custom_name;': courseScheduled.scheduled_name,
          'type': 'scheduled',
          'status': '',
          'start_date': getFormattedDateTime(courseScheduled.schedule_datetime),
          'duration': courseScheduled.topic.duration_in_mins,
          'timeZone': 'UTC',
          'strict_event': '',
          'manual_confirm_registrants': '',
          'autostart[thankyou_page_url]': 'https://ascendnow-user.pacewisdom.in/Dashboard',
          'autostart[initial_template_id]': 0
        }
      };

      return new Promise((resolve, reject) => {
        request(options, async function (error, response) {
            if (error) reject(error);
            const responseBody = JSON.parse(response.body);
            resolve(responseBody);
          });
      });
}

exports.createLiveWebinarBookedClass = (responseObj, bookedClass) => {
    var options = {
        'method': 'POST',
        'url': `${process.env.LIVE_WEBINAR_BASE_URL}/widgets`,
        'headers': {
          'Accept': 'application/vnd.archiebot.v1+json',
          'Authorization': "Bearer "+ responseObj.accessToken.access_token
        },
        formData: {
          'name': bookedClass.name,
          'password': '',
          'agenda': '',
          'custom_name;': bookedClass.name,
          'type': 'scheduled',
          'status': '',
          'start_date': getFormattedDateTime(bookedClass.start_date),
          'duration':bookedClass.duration_in_mins,
          'timeZone': 'UTC',
          'strict_event': '',
          'manual_confirm_registrants': '',
          'autostart[thankyou_page_url]': 'https://ascendnow-user.pacewisdom.in/Dashboard',
          'autostart[initial_template_id]': 0
        }
      };

      return new Promise((resolve, reject) => {
        request(options, async function (error, response) {
            if (error) reject(error);
            const responseBody = JSON.parse(response.body);
            resolve(responseBody);
          });
      });
}

exports.getLiveWebinarWidgerRecording = (responseObj, widget_id) => {
  var options = {
    'method': 'GET',
    'url': `${process.env.LIVE_WEBINAR_BASE_URL}/widgets/${widget_id}/recordings`,
    'headers': {
      'Accept': 'application/vnd.archiebot.v1+json',
      'Authorization': "Bearer "+ responseObj.accessToken.access_token
    }
  };

  return new Promise((resolve, reject) => {
    request(options, async function (error, response) {
        if (error) reject(error);
        const responseBody = JSON.parse(response.body);
        resolve(responseBody);
      });
  });
}

exports.getAttendeeDetails = (responseObj, widget_id) => {
  console.log(widget_id)
var options = {
  'method': 'GET',
  'url': `${process.env.LIVE_WEBINAR_BASE_URL}/widgets/${widget_id}/participants`,
  'headers': {
    'Accept': 'application/vnd.archiebot.v1+json',
    'Authorization': "Bearer "+ responseObj.accessToken.access_token
  }
};
return new Promise((resolve, reject) => {
  request(options, async function (error, response) {
      if (error) reject(error);
      const responseBody = JSON.parse(response.body);
      resolve(responseBody);
    });
});
}