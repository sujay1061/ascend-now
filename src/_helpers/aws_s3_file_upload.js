const aws = require('aws-sdk'),
    multer = require('multer'),
    multerS3 = require('multer-s3');

aws.config.update({
  secretAccessKey: process.env.AWS_S3_SECRET_KEY,
  accessKeyId: process.env.AWS_S3_ACCESS_KEY,
  region: process.env.AWS_S3_REGION
});

var s3 = new aws.S3();

var storage = multerS3({
  s3: s3,
  bucket: process.env.AWS_S3_BUCKET_NAME,
  contentLength: 500000000,
  metadata: function (req, file, cb) {
    cb(null, {fieldName: `${Date.now()}`});
  },
  key: function (req, file, cb) {
      cb(null, `${Date.now()}`);
  }
})

module.exports = upload = multer({ storage: storage });

module.exports.getSignedUrlFromS3 = async (url) => {
  if(url) {
    let splittedInfo = url.split('/');
    let urlKey = splittedInfo[splittedInfo.length-1];
    if(urlKey) {
      return await s3.getSignedUrl('getObject', {
        Bucket: process.env.AWS_S3_BUCKET_NAME,
        Key: urlKey,
        Expires: 86400
    })
    } else {
      return ""
    }
  } else {
    return "";
  }
}

module.exports.getMultipleSignedUrlFromS3 = async (urls) => {
  const newUrls = []
  urls.forEach(async (url) => {
    if(url) {
      let splittedInfo = url.split('/');
      let urlKey = splittedInfo[splittedInfo.length-1];
      if(urlKey) {
        let signedUrl = await s3.getSignedUrl('getObject', {
          Bucket: process.env.AWS_S3_BUCKET_NAME,
          Key: urlKey,
          Expires: 86400
      })
        await newUrls.push(signedUrl);
      } else {
        await newUrls.push("")
      }
    } else {
      await newUrls.push("")
    }
  });
  return newUrls;
}