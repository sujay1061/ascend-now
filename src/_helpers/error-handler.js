const apiResponse = require("./api-response");

function errorHandler(err, req, res, next) {
  if (typeof err === "string") {
    // custom application error
    return res.status(400).json(
      apiResponse({
        data: [],
        status: "BAD",
        errors: [],
        message: err,
      })
    )
  }

  if (err.name === "ValidationError") {
    // postgres validation error
    return res.status(400).json(
      apiResponse({
        data: [],
        status: "BAD",
        errors: [],
        message: err,
      })
    )
  }

  if (err.name === "UnauthorizedError") {
    // jwt authentication error
    return res.status(401).json(
      apiResponse({
        data: [],
        status: "BAD",
        errors: [],
        message: "Invalid Token"
      })
    )
  }

  if(err.name === "SequelizeUniqueConstraintError") {
    return res.status(400).json(
      apiResponse({
        data: [],
        status: "BAD",
        errors: [],
        message: err.errors[0] ? err.errors[0].message : 'Unique constraint Error',
      })
    )
  }

  // default to 500 server error
  return res.status(500).json(
    apiResponse({
      data: [],
      status: "BAD",
      errors: [],
      message: err.message
    })
  )
}

module.exports = errorHandler