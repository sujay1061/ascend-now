const apiResponse = require("../../_helpers/api-response");
const { account } = require('../../services/admin')

const {
  _loginToAccount,
  _createNewAccount,
  _logoutFromAccount,
  _ResetPassword,
  _forgotPassword,
  _forgotResetPassword,
  _refreshToken,
  _TestloginToAccount
} = account


function loginToAccount(req, res, next) {
  _loginToAccount(req.body)
    .then((user) =>
      res.json(
        apiResponse({
          data: user,
          status: "OK",
          message: "logged in succesffully",
        })
      )
    )
    .catch((err) => next(err));
}

function logoutFromAccount(req, res, next) {
  _logoutFromAccount(req)
    .then((user) =>
      res.json(
        apiResponse({
          data: user,
          status: "OK",
          message: "logged out succesffully",
        })
      )
    )
    .catch((err) => next(err));
}

function createNewAccount(req, res, next) {
  // req.body.created_by_account_id = req.user.account_id;
  _createNewAccount(req.body)
    .then((userAccount) =>
      res.json(
        apiResponse({
          data: userAccount,
          message: "User Created Successfully",
          status: "OK",
        })
      )
    )
    .catch((err) => next(err));
}

function resetPassword(req, res, next)
{
  _ResetPassword(req)
  .then((userAccount) => 
    res.json(
      apiResponse({
        data:userAccount,
        message: "User Password updated Successfully",
        status: "OK"
      })
    )
  )
  .catch((err) => next(err));
  
}




function forgotPassword(req, res, next)
{

  _forgotPassword(req)
  .then((userAccount) => 
    res.json(
      apiResponse({
        data:userAccount,
        message: "User Password updated Successfully",
        status: "OK"
      })
    )
  )
  .catch((err) => next(err));
  
}

function forgotResetPassword(req, res, next)
{

  _forgotResetPassword(req)
  .then((userAccount) => 
    res.json(
      apiResponse({
        data:userAccount,
        message: "User Password updated Successfully",
        status: "OK"
      })
    )
  )
  .catch((err) => next(err));
  
}

function refreshToken(req, res, next) {
  _refreshToken(req, res)
  .then((userAccount) => 
    res.json(
      apiResponse({
        data:userAccount,
        message: "Access Token refreshed",
        status: "OK"
      })
    )
  )
  .catch((err) => next(err));
}

function TestloginToAccount(req, res, next) {
  _TestloginToAccount(req.body)
    .then((user) =>
      res.json(
        apiResponse({
          data: user,
          status: "OK",
          message: "logged in succesffully",
        })
      )
    )
    .catch((err) => next(err));
}

module.exports = {
  loginToAccount,
  createNewAccount,
  logoutFromAccount,
  resetPassword,
  forgotPassword,
  forgotResetPassword,
  refreshToken,
  TestloginToAccount
};