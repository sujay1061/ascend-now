const apiResponse = require("../../_helpers/api-response");
const { course_schedule } = require('../../services/admin');

const {
  _getAllCourseSchedules,
  _getCourseScheduleById,
  _createNewCourseSchedule,
  _updateCourseScheduleById,
  _deleteCourseScheduleById,
  _updateCourseCompletedById,
  _getTeacherScheduleForAWeek,
  _monthlyCalendar
} = course_schedule

getAllCourseSchedules = (req, res, next) => {
  _getAllCourseSchedules(req)
    .then((courses) =>
      res.json(
        apiResponse({
          data: courses,
          status: "OK",
          message: "Class Schedules fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getCourseScheduleById = (req, res, next) => {
  _getCourseScheduleById(req.params)
    .then((course) =>
      res.json(
        apiResponse({
          data: course,
          status: "OK",
          message: "Class fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

createNewCourseSchedule = (req, res, next) => {
  req.body.created_by_account_id = req.user.account_id;
  req.body.scheduled_by_account_id = req.user.account_id;
  _createNewCourseSchedule(req)
    .then((course) =>
      res.json(
        apiResponse({
          data: course,
          status: "OK",
          message: "Class Scheduled succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

updateCourseScheduleById = (req, res, next) => {
  req.body.updated_by_account_id = req.user.account_id;
  _updateCourseScheduleById(req)
    .then((course) =>
      res.json(
        apiResponse({
          data: course,
          status: "OK",
          message: "Class updated succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

updateCourseCompletedById = (req, res, next) => {
  _updateCourseCompletedById(req.params)
    .then((course) =>
      res.json(
        apiResponse({
          data: course,
          status: "OK",
          message: "Class updated succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

monthlyCalendar = (req, res, next) => {
  _monthlyCalendar(req)
    .then((courses) =>
      res.json(
        apiResponse({
          data: courses,
          status: "OK",
          message: "Class deleted succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

deleteCourseScheduleById = (req, res, next) => {
  _deleteCourseScheduleById(req.params)
    .then((course) =>
      res.json(
        apiResponse({
          data: course,
          status: "OK",
          message: "Class deleted succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getTeacherScheduleForAWeek = (req,res,next) => {
  _getTeacherScheduleForAWeek(req)
  .then((teacherClasses) =>
    res.json(
      apiResponse({
        data: teacherClasses,
        status: "OK",
        message: "Teacher Classes fetched succesfully",
      })
    )
  )
  .catch((err) => next(err));
}

module.exports = {
  getAllCourseSchedules,
  getCourseScheduleById,
  createNewCourseSchedule,
  updateCourseScheduleById,
  deleteCourseScheduleById,
  updateCourseCompletedById,
  getTeacherScheduleForAWeek,
  monthlyCalendar
}