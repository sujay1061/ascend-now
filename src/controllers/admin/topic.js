const apiResponse = require("../../_helpers/api-response");
const { 
    _getAllTopics, 
    _getTopicById, 
    _createNewTopic, 
    _updateTopicById, 
    _deleteTopicById,
    _getAllTopicsByCourseId,
    _downloadMaterial,
    _getSignedUrl
} = require('../../services/admin/topic.service');

getAllTopics = (req, res, next) => {
    _getAllTopics(req)
    .then((Topics) =>
      res.json(
        apiResponse({
          data: Topics,
          status: "OK",
          message: "Topics fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getTopicById = (req, res, next) => {
    _getTopicById(req.params.id)
    .then((Topic) =>
      res.json(
        apiResponse({
          data: Topic,
          status: "OK",
          message: "Topic fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

createNewTopic = (req, res, next) => {
  const { topic_attachment, topic_script } = req.files;
  req.body.topic_attachment = (req.files && topic_attachment) ? topic_attachment[0].location : req.body.topic_attachment;
  req.body.topic_script = (req.files && topic_script) ? topic_script[0].location : req.body.topic_script;
  req.body.created_by_account_id = req.user.account_id;
    _createNewTopic(req.body)
    .then((Topic) =>
      res.json(
        apiResponse({
          data: Topic,
          status: "OK",
          message: "Topic created succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

updateTopicById = (req, res, next) => {
    _updateTopicById(req)
    .then((Topic) =>
      res.json(
        apiResponse({
          data: Topic,
          status: "OK",
          message: "Topic updated succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

deleteTopicById = (req, res, next) => {
    _deleteTopicById(req.params.id)
    .then((Topic) =>
        res.json(
        apiResponse({
            data: Topic,
            status: "OK",
            message: "Topic deleted succesfully",
        })
        )
    )
    .catch((err) => next(err));
}

getAllTopicsByCourseId = (req, res, next) => {
  _getAllTopicsByCourseId(req.params.id)
  .then((Topic) =>
        res.json(
        apiResponse({
            data: Topic,
            status: "OK",
            message: "Topics fetched succesfully",
        })
        )
    )
    .catch((err) => next(err));
}

downloadMaterial = (req, res, next) => {
  _downloadMaterial(req.body.url)
  .then((SignedUrl) =>
        res.json(
        apiResponse({
            data: SignedUrl,
            status: "OK",
            message: "Signed Url fetched succesfully",
        })
        )
    )
    .catch((err) => next(err));
}

getSignedUrl = (req, res, next) => {
  _getSignedUrl(req.body.url)
  .then((SignedUrl) =>
        res.json(
        apiResponse({
            data: SignedUrl,
            status: "OK",
            message: "Signed Url fetched succesfully",
        })
        )
    )
    .catch((err) => next(err));
}

module.exports = {
    getAllTopics,
    getTopicById,
    createNewTopic,
    updateTopicById,
    deleteTopicById,
    getAllTopicsByCourseId,
    downloadMaterial,
    getSignedUrl
}