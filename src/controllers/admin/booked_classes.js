const apiResponse = require("../../_helpers/api-response");
const { booked_classes } = require('../../services/admin');

const {
    _bookClassWithStudentTeacher,
    _cancelClassWithStudentTeacher,
    _getAllTeachersByDeliveryChannel,
    _getAllStudentsByDeliveryChannel,
    _getAllSchedules
} = booked_classes;

function bookClassWithStudentTeacher(req, res, next) {
    _bookClassWithStudentTeacher(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function cancelClassWithStudentTeacher(req, res, next) {
    _cancelClassWithStudentTeacher(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function getAllTeachersByDeliveryChannel(req, res, next) {
    _getAllTeachersByDeliveryChannel(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function getAllStudentsByDeliveryChannel(req, res, next) {
    _getAllStudentsByDeliveryChannel(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function getAllSchedules(req, res, next) {
    _getAllSchedules(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

module.exports = {
    bookClassWithStudentTeacher,
    cancelClassWithStudentTeacher,
    getAllTeachersByDeliveryChannel,
    getAllStudentsByDeliveryChannel,
    getAllSchedules
};