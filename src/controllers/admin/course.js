const apiResponse = require("../../_helpers/api-response");
const { 
    _getAllCourses,
    _getCourseById,
    _createNewCourse,
    _updateCourseById,
    _deleteCourseById
} = require('../../services/admin/course.service');

getAllCourses = (req, res, next) => {
    _getAllCourses(req)
    .then((courses) =>
      res.json(
        apiResponse({
          data: courses,
          status: "OK",
          message: "Courses fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getCourseById = (req, res, next) => {
    _getCourseById(req.params.id)
    .then((course) =>
      res.json(
        apiResponse({
          data: course,
          status: "OK",
          message: "Course fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

createNewCourse=(req,res,next) => {
    req.body.created_by_account_id = req.user.account_id;
    _createNewCourse(req.body)
    .then((course) =>
      res.json(
        apiResponse({
          data: course,
          status: "OK",
          message: "Course Added succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

updateCourseById = (req, res, next) => {
  req.body.updated_by_account_id = req.user.account_id;
    _updateCourseById(req.params.id,req.body)
    .then((course) =>
      res.json(
        apiResponse({
          data: course,
          status: "OK",
          message: "Course updated succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

deleteCourseById = (req,res,next) => {

    _deleteCourseById(req.params.id)
    .then((course) =>
    res.json(
      apiResponse({
        data: course,
        status: "OK",
        message: "course Deleted succesfully",
      })
    )
  )
  .catch((err) => next(err));
  
  }
  


module.exports = {
    getAllCourses,
    getCourseById,
    createNewCourse,
    updateCourseById,
    deleteCourseById
    
}