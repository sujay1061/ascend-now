const apiResponse = require("../../_helpers/api-response");

const {
  _getAllTopicAssignment,
  _getTopicAssignmentById,
  _createNewTopicAssignment,
  _updateTopicAssignmentById,
  _deleteTopicAssignmentById
} = require('../../services/admin/topic_assignment.service');

getAllTopicAssignment = (req, res, next) => {
  _getAllTopicAssignment(req)
    .then((topic_assignment) =>
      res.json(
        apiResponse({
          data: topic_assignment,
          status: "OK",
          message: "topic assignments fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getTopicAssignmentById = (req, res, next) => {

  _getTopicAssignmentById(req.params.id)
    .then((topic_assignment) => {
      res.json(
        apiResponse({
          data: topic_assignment,
          status: "OK",
          messag: "topic assignment fetched succesfully"
        })
      )
    })
}

createNewTopicAssignment = async (req, res, next) => {
  const file = req.file
  req.body.url = file ? file.location : req.body.url;
  req.body.created_by_account_id = req.user.account_id;
  _createNewTopicAssignment(req.body)
    .then((TopicAssignmet) =>
      res.json(
        apiResponse({
          data: TopicAssignmet,
          status: "OK",
          message: "Topic Assignmet created succesfully",
        })
      )
    )
    .catch((err) => next(err));

}

updateTopicAssignmentById = (req, res, next) => {
  const file = req.file;
  req.body.url = (file && file.location) ? file.location : req.body.url;
  req.body.updated_by_account_id = req.user.account_id;
  _updateTopicAssignmentById(req.body, req.params.id)
    .then((TopicAssignmet) =>
      res.json(
        apiResponse({
          data: TopicAssignmet,
          status: "OK",
          message: "TopicAssignmet updated succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

deleteTopicAssignmentById = (req, res, next) => {
  console.log(req.params.id)
  _deleteTopicAssignmentById(req.params.id)
    .then((TopicAssignmet) =>
      res.json(
        apiResponse({
          data: TopicAssignmet,
          status: "OK",
          message: "Topic Assignmet deleted succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

module.exports = {
  getAllTopicAssignment,
  getTopicAssignmentById,
  createNewTopicAssignment,
  updateTopicAssignmentById,
  deleteTopicAssignmentById

}