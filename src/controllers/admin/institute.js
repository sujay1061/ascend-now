
const apiResponse = require("../../_helpers/api-response");
const { institute } = require('../../services/admin');

const {
  _getAllInstitutes, 
  _createNewInstitute, 
  _getInstitutesById, 
  _updateInstituteById, 
  _deleteInstituteById
} = institute

createNewInstitute = (req, res, next) => {
  req.body.created_by_account_id = req.user.account_id;
  _createNewInstitute(req)
    .then((institutes) =>
      res.json(
        apiResponse({
          data: institutes,
          status: "OK",
          message: "Institute Added succesfully",
        })
      )
    )
    .catch((err) => next(err));

}

getInstituteById = (req, res, next) => {
  _getInstitutesById(req.params.id)
    .then((institutes) =>
      res.json(
        apiResponse({
          data: institutes,
          status: "OK",
          message: "Institutes fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}



getAllInstitutes = (req, res, next) => {
  req.body.updated_by_account_id = req.user.account_id;
  _getAllInstitutes(req)
    .then((institutes) =>
      res.json(
        apiResponse({
          data: institutes,
          status: "OK",
          message: "Institutes fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

updateInstituteById = (req, res, next) => {
  _updateInstituteById(req.params.id, req.body)
    .then((institutes) =>
      res.json(
        apiResponse({
          data: institutes,
          status: "OK",
          message: "Institute Updated succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

deleteInstituteById = (req, res, next) => {
  _deleteInstituteById(req.params.id)
    .then((institutes) =>
      res.json(
        apiResponse({
          data: institutes,
          status: "OK",
          message: "Institutes Deleted succesfully",
        })
      )
    )
    .catch((err) => next(err));
}


module.exports = {
  getAllInstitutes,
  createNewInstitute,
  getInstituteById,
  updateInstituteById,
  deleteInstituteById
}