const apiResponse = require("../../_helpers/api-response");
const { role } = require('../../services/admin');

const {
  _getAllRoles,
  _getRoleById,
  _createNewRole,
  _updateRoleById,
  _deleteRoleById
} = role

getAllRoles = (req, res, next) => {
  _getAllRoles(req.body)
    .then((roles) =>
      res.json(
        apiResponse({
          data: roles,
          status: "OK",
          message: "Roles fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getRoleById = (req, res, next) => {
  _getRoleById(req.params)
    .then((role) =>
      res.json(
        apiResponse({
          data: role,
          status: "OK",
          message: "Role fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

createNewRole = (req, res, next) => {
  // req.body.created_by_account_id = req.user.account_id;
  _createNewRole(req.body)
    .then((role) =>
      res.json(
        apiResponse({
          data: role,
          status: "OK",
          message: "Role created succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

updateRoleById = (req, res, next) => {
  _updateRoleById(req)
    .then((role) =>
      res.json(
        apiResponse({
          data: role,
          status: "OK",
          message: "Role updated succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

deleteRoleById = (req, res, next) => {
  _deleteRoleById(req.params)
    .then((role) =>
      res.json(
        apiResponse({
          data: role,
          status: "OK",
          message: "Role deleted succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

module.exports = {
  getAllRoles,
  getRoleById,
  createNewRole,
  updateRoleById,
  deleteRoleById
}