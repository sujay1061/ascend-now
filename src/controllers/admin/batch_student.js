const apiResponse = require("../../_helpers/api-response");

const { batch_student } = require('../../services/admin');

const { 
    _getAllBatchStudent,
    _getBatchStudentById,
    _createNewBatchStudent,
    _updateBatchStudentById,
    _deleteBatchStudentById
} = batch_student;

getAllBatchStudents = (req,res,next) => {
    _getAllBatchStudent(req)
    .then((batchStudents) => {
        res.json(
            apiResponse({
                data: batchStudents,
                status: "OK",
                message: "Batch Student fetched succesfully",
            })
        )
    })
    .catch((err) => next(err));
}

getBatchStudentById = (req,res,next) => {
    _getBatchStudentById(req.params.id)
    .then((batchStudent) => {
        res.json(
            apiResponse({
                data: batchStudent,
                status: "OK",
                message: "Batch Student fetched succesfully",
            })
        )
    })
    .catch((err) => next(err));
}

createNewBatchStudent= (req,res,next) => {
    req.body.created_by_account_id = req.user.account_id;
    _createNewBatchStudent(req.body)
    .then((batchStudent) =>{
        res.json(
            apiResponse({
                data: batchStudent,
                status: "OK",
                message: "Batch Student Created succesfully",
            })
        )
    })
    .catch((err) => next(err));
}


updateBatchStudentById = (req, res, next) => {
    req.body.updated_by_account_id = req.user.account_id;
    _updateBatchStudentById(req.params.id,req.body)
    .then((batchStudent) =>
      res.json(
        apiResponse({
          data: batchStudent,
          status: "OK",
          message: "Batch Student updated succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

deleteBatchStudentById = (req, res, next) => {
    _deleteBatchStudentById(req.params.id)
    .then((batchStudent) =>
        res.json(
        apiResponse({
            data: batchStudent,
            status: "OK",
            message: "Batch Student deleted succesfully",
        })
        )
    )
    .catch((err) => next(err));
}

module.exports = {
    getAllBatchStudents,
    getBatchStudentById,
    createNewBatchStudent,
    updateBatchStudentById,
    deleteBatchStudentById
}