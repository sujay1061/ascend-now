const apiResponse = require("../../_helpers/api-response");
const { 
  _getAllfeedbackQuestion,
  _getFeedBackQuestionById,
  _createNewFeedBackQuestion,
  _updateFeedBackQuestionById,
  _deleteFeedBackQuestionById
} = require('../../services/admin/feedback_question.service');

getAllfeedbackQuestion = (req, res, next) => {
    _getAllfeedbackQuestion(req)
    .then((feedback) =>
      res.json(
        apiResponse({
          data: feedback,
          status: "OK",
          message: "feedbackQuestions fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getFeedBackQuestionById = (req, res, next) => {
    _getFeedBackQuestionById(req.params.id)
    .then((feedback) =>
      res.json(
        apiResponse({
          data: feedback,
          status: "OK",
          message: "feedback fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

createNewFeedBackQuestion=(req,res,next) => {
  req.body.created_by_account_id = req.user.account_id;
    _createNewFeedBackQuestion(req.body)
    .then((feedback) =>
      res.json(
        apiResponse({
          data: feedback,
          status: "OK",
          message: "feedback Added succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

updateFeedBackQuestionById = (req, res, next) => {
  req.body.updated_by_account_id = req.user.account_id;
    _updateFeedBackQuestionById(req.params.id,req.body)
    .then((feedback) =>
      res.json(
        apiResponse({
          data: feedback,
          status: "OK",
          message: "feedback updated succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

deleteFeedBackQuestionById = (req,res,next) => {

    _deleteFeedBackQuestionById(req.params.id)
    .then((feedback) =>
    res.json(
      apiResponse({
        data: feedback,
        status: "OK",
        message: "feedback Deleted succesfully",
      })
    )
  )
  .catch((err) => next(err));
  
  }
  


module.exports = {   
getAllfeedbackQuestion,
    getFeedBackQuestionById,
    createNewFeedBackQuestion,
    updateFeedBackQuestionById,
    deleteFeedBackQuestionById
    
}