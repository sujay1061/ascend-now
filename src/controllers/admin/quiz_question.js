const apiResponse = require("../../_helpers/api-response");
const { quiz_question } = require('../../services/admin');

const {
  _getAllQuizQuestions,
  _getQuizQuestionById,
  _getQuizQuestionByTopicId,
  _getAllQuizes,
  _createQuiz,
  _updateQuizById,
  _createQuizQuestion,
  _updateQuizQuestionById,
  _deleteQuizQuestionById,
  _getAllQuizQuestionByQuizId,
  _publishQuiz
} = quiz_question

getAllQuizQuestions = (req, res, next) => {
  _getAllQuizQuestions(req)
    .then((roles) =>
      res.json(
        apiResponse({
          data: roles,
          status: "OK",
          message: "Quiz Questions fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getQuizQuestionById = (req, res, next) => {
  _getQuizQuestionById(req.params)
    .then((question) =>
      res.json(
        apiResponse({
          data: question,
          status: "OK",
          message: "Quiz fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getQuizQuestionByTopicId = (req, res, next) => {
  _getQuizQuestionByTopicId(req)
    .then((quiz) => {
      res.json(
        apiResponse({
          data: quiz,
          status: "OK",
          message: "Quiz fetched succesfully",
        })
      )
    })
}

getAllQuizes = (req, res, next) => {
  _getAllQuizes(req)
    .then((quiz) => {
      res.json(
        apiResponse({
          data: quiz,
          status: "OK",
          message: "Quiz fetched succesfully",
        })
      )
    })
    .catch((err) => next(err));
}

createQuiz = (req, res, next) => {
  _createQuiz(req)
    .then((quiz) => {
      res.json(
        apiResponse({
          data: quiz,
          status: "OK",
          message: "Quiz fetched succesfully",
        })
      )
    })
    .catch((err) => next(err));
}

updateQuizById = (req, res, next) => {
  _updateQuizById(req)
    .then((quiz) => {
      res.json(
        apiResponse({
          data: quiz,
          status: "OK",
          message: "Quiz fetched succesfully",
        })
      )
    })
    .catch((err) => next(err));
}

createQuizQuestion = (req, res, next) => {
  req.body.created_by_account_id = req.user.account_id;
  _createQuizQuestion(req)
    .then((role) =>
      res.json(
        apiResponse({
          data: role,
          status: "OK",
          message: "Quiz created succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

updateQuizQuestionById = (req, res, next) => {
  _updateQuizQuestionById(req)
    .then((quiz) =>
      res.json(
        apiResponse({
          data: quiz,
          status: "OK",
          message: "Quiz updated succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

deleteQuizQuestionById = (req, res, next) => {
  _deleteQuizQuestionById(req.params)
    .then((quiz) =>
      res.json(
        apiResponse({
          data: quiz,
          status: "OK",
          message: "Quiz deleted succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getAllQuizQuestionByQuizId = (req, res, next) => {
  _getAllQuizQuestionByQuizId(req)
    .then((quiz) =>
      res.json(
        apiResponse({
          data: quiz,
          status: "OK",
          message: "Quiz question fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

publishQuiz = (req, res, next) => {
  _publishQuiz(req)
    .then((quiz) =>
      res.json(
        apiResponse({
          data: quiz,
          status: "OK",
          message: "Quiz published succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

module.exports = {
  getAllQuizQuestions,
  getQuizQuestionById,
  getAllQuizes,
  createQuiz,
  updateQuizById,
  createQuizQuestion,
  updateQuizQuestionById,
  deleteQuizQuestionById,
  getQuizQuestionByTopicId,
  getAllQuizQuestionByQuizId,
  publishQuiz
}