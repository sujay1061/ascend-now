const apiResponse = require("../../_helpers/api-response");
const { teacher } = require('../../services/admin');

const {
  _getAllTeachers,
  _getTeacherAccountById,
  _createNewTeacherAccount,
  _updateTeacherAccountById,
  _deleteTeacherAccountById,
  _getAllTeachersByDeliveryChannel,
  _getAllInActiveTeachers,
  _activateTeacherAccountById
} = teacher

getAllTeacherAccounts = (req, res, next) => {
  _getAllTeachers(req)
    .then((teachers) =>
      res.json(
        apiResponse({
          data: teachers,
          status: "OK",
          message: "Teachers fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getTeacherAccountById = (req, res, next) => {
  _getTeacherAccountById(req.params)
    .then((teacher) =>
      res.json(
        apiResponse({
          data: teacher,
          status: "OK",
          message: "Teacher fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

createNewTeacherAccount = (req, res, next) => {
  req.body.created_by_account_id = req.user.account_id;
  _createNewTeacherAccount(req)
    .then((teacher) =>
      res.json(
        apiResponse({
          data: teacher,
          status: "OK",
          message: "Teacher created succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

updateTeacherAccountById = (req, res, next) => {
  req.body.updated_by_account_id = req.user.account_id;
  _updateTeacherAccountById(req)
    .then((teacher) =>
      res.json(
        apiResponse({
          data: teacher,
          status: "OK",
          message: "Teacher updated succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

deleteTeacherAccountById = (req, res, next) => {
  _deleteTeacherAccountById(req.params)
    .then((teacher) =>
      res.json(
        apiResponse({
          data: teacher,
          status: "OK",
          message: "Teacher deleted succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getAllTeachersByDeliveryChannel = (req, res, next) => {
  _getAllTeachersByDeliveryChannel(req.params)
    .then((teacher) =>
      res.json(
        apiResponse({
          data: teacher,
          status: "OK",
          message: "Teacher deleted succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getAllInActiveTeachers = (req, res, next) => {
  _getAllInActiveTeachers(req)
    .then((teachers) =>
      res.json(
        apiResponse({
          data: teachers,
          status: "OK",
          message: "Teachers fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}


activateTeacherAccountById = (req, res, next) => {
  _activateTeacherAccountById(req.params)
    .then((teacher) =>
      res.json(
        apiResponse({
          data: teacher,
          status: "OK",
          message: "Activated the teacher succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

module.exports = {
  getAllTeacherAccounts,
  getTeacherAccountById,
  createNewTeacherAccount,
  updateTeacherAccountById,
  deleteTeacherAccountById,
  getAllTeachersByDeliveryChannel,
  getAllInActiveTeachers,
  activateTeacherAccountById
}