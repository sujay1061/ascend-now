const apiResponse = require("../../_helpers/api-response");
const { account_active_expiration } = require('../../services/admin')

const {
  _getAllAccountSessions,
  _getAllActiveSessions
} = account_active_expiration


function getAllAccountSessions(req, res, next) {
    _getAllAccountSessions(req.body)
    .then((users) =>
      res.json(
        apiResponse({
          data: users,
          status: "OK",
          message: "Session Data fetched successfully",
        })
      )
    )
    .catch((err) => next(err));
}

function getAllActiveSessions(req, res, next) {
    _getAllActiveSessions(req.body)
    .then((users) =>
      res.json(
        apiResponse({
          data: users,
          status: "OK",
          message: "Session Data fetched successfully",
        })
      )
    )
    .catch((err) => next(err));
}


module.exports = {
    getAllAccountSessions,
    getAllActiveSessions
};