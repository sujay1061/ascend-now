const apiResponse = require("../../_helpers/api-response");
const { class_activity } = require('../../services/admin');

const {
    _getAllClassActivities,
    _getClassActivityById,
    _createNewClassActivity,
    _updateClassActivityById,
    _deleteClassActivityById,
} = class_activity

getAllClassActivities = (req, res, next) => {
    _getAllClassActivities(req)
        .then((activites) =>
            res.json(
                apiResponse({
                    data: activites,
                    status: "OK",
                    message: "Class activity Schedules fetched succesfully",
                })
            )
        )
        .catch((err) => next(err));
}

getClassActivityById = (req, res, next) => {
    _getClassActivityById(req)
        .then((activity) =>
            res.json(
                apiResponse({
                    data: activity,
                    status: "OK",
                    message: "Class activity fetched succesfully",
                })
            )
        )
        .catch((err) => next(err));
}

createNewClassActivity = (req, res, next) => {
    req.body.created_by_account_id = req.user.account_id;
    req.body.scheduled_by_account_id = req.user.account_id;
    _createNewClassActivity(req)
        .then((activity) =>
            res.json(
                apiResponse({
                    data: activity,
                    status: "OK",
                    message: "Class activity Scheduled succesfully",
                })
            )
        )
        .catch((err) => next(err));
}

updateClassActivityById = (req, res, next) => {
    req.body.updated_by_account_id = req.user.account_id;
    _updateClassActivityById(req)
        .then((activity) =>
            res.json(
                apiResponse({
                    data: activity,
                    status: "OK",
                    message: "Class activity updated succesfully",
                })
            )
        )
        .catch((err) => next(err));
}

deleteClassActivityById = (req, res, next) => {
    _deleteClassActivityById(req)
        .then((course) =>
            res.json(
                apiResponse({
                    data: course,
                    status: "OK",
                    message: "Class activity deleted succesfully",
                })
            )
        )
        .catch((err) => next(err));
}

module.exports = {
    getAllClassActivities,
    getClassActivityById,
    createNewClassActivity,
    updateClassActivityById,
    deleteClassActivityById,
}