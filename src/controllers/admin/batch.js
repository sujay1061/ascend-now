const apiResponse = require("../../_helpers/api-response");
const { batch } = require('../../services/admin');

const {
  _getAllBatch,
  _createNewBatch,
  _getBatchById,
  _updateBatchById,
  _deleteBatchById,
  _getStudentsByDeliveryChannel,
  _getTeachersByBatchDeliverChannel,
  _getAllB2BBatch,
  _getUniqueStudents
} = batch;

getAllBatch = (req, res, next) => {
  _getAllBatch(req)
    .then((batches) =>
      res.json(
        apiResponse({
          data: batches,
          status: "OK",
          message: "Batches fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getBatchById = (req, res, next) => {
  _getBatchById(req.params.id)
    .then((Batch) =>
      res.json(
        apiResponse({
          data: Batch,
          status: "OK",
          message: "Batch fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

createNewBatch = (req, res, next) => {
  req.body.course_scheduled_id = req.user.account_id;
  _createNewBatch(req.body)
    .then((Batch) =>
      res.json(
        apiResponse({
          data: Batch,
          status: "OK",
          message: "Batch created succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

updateBatchById = (req, res, next) => {
  req.body.updated_by_account_id = req.user.account_id;
  _updateBatchById(req.params.id, req.body)
    .then((Batch) =>
      res.json(
        apiResponse({
          data: Batch,
          status: "OK",
          message: "Batch updated succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

deleteBatchById = (req, res, next) => {
  _deleteBatchById(req.params.id)
    .then((Batch) =>
      res.json(
        apiResponse({
          data: Batch,
          status: "OK",
          message: "Batch deleted succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getStudentsByDeliveryChannel = (req, res, next) => {
  _getStudentsByDeliveryChannel(req.params.channel)
    .then((Batch) =>
      res.json(
        apiResponse({
          data: Batch,
          status: "OK",
          message: "Batch deleted succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getTeachersByBatchDeliverChannel = (req, res, next) => {
  _getTeachersByBatchDeliverChannel(req)
    .then((batch) =>
      res.json(
        apiResponse({
          data: batch,
          status: "OK",
          message: "Batch deleted succesfully",
        })
      )
    )
    .catch((err) => next(err));
}


getAllB2BBatch = (req, res, next) => {
  _getAllB2BBatch(req)
    .then((batches) =>
      res.json(
        apiResponse({
          data: batches,
          status: "OK",
          message: "Batches fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getUniqueStudents = (req, res, next) => {
  _getUniqueStudents(req)
    .then((batches) =>
      res.json(
        apiResponse({
          data: batches,
          status: "OK",
          message: "students fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

module.exports = {
  getAllBatch,
  createNewBatch,
  getBatchById,
  updateBatchById,
  deleteBatchById,
  getStudentsByDeliveryChannel,
  getTeachersByBatchDeliverChannel,
  getAllB2BBatch,
  getUniqueStudents
}