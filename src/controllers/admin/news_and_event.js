const apiResponse = require("../../_helpers/api-response");
const { news_and_event } = require('../../services/admin');

const {
  _getAllNewsAndEvents,
  _getNewsAndEventById,
  _createNewNewsAndEvent,
  _updateNewsAndEventById,
  _deleteNewsAndEventById,
  _getNewsAndEventsForAWeek
} = news_and_event

getAllNewsAndEvents = (req, res, next) => {
  _getAllNewsAndEvents(req)
    .then((roles) =>
      res.json(
        apiResponse({
          data: roles,
          status: "OK",
          message: "NewsAndEvents fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getNewsAndEventById = (req, res, next) => {
  _getNewsAndEventById(req.params)
    .then((role) =>
      res.json(
        apiResponse({
          data: role,
          status: "OK",
          message: "NewsAndEvent fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getNewsAndEventsForAWeek = (req,res,next) => {
  _getNewsAndEventsForAWeek()
  .then((events) => {
    res.json(
      apiResponse({
        data: events,
        status: "OK",
        message: "NewsAndEvent fetched succesfully",
      })
    )
  })
}

createNewNewsAndEvent = (req, res, next) => {
  // req.body.created_by_account_id = req.user.account_id;
  _createNewNewsAndEvent(req.body)
    .then((role) =>
      res.json(
        apiResponse({
          data: role,
          status: "OK",
          message: "NewsAndEvent created succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

updateNewsAndEventById = (req, res, next) => {
  _updateNewsAndEventById(req)
    .then((role) =>
      res.json(
        apiResponse({
          data: role,
          status: "OK",
          message: "NewsAndEvent updated succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

deleteNewsAndEventById = (req, res, next) => {
  _deleteNewsAndEventById(req.params)
    .then((role) =>
      res.json(
        apiResponse({
          data: role,
          status: "OK",
          message: "NewsAndEvent deleted succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

module.exports = {
  getAllNewsAndEvents,
  getNewsAndEventById,
  createNewNewsAndEvent,
  updateNewsAndEventById,
  deleteNewsAndEventById,
  getNewsAndEventsForAWeek
}