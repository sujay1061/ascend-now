const apiResponse = require("../../_helpers/api-response");
const { student } = require('../../services/admin');

const {
  _getAllStudents,
  _getStudentAccountById,
  _createNewStudentAccount,
  _updateStudentAccountById,
  _deleteStudentAccountById,
  _getAllStudentBatches,
  _getAllInActiveStudents,
  _activateStudentAccountById
} = student

getAllStudentAccounts = (req, res, next) => {
  _getAllStudents(req)
    .then((students) =>
      res.json(
        apiResponse({
          data: students,
          status: "OK",
          message: "Students fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getStudentAccountById = (req, res, next) => {
  _getStudentAccountById(req.params)
    .then((student) =>
      res.json(
        apiResponse({
          data: student,
          status: "OK",
          message: "Student fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

createNewStudentAccount = (req, res, next) => {
  req.body.created_by_account_id = req.user.account_id;
  _createNewStudentAccount(req)
    .then((student) =>
      res.json(
        apiResponse({
          data: student,
          status: "OK",
          message: "Student created succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

updateStudentAccountById = (req, res, next) => {
  req.body.updated_by_account_id = req.user.account_id;
  _updateStudentAccountById(req)
    .then((student) =>
      res.json(
        apiResponse({
          data: student,
          status: "OK",
          message: "Student updated succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

deleteStudentAccountById = (req, res, next) => {
  _deleteStudentAccountById(req.params)
    .then((student) =>
      res.json(
        apiResponse({
          data: student,
          status: "OK",
          message: "Student deleted succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getAllStudentBatches = (req, res, next) => {
  _getAllStudentBatches(req)
    .then((students) =>
      res.json(
        apiResponse({
          data: students,
          status: "OK",
          message: "Student batches fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

getAllInActiveStudents = (req, res, next) => {
  _getAllInActiveStudents(req)
    .then((students) =>
      res.json(
        apiResponse({
          data: students,
          status: "OK",
          message: "Inactive Students fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

activateStudentAccountById = (req, res, next) => {
  _activateStudentAccountById(req.params)
    .then((students) =>
      res.json(
        apiResponse({
          data: students,
          status: "OK",
          message: "Activated the student succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

module.exports = {
  getAllStudentAccounts,
  getStudentAccountById,
  createNewStudentAccount,
  updateStudentAccountById,
  deleteStudentAccountById,
  getAllStudentBatches,
  getAllInActiveStudents,
  activateStudentAccountById
}
