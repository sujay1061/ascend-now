const apiResponse = require("../../_helpers/api-response");
const { student_activity } = require('../../services/student');

const {
    _getAllActivitesByTopicId
} = student_activity;

function getAllActivitiesByTopicId(req, res, next) {
    _getAllActivitesByTopicId(req)
        .then((feedback) =>
            res.json(
                apiResponse({
                    data: feedback,
                    status: "OK",
                    message: "Activities fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

module.exports = {
    getAllActivitiesByTopicId,
};