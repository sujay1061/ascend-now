const apiResponse = require("../../_helpers/api-response");
const { student_course_scheduled } = require('../../services/student');

const {
    _uploadopicAssignmentReport,
    _monthlyCalendar,
    _getAllCourseSchedules,
    _getMyAssignments,
    _getAllSubmittedAssignments,
    _joinClass,
    _nextClass,
    _myClassesScheduledForTheDate,
    _getLatestScheduleds,
    _getPastScheduleds
} = student_course_scheduled;

function uploadopicAssignmentReport(req, res, next) {
    req.body.created_by_account_id = req.user.account_id;
    _uploadopicAssignmentReport(req)
        .then((assignment) =>
            res.json(
                apiResponse({
                    data: assignment,
                    status: "OK",
                    message: "Assignment uploaded succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function monthlyCalendar(req, res, next) {
    _monthlyCalendar(req)
        .then((courses) =>
            res.json(
                apiResponse({
                    data: courses,
                    status: "OK",
                    message: "Calendar fetched succesfully",
                })
            )
        )
        .catch((err) => next(err));
}

function getAllCourseSchedules(req, res, next) {
    _getAllCourseSchedules(req)
        .then((courses) =>
            res.json(
                apiResponse({
                    data: courses,
                    status: "OK",
                    message: "Class Schedules fetched succesfully",
                })
            )
        )
        .catch((err) => next(err));
}


function getMyAssignments(req, res, next) {
    _getMyAssignments(req)
        .then((assignments) =>
            res.json(
                apiResponse({
                    data: assignments,
                    status: "OK",
                    message: "Assignments Fetched succesfully",
                })
            )
        )
        .catch((err) => next(err));
}

function getAllSubmittedAssignments(req, res, next) {
    _getAllSubmittedAssignments(req)
        .then((assignments) =>
            res.json(
                apiResponse({
                    data: assignments,
                    status: "OK",
                    message: "Assignments Fetched succesfully",
                })
            )
        )
        .catch((err) => next(err));
}

function nextClass(req, res, next) {
    _nextClass(req)
        .then((result) =>
            res.json(
                apiResponse({
                    data: result,
                    status: "OK",
                    message: "Classes Fetched succesfully",
                })
            )
        )
        .catch((err) => next(err));
}

function joinClass(req, res, next) {
    _joinClass(req)
        .then((result) =>
            res.json(
                apiResponse({
                    data: result,
                    status: "OK",
                    message: "Classes Fetched succesfully",
                })
            )
        )
        .catch((err) => next(err));
}

function myClassesScheduledForTheDate(req, res, next) {
    _myClassesScheduledForTheDate(req)
        .then((result) =>
            res.json(
                apiResponse({
                    data: result,
                    status: "OK",
                    message: "Classes Fetched succesfully",
                })
            )
        )
        .catch((err) => next(err));
}

function getLatestScheduleds(req, res, next) {
    _getLatestScheduleds(req)
        .then((result) =>
            res.json(
                apiResponse({
                    data: result,
                    status: "OK",
                    message: "Classes Fetched succesfully",
                })
            )
        )
        .catch((err) => next(err));
}

function getPastScheduleds(req, res, next) {
    _getPastScheduleds(req)
        .then((result) =>
            res.json(
                apiResponse({
                    data: result,
                    status: "OK",
                    message: "Classes Fetched succesfully",
                })
            )
        )
        .catch((err) => next(err));
}

module.exports = {
    uploadopicAssignmentReport,
    monthlyCalendar,
    getAllCourseSchedules,
    getMyAssignments,
    getAllSubmittedAssignments,
    joinClass,
    nextClass,
    myClassesScheduledForTheDate,
    getLatestScheduleds,
    getPastScheduleds
};