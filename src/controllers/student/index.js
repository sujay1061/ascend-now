const student_course_scheduled = require('./course_scheduled');
const student_profile = require('./student');
const student_points = require('./student_point');
const student_quiz = require('./quiz');
const student_booked_classes = require('./booked_classes');
const student_teacher = require('./teacher');
const student_topic = require('./topic');
const student_activity = require('./class_activity');

module.exports = {
    student_course_scheduled,
    student_profile,
    student_points,
    student_quiz,
    student_booked_classes,
    student_teacher,
    student_topic,
    student_activity
};
