const apiResponse = require("../../_helpers/api-response");
const { student_booked_classes } = require('../../services/student');

const {
    _getMyBookedClasses,
    _bookClassWithTeacher,
    _getMyConfirmedClasses,
    _getBookedClassesByDate,
    _fetchMyUpcomingBookedClasses,
    _joinMyBookedClass,
    _getAllBookedClass,
    _getLatestBookings,
    _getPastBookings
} = student_booked_classes;

function getMyBookedClasses(req, res, next) {
    _getMyBookedClasses(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function bookClassWithTeacher(req, res, next) {
    req.body.created_by_account_id = req.user.account_id;
    _bookClassWithTeacher(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function getMyConfirmedClasses(req, res, next) {
    _getMyConfirmedClasses(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function getBookedClassesByDate(req, res, next) {
    _getBookedClassesByDate(req)
    .then((classes) =>
        res.json(
            apiResponse({
                data: classes,
                status: "OK",
                message: "Classes fetched succesffully",
            })
        )
    )
    .catch((err) => next(err));
}

function fetchMyUpcomingBookedClasses(req, res, next) {
    _fetchMyUpcomingBookedClasses(req)
    .then((classes) =>
        res.json(
            apiResponse({
                data: classes,
                status: "OK",
                message: "Classes fetched succesffully",
            })
        )
    )
    .catch((err) => next(err));
}

function joinMyBookedClass(req, res, next) {
    _joinMyBookedClass(req)
    .then((classes) =>
        res.json(
            apiResponse({
                data: classes,
                status: "OK",
                message: "Classes fetched succesffully",
            })
        )
    )
    .catch((err) => next(err));
}

function getAllBookedClass(req, res, next) {
    _getAllBookedClass(req)
    .then((classes) =>
        res.json(
            apiResponse({
                data: classes,
                status: "OK",
                message: "Booked classes fetched successfully",
            })
        )
    )
    .catch((err) => next(err));
}

function getLatestBookings(req, res, next) {
    _getLatestBookings(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function getPastBookings(req, res, next) {
    _getPastBookings(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

module.exports = {
    getMyBookedClasses,
    bookClassWithTeacher,
    getMyConfirmedClasses,
    getBookedClassesByDate,
    fetchMyUpcomingBookedClasses,
    joinMyBookedClass,
    getAllBookedClass,
    getLatestBookings,
    getPastBookings
};