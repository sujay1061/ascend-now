const apiResponse = require("../../_helpers/api-response");
const { student_points } = require('../../services/student');

const {
    _getMyPointsHistory
} = student_points;

function getMyPointsHistory(req, res, next) {
    _getMyPointsHistory(req)
        .then((points) =>
            res.json(
                apiResponse({
                    data: points,
                    status: "OK",
                    message: "Points fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

module.exports = {
    getMyPointsHistory
};