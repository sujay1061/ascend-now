const apiResponse = require("../../_helpers/api-response");
const { student_profile } = require('../../services/student');

const {
  _updateStudentAccountById,
  _registerStudentForB2C
} = student_profile;

updateStudentAccountById = (req, res, next) => {
  req.body.updated_by_account_id = req.user.account_id;
  _updateStudentAccountById(req)
    .then((student) =>
      res.json(
        apiResponse({
          data: student,
          status: "OK",
          message: "Student updated succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

registerStudentForB2C = (req, res, next) => {
  _registerStudentForB2C(req)
    .then((student) =>
      res.json(
        apiResponse({
          data: student,
          status: "OK",
          message: "Student registered succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

module.exports = {
  updateStudentAccountById,
  registerStudentForB2C
}
