const apiResponse = require("../../_helpers/api-response");
const { student_teacher } = require('../../services/student');

const {
  _getAllTeachersByDeliveryChannel
} = student_teacher;

function getAllTeachersByDeliveryChannel(req, res, next) {
  _getAllTeachersByDeliveryChannel(req)
    .then((courses) =>
      res.json(
        apiResponse({
          data: courses,
          status: "OK",
          message: "Teachers fetched successfully",
        })
      )
    )
    .catch((err) => next(err));
}

module.exports = {
  getAllTeachersByDeliveryChannel
}