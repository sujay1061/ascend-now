const apiResponse = require("../../_helpers/api-response");
const { student_quiz } = require('../../services/student');

const {
    _getQuizQuestionByTopicId,
    _createQuizAnswer,
    _getMyQuizzes,
    _getMyCompletedQuizzes,
    _getInClassQuizQuestionByTopicId,
    _getMyInClassQuiz
} = student_quiz;

function getQuizQuestionByTopicId(req, res, next) {
    _getQuizQuestionByTopicId(req)
        .then((points) =>
            res.json(
                apiResponse({
                    data: points,
                    status: "OK",
                    message: "Quiz fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function createQuizAnswer(req, res, next) {
    req.body.created_by_account_id = req.user.account_id;
    _createQuizAnswer(req)
        .then((answer) =>
            res.json(
                apiResponse({
                    data: answer,
                    status: "OK",
                    message: "Answer Created succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function getMyQuizzes(req, res, next) {
    _getMyQuizzes(req)
        .then((points) =>
            res.json(
                apiResponse({
                    data: points,
                    status: "OK",
                    message: "Quizzes fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function getMyCompletedQuizzes(req, res, next) {
    _getMyCompletedQuizzes(req)
        .then((points) =>
            res.json(
                apiResponse({
                    data: points,
                    status: "OK",
                    message: "Quizzes fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function getInClassQuizQuestionByTopicId(req, res, next) {
    _getInClassQuizQuestionByTopicId(req)
        .then((points) =>
            res.json(
                apiResponse({
                    data: points,
                    status: "OK",
                    message: "Quiz fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function getMyInClassQuiz(req, res, next) {
    _getMyInClassQuiz(req)
        .then((points) =>
            res.json(
                apiResponse({
                    data: points,
                    status: "OK",
                    message: "Quiz fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

module.exports = {
    getQuizQuestionByTopicId,
    createQuizAnswer,
    getMyQuizzes,
    getMyCompletedQuizzes,
    getInClassQuizQuestionByTopicId,
    getMyInClassQuiz
};