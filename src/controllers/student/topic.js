const apiResponse = require("../../_helpers/api-response");
const { student_topic } = require('../../services/student');

const {
    _getTopicsById,
    _getAllTopicsByCourseId
} = student_topic;

function getTopicsById(req, res, next) {
    _getTopicsById(req)
        .then((topic) =>
            res.json(
                apiResponse({
                    data: topic,
                    status: "OK",
                    message: "Topic fetched successfully",
                })
            )
        )
        .catch((err) => next(err));
}

function getAllTopicsByCourseId(req, res, next) {
    _getAllTopicsByCourseId(req)
        .then((topics) =>
            res.json(
                apiResponse({
                    data: topics,
                    status: "OK",
                    message: "Topics fetched successfully",
                })
            )
        )
        .catch((err) => next(err));
}

module.exports = {
    getTopicsById,
    getAllTopicsByCourseId
};