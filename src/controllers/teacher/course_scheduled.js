const apiResponse = require("../../_helpers/api-response");
const { teacher_course_scheduled } = require('../../services/teacher');

const {
    _finishScheduledClass,
    _fetchNextScheduledClass,
    _getMyStudentsInformation,
    _MyStudentsDetailsById,
    _monthlyCalendar,
    _getTeacherTotalClasses,
    _startClass,
    _myCompletedClasses,
    _myScheduledClassForTheDate,
    _enableQuiz,
    _getInClassQuizQuestionByTopicId,
    _getAllCourseSchedules,
    _getLatestScheduleds,
    _getPastScheduleds
} = teacher_course_scheduled;

function finishScheduledClass(req, res, next) {
    req.body.updated_by_account_id = req.user.account_id;
    _finishScheduledClass(req)
        .then((attendance) =>
            res.json(
                apiResponse({
                    data: attendance,
                    status: "OK",
                    message: "Scheduled Course updated succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function fetchNextScheduledClass(req, res, next) {
    _fetchNextScheduledClass(req)
        .then((attendance) =>
            res.json(
                apiResponse({
                    data: attendance,
                    status: "OK",
                    message: "Scheduled Course updated succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function getMyStudentsInformation(req, res, next) {
    _getMyStudentsInformation(req)
        .then((attendance) =>
            res.json(
                apiResponse({
                    data: attendance,
                    status: "OK",
                    message: "Students fetched successfully",
                })
            )
        )
        .catch((err) => next(err));
}

function MyStudentsDetailsById(req, res, next) {
    _MyStudentsDetailsById(req)
        .then((studentData) => {
            res.json(
                apiResponse({
                    data: studentData,
                    status: "OK",
                    message: "Student details fetched successfully",
                })
            )
        })
}

function monthlyCalendar(req, res, next) {
    _monthlyCalendar(req)
        .then((courses) =>
            res.json(
                apiResponse({
                    data: courses,
                    status: "OK",
                    message: "Calendar fetched succesfully",
                })
            )
        )
        .catch((err) => next(err));
}

function getTeacherTotalClasses(req, res, next) {
    _getTeacherTotalClasses(req)
        .then((TeacherAttendance) => {
            res.json(
                apiResponse({
                    data: TeacherAttendance,
                    status: "OK",
                    message: "Teacher Total Classes fetched successfully",
                })
            )
        })
}

function startClass(req, res, next) {
    _startClass(req)
        .then((feedback) =>
            res.json(
                apiResponse({
                    data: feedback,
                    status: "OK",
                    message: "feedback added succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function myCompletedClasses(req, res, next) {
    _myCompletedClasses(req)
        .then((feedback) =>
            res.json(
                apiResponse({
                    data: feedback,
                    status: "OK",
                    message: "feedback added succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function myScheduledClassForTheDate(req, res, next) {
    _myScheduledClassForTheDate(req)
        .then((result) =>
            res.json(
                apiResponse({
                    data: result,
                    status: "OK",
                    message: "Classes Fetched succesfully",
                })
            )
        )
        .catch((err) => next(err));
}

function enableQuiz(req, res, next) {
    _enableQuiz(req)
        .then((result) =>
            res.json(
                apiResponse({
                    data: result,
                    status: "OK",
                    message: "Quiz enabled succesfully",
                })
            )
        )
        .catch((err) => next(err));
}

function getInClassQuizQuestionByTopicId(req, res, next) {
    _getInClassQuizQuestionByTopicId(req)
        .then((result) =>
            res.json(
                apiResponse({
                    data: result,
                    status: "OK",
                    message: "Quiz fetched succesfully",
                })
            )
        )
        .catch((err) => next(err));
}



function getAllCourseSchedules(req, res, next) {
    _getAllCourseSchedules(req)
        .then((attendance) =>
            res.json(
                apiResponse({
                    data: attendance,
                    status: "OK",
                    message: "courseScheduleds fetched successfully",
                })
            )
        )
        .catch((err) => next(err));
}

function getLatestScheduleds(req, res, next) {
    _getLatestScheduleds(req)
        .then((attendance) =>
            res.json(
                apiResponse({
                    data: attendance,
                    status: "OK",
                    message: "courseScheduleds fetched successfully",
                })
            )
        )
        .catch((err) => next(err));
}

function getPastScheduleds(req, res, next) {
    _getPastScheduleds(req)
        .then((attendance) =>
            res.json(
                apiResponse({
                    data: attendance,
                    status: "OK",
                    message: "courseScheduleds fetched successfully",
                })
            )
        )
        .catch((err) => next(err));
}

module.exports = {
    finishScheduledClass,
    fetchNextScheduledClass,
    getMyStudentsInformation,
    MyStudentsDetailsById,
    monthlyCalendar,
    getTeacherTotalClasses,
    startClass,
    myCompletedClasses,
    myScheduledClassForTheDate,
    enableQuiz,
    getInClassQuizQuestionByTopicId,
    getAllCourseSchedules,
    getLatestScheduleds,
    getPastScheduleds
};