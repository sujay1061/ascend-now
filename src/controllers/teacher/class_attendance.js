const apiResponse = require("../../_helpers/api-response");
const { teacher_class_attendance } = require('../../services/teacher');

const {
    _createNewClassAttendance,
    _addMarksToStudentByTopic,
    _getAllfeedbackQuestion,
    _addStudentFeedBack,
    _addAssignmentFeedback
} = teacher_class_attendance;


function createNewClassAttendance(req, res, next) {
    req.body.created_by_account_id = req.user.account_id;
    _createNewClassAttendance(req)
        .then((attendance) =>
            res.json(
                apiResponse({
                    data: attendance,
                    status: "OK",
                    message: "Attendance added succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function addMarksToStudentByTopic(req, res, next) {
    req.body.updated_by_account_id = req.user.account_id;
    _addMarksToStudentByTopic(req)
        .then((attendance) =>
            res.json(
                apiResponse({
                    data: attendance,
                    status: "OK",
                    message: "Marks added succesffully",
                })
            )
        )
        .catch((err) => next(err));
}


function getAllfeedbackQuestion(req, res, next) {
    _getAllfeedbackQuestion(req)
        .then((feedback) =>
            res.json(
                apiResponse({
                    data: feedback,
                    status: "OK",
                    message: "feedback fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function addStudentFeedBack(req, res, next) {
    _addStudentFeedBack(req)
        .then((feedback) =>
            res.json(
                apiResponse({
                    data: feedback,
                    status: "OK",
                    message: "feedback added succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function addAssignmentFeedback(req, res, next) {
    _addAssignmentFeedback(req)
        .then((feedback) =>
            res.json(
                apiResponse({
                    data: feedback,
                    status: "OK",
                    message: "feedback added succesffully",
                })
            )
        )
        .catch((err) => next(err));
}



module.exports = {
    createNewClassAttendance,
    addMarksToStudentByTopic,
    getAllfeedbackQuestion,
    addStudentFeedBack,
    addAssignmentFeedback
};