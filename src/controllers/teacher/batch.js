const apiResponse = require("../../_helpers/api-response");
const { teacher_batch } = require('../../services/teacher');

const {
    _getAllStudentsByBatchId,
} = teacher_batch;


function getAllStudentsByBatchId(req, res, next) {
    req.body.created_by_account_id = req.user.account_id;
    _getAllStudentsByBatchId(req)
        .then((batch) =>
            res.json(
                apiResponse({
                    data: batch,
                    status: "OK",
                    message: "Student fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

module.exports = {
    getAllStudentsByBatchId,
};