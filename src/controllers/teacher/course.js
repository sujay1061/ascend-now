const apiResponse = require("../../_helpers/api-response");
const { teacher_course } = require('../../services/teacher');

const {
  _getAllCourses
} = teacher_course;

function getAllCourses(req, res, next) {
  _getAllCourses(req)
    .then((courses) =>
      res.json(
        apiResponse({
          data: courses,
          status: "OK",
          message: "Courses fetched succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

module.exports = {
  getAllCourses
}