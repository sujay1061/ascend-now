const apiResponse = require("../../_helpers/api-response");
const { teacher_student_point } = require('../../services/teacher');

const {
    _createNewPoints,
} = teacher_student_point;


function createNewPoints(req, res, next) {
    req.body.created_by_account_id = req.user.account_id;
    req.body.teacher_account_id = req.user.id;
    _createNewPoints(req)
        .then((point) =>
            res.json(
                apiResponse({
                    data: point,
                    status: "OK",
                    message: "Points created succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

module.exports = {
    createNewPoints,
};