const apiResponse = require("../../_helpers/api-response");
const { teacher_topic_feedback } = require('../../services/teacher');

const {
    _getAllTopicFeedbacks,
    _createTopicFeedback,
    _getTeacherTopicFeedbackByStudentCredentials
} = teacher_topic_feedback;


function getAllTopicFeedbacks(req, res, next) {
    _getAllTopicFeedbacks(req)
        .then((feedbacks) =>
            res.json(
                apiResponse({
                    data: feedbacks,
                    status: "OK",
                    message: "Feedbacks fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function createTopicFeedback(req, res, next) {
    req.body.created_by_account_id = req.user.account_id;
    _createTopicFeedback(req)
        .then((feedback) =>
            res.json(
                apiResponse({
                    data: feedback,
                    status: "OK",
                    message: "Feedback added succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function getTeacherTopicFeedbackByStudentCredentials(req, res, next) {
    _getTeacherTopicFeedbackByStudentCredentials(req)
        .then((feedback) =>
            res.json(
                apiResponse({
                    data: feedback,
                    status: "OK",
                    message: "Feedback fetched successfully",
                })
            )
        )
        .catch((err) => next(err));
}

module.exports = {
    getAllTopicFeedbacks,
    createTopicFeedback,
    getTeacherTopicFeedbackByStudentCredentials
};