const apiResponse = require("../../_helpers/api-response");
const { teacher_class_activities } = require('../../services/teacher');

const {
    _getAllActivitesByTopicId
} = teacher_class_activities;

function getAllActivitiesByTopicId(req, res, next) {
    _getAllActivitesByTopicId(req)
        .then((feedback) =>
            res.json(
                apiResponse({
                    data: feedback,
                    status: "OK",
                    message: "feedback fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

module.exports = {
    getAllActivitiesByTopicId,
};