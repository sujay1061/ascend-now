const apiResponse = require("../../_helpers/api-response");
const { teacher_profile } = require('../../services/teacher');

const {
  _updateTeacherAccountById
} = teacher_profile;

updateTeacherAccountById = (req, res, next) => {
  req.body.updated_by_account_id = req.user.account_id;
  _updateTeacherAccountById(req)
    .then((teacher) =>
      res.json(
        apiResponse({
          data: teacher,
          status: "OK",
          message: "Teacher updated succesfully",
        })
      )
    )
    .catch((err) => next(err));
}
module.exports = {
  updateTeacherAccountById
}