const apiResponse = require("../../_helpers/api-response");
const { teacher_booked_classes } = require('../../services/teacher');

const {
    _getMyBookedClasses,
    _confirmBookedClass,
    _rejectBookedClass,
    _getMyConfirmedClasses,
    _bookClassWithStudent,
    _getBookedClassesByDate,
    _fetchNextBookedClass,
    _startMyBookedClass,
    _finishBookedClass,
    _getAllBookedClasses,
    _getLatestBookings,
    _getPastBookings
} = teacher_booked_classes;

function getMyBookedClasses(req, res, next) {
    _getMyBookedClasses(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function confirmBookedClass(req, res, next) {
    req.body.created_by_account_id = req.user.account_id;
    _confirmBookedClass(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes Accepted succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function rejectBookedClass(req, res, next) {
    _rejectBookedClass(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes Rejected succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function getMyConfirmedClasses(req, res, next) {
    _getMyConfirmedClasses(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes fetched succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function bookClassWithStudent(req, res, next) {
    _bookClassWithStudent(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes booked succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function getBookedClassesByDate(req, res, next) {
    _getBookedClassesByDate(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes booked succesffully",
                })
            )
        )
        .catch((err) => next(err));
}

function fetchNextBookedClass(req, res, next) {
    _fetchNextBookedClass(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes booked found",
                })
            )
        )
        .catch((err) => next(err));
}

function startMyBookedClass(req, res, next) {
    _startMyBookedClass(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes booked found",
                })
            )
        )
        .catch((err) => next(err));
}

function finishBookedClass(req, res, next) {
    _finishBookedClass(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes booked found",
                })
            )
        )
        .catch((err) => next(err));
}


function getAllBookedClasses(req, res, next) {
    _getAllBookedClasses(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes booked fetched successfully",
                })
            )
        )
        .catch((err) => next(err));
}

function getLatestBookings(req, res, next) {
    _getLatestBookings(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes booked fetched successfully",
                })
            )
        )
        .catch((err) => next(err));
}

function getPastBookings(req, res, next) {
    _getPastBookings(req)
        .then((classes) =>
            res.json(
                apiResponse({
                    data: classes,
                    status: "OK",
                    message: "Classes booked fetched successfully",
                })
            )
        )
        .catch((err) => next(err));
}



module.exports = {
    getMyBookedClasses,
    confirmBookedClass,
    rejectBookedClass,
    getMyConfirmedClasses,
    bookClassWithStudent,
    getBookedClassesByDate,
    fetchNextBookedClass,
    startMyBookedClass,
    finishBookedClass,
    getAllBookedClasses,
    getLatestBookings,
    getPastBookings
};