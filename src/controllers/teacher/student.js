const apiResponse = require("../../_helpers/api-response");
const { teacher_student } = require('../../services/teacher');

const {
  _getStudentsByDeliveryChannel
} = teacher_student;

getStudentsByDeliveryChannel = (req, res, next) => {
  _getStudentsByDeliveryChannel(req.params.channel)
    .then((Batch) =>
      res.json(
        apiResponse({
          data: Batch,
          status: "OK",
          message: "Batch deleted succesfully",
        })
      )
    )
    .catch((err) => next(err));
}

module.exports = {
  getStudentsByDeliveryChannel
}