const { teacherWelcomeMailTemplate, teacherCourseScheduleMailTemplate,teacherCourseScheduleStartsInMinutes, onPostLessonUpdateEmailTemplate, onConfirmTheBookedCLassMailTemplate, onCancellingTheBookedClassMailTemplate,onstudentFeedbackMailTemplate, onSendViewReportMailTemplate, onTeacherBooksClassTemplate , bookedClassReminderInMinutes,onTeacherAbscent,onB2BTeacherWereAbscent,oncancelOnTeacherStudentDintJoinTheClass,onTeacherBooksClassTeacherMailTemplate} = require("../../notification-templates/teacher/email-template");
const { jobProcessor } = require("../job-processor");

const fs = require("fs");



exports.teacherWelcomeMail = (teacher, password) => {
    const emailTemplate = teacherWelcomeMailTemplate(teacher, password);
    jobProcessor({
        to: teacher.primary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}

exports.teacherCourseScheduleMail = (teacher,batch,topic,scheduleDateTime) => {
    const emailTemplate = teacherCourseScheduleMailTemplate(teacher,batch.name,topic.name,scheduleDateTime)
    jobProcessor({
        to: teacher.primary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}

exports.teacherCourseSchedueldInMinutes = (teacher,batch,topic) => {
    const emailTemplate = teacherCourseScheduleStartsInMinutes(teacher,batch.name,topic.name)
    jobProcessor({
        to: teacher.primary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}

exports.postLessonUpdateEmail = (DeatilsOfTeacherStudentTopic) => {
    const emailTemplate = onPostLessonUpdateEmailTemplate(DeatilsOfTeacherStudentTopic)
    jobProcessor({
        to: DeatilsOfTeacherStudentTopic.student.primary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}

exports.confirmTheBookedClassMail = (student,teacher,scheduleDateTime,topic) => {
    const emailTemplate = onConfirmTheBookedCLassMailTemplate(student,teacher,scheduleDateTime,topic)
    jobProcessor({
        to: student.primary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}

exports.cancellingTheBookedClassMail = (student) => {
    const emailTemplate = onCancellingTheBookedClassMailTemplate(student)
    jobProcessor({
        to: student.primary_email,
        cc:"shivakumar.d@pacewisdom.com",
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}

exports.studentFeedback = (student,teacher,topic) => {
   console.log(student.name)
    pathToAttachment = `report_${student.name}_${topic.id}.pdf`;
console.log(pathToAttachment)
attachment = fs.readFileSync(pathToAttachment).toString("base64");
    const emailTemplate = onstudentFeedbackMailTemplate(student, teacher, topic)
    jobProcessor({
        to: student.primary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html,
        attachments: [
            {
              content: attachment,
              filename: `report_${student.name}_${topic.id}.pdf`,
              type: "application/pdf",
              disposition: "attachment"
            }
          ]
        
    });
}

exports.sendViewReportMail = (student,teacher,topic,urlToken) => {
    const emailTemplate = onSendViewReportMailTemplate(student,teacher,topic,urlToken)
    jobProcessor({
        to: student.primary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}

exports.teacherBooksClass = (student,teacher,topic,date,comment) => {
    const emailTemplate = onTeacherBooksClassTemplate(student,teacher,topic,date,comment)
    jobProcessor({
        to: student.primary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}

exports.teacherReminderMailInMinutes = (teacher,student,topic) => {
    const emailTemplate = bookedClassReminderInMinutes(teacher,student,topic)
    jobProcessor({
        to: teacher.primary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}

exports.teacherYouWereAbscent = (teacher,student,topic,bookedTime) => {
    const emailTemplate = onTeacherAbscent(teacher,student,topic,bookedTime)
    jobProcessor({
        to: teacher.primary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}

exports.B2BTeacherWereAbscent = (teacher,topic,batch,bookedTime) => {
    const emailTemplate = onB2BTeacherWereAbscent(teacher,topic,batch,bookedTime)
    jobProcessor({
        to: teacher.primary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}

exports.cancelOnTeacherDintJoinTheClass = (teacher,student,topic,bookedTime) => {
    const emailTemplate = oncancelOnTeacherStudentDintJoinTheClass(teacher,student,topic,bookedTime)
    jobProcessor({
        to: teacher.primary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}


exports.teacherBooksClassTeacherMail = (student,teacher,topic,date,comment) => {
    const emailTemplate = onTeacherBooksClassTeacherMailTemplate(student,teacher,topic,date,comment)
    jobProcessor({
        to: teacher.primary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}