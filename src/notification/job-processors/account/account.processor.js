const { forgotAssendNowPassword,successPasswordMail, successChangeOfPasswordParentMailTemplate } = require("../../notification-templates/account/account_email-template");
const { jobProcessor } = require("../job-processor");

exports.forgotPassword = (emailId,userName,token) => {
    const emailTemplate = forgotAssendNowPassword(userName,token);
    jobProcessor({
        to: emailId,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}

exports.successChangeOfPassword = (emailId,userName, password) => {
    const emailTemplate = successPasswordMail(userName, password);
    jobProcessor({
        to: emailId,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}

exports.successChangeOfPasswordParentMail = (emailId,userName,password) => {
    const emailTemplate = successChangeOfPasswordParentMailTemplate(userName,password);
    jobProcessor({
        to: emailId,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}