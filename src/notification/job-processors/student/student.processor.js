const { studentWelcomeMailTemplate, studentCourseScheduleMailTemplate, studentCourseScheduleStartsInMinutes,bookedRequestedSlotWithTeacher,bookedClassReminderInMinutes,onTeacherIsAbscent,OnNotificationToStudentOnTeacherAbscent,oncancelOnStudentTeacherDintJoinTheClass, studentsParentWelcomeMailTemplate } = require("../../notification-templates/student/email-template");
const { jobProcessor } = require("../job-processor");

exports.studentWelcomeMail = (student, password) => {
    const emailTemplate = studentWelcomeMailTemplate(student, password);
    jobProcessor({
        to: student.primary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}

exports.studentCourseScheduleMail = (student,TeacherData,topic,scheduleDateTime) => {
    for(var i=0;i<student.length;i++)
    {
        const emailTemplate = studentCourseScheduleMailTemplate(student[i],TeacherData.name,topic.name,scheduleDateTime);
        jobProcessor({
            to: student[i].primary_email,
            subject: emailTemplate.subject,
            html: emailTemplate.html
        });
    }
  
}

exports.studentCourseScheduleInMinutes = (student,teacherDetails,topicName) => {
    var TeacherName=teacherDetails.dataValues.name
    for(var i=0;i<student.length;i++)
    {
        const emailTemplate = studentCourseScheduleStartsInMinutes(student[i],TeacherName,topicName);
        jobProcessor({
            to: student[i].primary_email,
            subject: emailTemplate.subject,
            html: emailTemplate.html
        });
    }
  
}

exports.requestedSlotsWithTeacher = (student,teacher,topic,requestedTime,comment) => {
    const emailTemplate = bookedRequestedSlotWithTeacher(student.name,teacher.name,topic.name,requestedTime,comment)
    jobProcessor({
        to: student.primary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}

exports.studentReminderMailInMinutes = (teacher,student,topic) => {
    const emailTemplate = bookedClassReminderInMinutes(teacher,student,topic)
    jobProcessor({
        to: student.primary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}

exports.teacherIsAbscent = (teacher,student,topic,bookedTime) => {
    const emailTemplate = onTeacherIsAbscent(teacher,student,topic,bookedTime)
    jobProcessor({
        to: student.primary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}
exports.notificationToStudentOnTeacherAbscent = (teacher,student,topic,scheduledTime) => {
    
    for(var i=0;i<student.length;i++)
    {
        const emailTemplate = OnNotificationToStudentOnTeacherAbscent(teacher,student[i],topic,scheduledTime);
        jobProcessor({
            to: student[i].primary_email,
            subject: emailTemplate.subject,
            html: emailTemplate.html
        });
    }
  
}

exports.cancelOnStudentDintJoinTheClass = (teacher,student,topic,bookedTime) => {
    const emailTemplate = oncancelOnStudentTeacherDintJoinTheClass(teacher,student,topic,bookedTime)
    jobProcessor({
        to: student.primary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}



exports.studentsParentWelcomeMail = (student, password) => {
    const emailTemplate = studentsParentWelcomeMailTemplate(student, password);
    jobProcessor({
        to: student.secondary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}