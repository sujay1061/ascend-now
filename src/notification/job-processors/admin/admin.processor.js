const { sendClassRecordingWidgetTemplate,onCancelBookedClassByAdminForStudentTemplate, onCancelBookedClassByAdminForTeacherTemplate, onCancelOnTeacherDintJoinTheClassTemplate,onTeacherAbscentToClassTemplate,onNotificationToAdminTeacherB2BAbscentTemplate } = require("../../notification-templates/admin/email-template");
const { jobProcessor } = require("../job-processor");

exports.sendClassRecordingWidgetMail = (classes, admin) => {
    const emailTemplate = sendClassRecordingWidgetTemplate(classes, admin);
    jobProcessor({
        to: admin.email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}

exports.cancelBookedClassByAdminForTeacher = (teacher,student,topic,schedules) => {
    const emailTemplate = onCancelBookedClassByAdminForTeacherTemplate(teacher,student,topic,schedules);
    jobProcessor({
        to: teacher.primary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}

exports.cancelBookedClassByAdminForStudent = (teacher,student,topic,schedules) => {
    const emailTemplate = onCancelBookedClassByAdminForStudentTemplate(teacher,student,topic,schedules);
    jobProcessor({
        to: student.primary_email,
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}

exports.cancelOnTeacherDintJoinTheClass = (teacher,student,topic,schedules) => {
   
    for(var i=0;i<student.length;i++)
    {
        const emailTemplate = onCancelOnTeacherDintJoinTheClassTemplate(teacher,student[i],topic,schedules);
        jobProcessor({
            to: student[i].primary_email,
            bcc:"shivakumar.d@pacewisdom.com",
            subject: emailTemplate.subject,
            html: emailTemplate.html
        });
}
}

exports.notificationToAdminTeacherIsAbscent = (teacher,student,topic,bookedTime) => {
    const emailTemplate = onTeacherAbscentToClassTemplate(teacher,student,topic,bookedTime);
    jobProcessor({
        to: "sujay.t@pacewisdom.com",
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}

exports.notificationToAdminTeacherB2BAbscent = (teacher,topic,batch,bookedTime) => {
    const emailTemplate = onNotificationToAdminTeacherB2BAbscentTemplate(teacher,topic,batch,bookedTime);
    jobProcessor({
        to: "sujay.t@pacewisdom.com",
        subject: emailTemplate.subject,
        html: emailTemplate.html
    });
}
