const sgMail = require('@sendgrid/mail');

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

/**
 * 
 * @param { to, from, subject, html, text } msg 
 */

exports.jobProcessor = (msg) => {
    msg.from = 'communications@ascendnow.info';
    sgMail
        .send(msg)
        .then(() => {
            console.log('Email sent to')
        })
        .catch((error) => {
            console.error(error)
        })
}