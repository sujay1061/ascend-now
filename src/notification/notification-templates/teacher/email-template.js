
exports.teacherWelcomeMailTemplate = (teacher, password) => {
    const subject = `Welcome Mail to Ascend Now ${teacher.name}`;
    const html = `
    <p> Dear ${teacher.name}, <br/>
    Thank you for choosing Ascend Now!<br/><br/>
  
    To access the Ascend Now platform please visit: <a href="${process.env.CLIENT_BASE_URL}">login</a><br/><br/>

    Your login details are as follows:<br/>
    ● Username: <b>${teacher.primary_email}</b><br/>
    ● Password: <b>${password}</b><br/><br/>

    Welcome to the community!<br/>
    Ascend Now Team

    </p>
    `

    return { subject, html };
}

exports.teacherCourseScheduleMailTemplate = (teacher,batchName,topicName,ScheduleDateTime) => {
    // var now=new Date(ScheduleDateTime)
    // var date = now.toLocaleDateString();
    // var time = now.toLocaleTimeString();
    const subject = `New Course Has been scheduled ${teacher.name}`;
    const html = `
    <p>Dear ${teacher.name}, <br/><br/>
    You have a ${topicName} class scheduled with batch ${batchName} on date ${ScheduleDateTime}.<br/>
    See you on that day <br/><br/>
    Yours sincerely,<br/>
    Ascend Now Team
   
    </p>
    `


    return { subject, html };
}

exports.teacherCourseScheduleStartsInMinutes = (teacher,batchName,topicName) => {
    const subject = `New Course will start in 15 minutes from now  ${teacher.name}`;
    const html = `
    <p>Dear ${teacher.name}, <br/>
    Your ${topicName} class with the batch ${batchName} will commence 15 minutes<br/><br/>
    See you soon!<br/><br/>
    Yours sincerely,<br/>
    Ascend Now Team<br/>
    All the best for the new Course.<br/>
    </p>
    `
    

    return { subject, html };
}

exports.onPostLessonUpdateEmailTemplate = (DeatilsOfTeacherStudentTopic) => {
    const subject = `Post lesson update Email`;
    const html = `
    <p>Dear ${DeatilsOfTeacherStudentTopic.student.name}, <br/>
    I hope I find you well!<br/><br/>
    Attached is the Post Lesson Update for ${DeatilsOfTeacherStudentTopic.student.name}'s ${DeatilsOfTeacherStudentTopic.topic.name} class with ${DeatilsOfTeacherStudentTopic.teacher.name}'s.<br/><br/>
    If you have any questions, please feel free to contact ${DeatilsOfTeacherStudentTopic.teacher.name} at ${DeatilsOfTeacherStudentTopic.teacher.primary_email}.<br/><br/>
    
    Yours sincerely,<br/>
    Ascend Now Team
    `
    

    return { subject, html };
}

exports.onConfirmTheBookedCLassMailTemplate = (student,teacher,scheduleDateTime,topic) => {
    console.log("email sent")
    // var now=new Date(scheduleDateTime)
    // var date = now.toLocaleDateString();
    // var time = now.toLocaleTimeString();
    const subject = `Confirmation of Scheduling`;
    const html = `
    <p>Dear ${student.name}, <br/><br/>

    Your requested time slot has been accepted by ${teacher.name} for the topic ${topic.name}.<br/>
    Your class is scheduled on ${scheduleDateTime}.<br/>
    See you then!<br/><br/>

    Yours sincerely,<br/>
    Ascend Now Team
    `
    return { subject, html };
}

exports.onCancellingTheBookedClassMailTemplate = (student) => {
    console.log("email sent")
    const subject = `Unavailable of requested dates`;
    const html = `
    <p>Dear ${student.name}, <br/><br/>

    The two options you provided in your scheduling request are unavailable.<br/>
  
    Yours sincerely,<br/>
    Ascend Now Team
    `
    return { subject, html };
}

exports.onstudentFeedbackMailTemplate = (student,teacher,topic) => {
    console.log("email sent")
    const subject = `Feedback of the student`;
    const html = `
    <p>Dear ${student.name}, <br/>
    ${teacher.name} has provided the feedback for ${student.name}.<br/><br/>
    Attached is the feedback for the ${student.name}'s for the topic ${topic.name} class with ${teacher.name}'s.<br/><br/>
    If you have any questions, please feel free to contact ${teacher.name} at ${teacher.primary_email}.<br/><br/>
    
    Yours sincerely,<br/>
    Ascend Now Team
    `
    

    return { subject, html };
}

exports.onSendViewReportMailTemplate = (student,teacher,topic,urlLink) => {
    console.log("email sent")
    const subject = `Feedback of the student`;
    const html = `
    <p>Dear ${student.name}, <br/>
    ${teacher.name} has provided the feedback for ${student.name}.<br/><br/>
    click of the below link to view the feedback for the ${student.name}'s for the topic ${topic.name} class with ${teacher.name}'s.<br/><br/>
    If you have any questions, please feel free to contact ${teacher.name} at ${teacher.primary_email}.<br/><br/>
    To check the feedback please <br/>
    <b> <a href="${process.env.CLIENT_BASE_URL}/pdfreports?token=${urlLink}">click here</a><br/><br/></b>
    
    Yours sincerely,<br/>
    Ascend Now Team
    `
    

    return { subject, html };
}

exports.onTeacherBooksClassTemplate = (student,teacher,topic,date,comment) => {
    if(comment==undefined||comment=="")
    {
        comment = "-"
    }
    console.log("email sent")
    const subject = `Class has been booked`;
    const html = `
    <p>Dear ${student.name}, <br/>
    ${teacher.name} has booked a class on topic ${topic.name} scheduled at time ${date}.<br/><br/>
    
    <b>Teacher comments :</b> ${comment}.<br/><br/>
    
    Yours sincerely,<br/>
    Ascend Now Team
    `
    

    return { subject, html };
}


exports.bookedClassReminderInMinutes = (teacher,student,topicName) => {
    const subject = `New Course will start in 15 minutes from now  ${teacher.name}`;
    const html = `
    <p>Dear ${teacher.name}, <br/>
    Your ${topicName} class with the student ${student.name} will commence 15 minutes<br/><br/>
    See you soon!<br/><br/>
    Yours sincerely,<br/>
    Ascend Now Team<br/>
    All the best for the new Course.<br/>
    </p>
    `
    

    return { subject, html };
}

exports.onTeacherAbscent = (teacher,student,topicName,bookedTime) => {
    const subject = `${teacher.name} You haven't taken a class`;
    const html = `
    <p>Dear ${teacher.name}, <br/>
    You were abscent for the topic ${topicName} with the student ${student.name} which was booked at ${bookedTime}<br/><br/>
    <b>Class is cancelled.<br/><br/></b>
    Yours sincerely,<br/>
    Ascend Now Team<br/>
    All the best for the new Course.<br/>
    </p>
    `
    

    return { subject, html };
}

exports.onB2BTeacherWereAbscent = (teacher,topicName,batch,ScheduleDateTime) => {
    const subject = `${teacher.name} You haven't taken a class`;
    const html = `
    <p>Dear ${teacher.name}, <br/>
    You were abscent for the topic ${topicName} for the batch ${batch.name} which was scheduled at ${ScheduleDateTime}<br/><br/>
    <b>Class is cancelled.<br/><br/></b>
    Yours sincerely,<br/>
    Ascend Now Team<br/>
    All the best for the new Course.<br/>
    </p>
    `
    

    return { subject, html };
}

exports.oncancelOnTeacherStudentDintJoinTheClass = (teacher,student,topicName,ScheduleDateTime) => {
    const subject = `${teacher.name} You haven't taken a class`;
    const html = `
    <p>Dear ${teacher.name}, <br/>
    Since student ${student.name} unavailability and even you dint take the class which was scheduled at ${ScheduleDateTime} for the topic ${topicName}.<br/><br/>
    <b>Class is cancelled.<br/><br/></b>
    Yours sincerely,<br/>
    Ascend Now Team<br/>
    All the best for the new Course.<br/>
    </p>
    `
    

    return { subject, html };
}
  

exports.onTeacherBooksClassTeacherMailTemplate = (student,teacher,topic,date,comment) => {
    if(comment==undefined||comment=="")
    {
        comment = "-"
    }
    console.log("email sent")
    const subject = `Class has been booked`;
    const html = `
    <p>Dear ${teacher.name}, <br/>
    you has booked a class with the student ${student.name} for the topic ${topic.name} booked at time ${date}.<br/><br/>
    <b>Teacher comments :</b> ${comment}.<br/><br/>
    
    Yours sincerely,<br/>
    Ascend Now Team
    `
    

    return { subject, html };
}