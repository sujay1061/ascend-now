exports.sendClassRecordingWidgetTemplate = (classes) => {
    const subject = `Recording for the class ${classes.scheduled_name}`;
    const html = `
    <p> Dear Admin, <br/>
        To check the recording please <a href="${classes.recording_url}">click here..</a>
    </p>
    `
    return { subject, html };
}

exports.onCancelBookedClassByAdminForTeacherTemplate = (teacher, student,topic, scheduleDateTime) => {
    const subject = `Cancellation Confirmation From Admin`;
    const html = `
    <p> Dear ${teacher.name}, <br/><br/>
    Admin has cancelled your ${topic.name} class scheduled with student ${student.name} which was scheduled on ${scheduleDateTime.schedule_datetime}.<br/><br/>
    Yours sincerely,
    Ascend Now Team

    </p>
    `
    return { subject, html };
}

exports.onCancelBookedClassByAdminForStudentTemplate = (teacher, student,topic, scheduleDateTime) => {
    const subject = `Cancellation Confirmation From Admin`;
    const html = `
    <p> Dear ${student.name}, <br/><br/>
    Admin has cancelled your ${topic.name} class scheduled with teacher ${teacher.name} which was scheduled on ${scheduleDateTime.schedule_datetime}.<br/><br/>
    Yours sincerely,
    Ascend Now Team

    </p>
    `
    return { subject, html };
}

exports.onCancelOnTeacherDintJoinTheClassTemplate = (teacher, student,topic, scheduleDateTime) => {
    const subject = `Cancellation of Class`;
    const html = `
    <p> Dear ${student.name}, <br/><br/>
    On the teacher ${teacher.name} unavailability and even you dint the join the class for the topic ${topic.name}. Class has been cancelled which was scheduled on ${scheduleDateTime}.<br/><br/>
    Yours sincerely,
    Ascend Now Team

    </p>
    `
    return { subject, html };
}

exports.onTeacherAbscentToClassTemplate = (teacher, student,topic,bookedTime) => {
    const subject = `Teacher abscent to class`;
    const html = `
    <p> Dear Admin, <br/><br/>
    Since teacher ${teacher.name} unavailability for the class which was booked at ${bookedTime} for the topic ${topic} with the student ${student.name}.<br/><br/>
    <b>Class is cancelled.<br/><br/></b>
    Yours sincerely,
    Ascend Now Team
    </p>
    `
    return { subject, html };
}

exports.onNotificationToAdminTeacherB2BAbscentTemplate = (teacher,topic,batch,ScheduleDateTime) => {
    const subject = `Teacher abscent to class`;
    const html = `
    <p> Dear Admin, <br/><br/>
    Since teacher ${teacher.name} unavailability for the class which was scheduled at ${ScheduleDateTime} for the topic ${topic} with batch students ${batch.name}. <br/><br/>
    <b>Class is cancelled.<br/><br/></b>
    Yours sincerely,
    Ascend Now Team
    </p>
    `
    return { subject, html };
}