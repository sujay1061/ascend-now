exports.forgotAssendNowPassword = (userName,token) => {
    const subject = `Forgot Password Mail from Ascend Now`;
    const html = `
    Dear ${userName},<br/><br/>

    We understand that you would like to change your password.<br/>
    Reset your password now by clicking here.<br/> 
    <a href="${process.env.CLIENT_BASE_URL}/reset?token=${token}">click here</a><br/><br/>

    Just follow the prompts to reset your password for your Ascend Now account<br/><br/>

    If you have not requested to reset your password, please ignore this email and your password will remain the same.<br/><br/>
    
    Yours sincerely,<br/>
    Ascend Now Team
    `
    return { subject, html };
}

exports.successPasswordMail = (userName,password) => {
    const subject = `Successfully Changed Your Password of Ascend Now`;
    const html = `
    Dear ${userName},<br/><br/>

    Your Ascend Now platform password has been successfully changed.<br/>
    The new password is <b>${password}</b>.<br/>
    If you did not request this change, please contact us at tech_support@ascendnow.info for assistance.<br/> 
    <b><a href="https://tech_support@ascendnow.info"></a></b><br/><br/>
   

    Thank you.<br/> 

    Yours sincerely,<br/>
    Ascend Now Team
    `
    return { subject, html };
}

exports.successChangeOfPasswordParentMailTemplate = (userName,password) => {
    const subject = `Successfully Changed Your Password of Ascend Now`;
    const html = `
    Dear ${userName}'s parent,<br/><br/>

    Your Ascend Now platform password has been successfully changed.<br/>
    The new password is <b>${password}</b>.<br/>
    If you or you child did not request this change, please contact us at tech_support@ascendnow.info for assistance.<br/> 
    <b><a href="https://tech_support@ascendnow.info"></a></b><br/><br/>
   

    Thank you.<br/> 

    Yours sincerely,<br/>
    Ascend Now Team
    `
    return { subject, html };
}