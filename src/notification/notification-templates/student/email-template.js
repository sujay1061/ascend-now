
exports.studentWelcomeMailTemplate = (student, password) => {
    const subject = `Welcome Mail to Ascend Now ${student.name}`;
    const html = `
    <p> Dear ${student.name}, <br/>
    Thank you for choosing Ascend Now!<br/><br/>
  
    To access the Ascend Now platform please visit: <a href="${process.env.CLIENT_BASE_URL}">login</a><br/><br/>

    Your login details are as follows:<br/>
    ● Username: <b>${student.primary_email}</b><br/>
    ● Password: <b>${password}</b><br/><br/>

    Welcome to the community!<br/>
    Ascend Now Team
    </p>
    `

    return { subject, html };
}

exports.studentCourseScheduleMailTemplate = (student,TeacherName,topicName,ScheduleDateTime) => {
    // var now=new Date(ScheduleDateTime)
    // var date = now.toLocaleDateString();
    // var time = now.toLocaleTimeString();
    const subject = `New Course Has been scheduled  ${student.name}`;
    const html = `
    <p>Dear ${student.name}, <br/><br/>
    You have a ${topicName} class scheduled with ${TeacherName} on date ${ScheduleDateTime}.<br/>
    See you on that day <br/><br/>
    Yours sincerely,<br/>
    Ascend Now Team   
    </p>
    `
    return { subject, html };
}
exports.studentCourseScheduleStartsInMinutes = (student,teacherName,topicName) => {
    const subject = `New Course will start in 15 minutes from now  ${student.name}`;
    const html = `
    <p>Dear ${student.name}, <br/>
    Your ${topicName} class with the teacher ${teacherName} will commence 15 minutes<br/><br/>
    See you soon!<br/><br/>
    Yours sincerely,<br/>
    Ascend Now Team<br/>
    All the best for the new Course.<br/>
    </p>
    `

    return { subject, html };
}

exports.bookedRequestedSlotWithTeacher = (studentName,teacherName,topicName,requestedTime,comment) => {
    if(comment==undefined||comment=="")
    {
        comment = "-"
    }
    var date1= new Date(requestedTime['1'].start_date)
    var date2= new Date(requestedTime['2'].start_date)

    var dateOf1 = date1.toLocaleDateString();
    var timeOf1 = date1.toLocaleTimeString();

    var dateOf2 = date2.toLocaleDateString();
    var timeOf2 = date2.toLocaleTimeString();

    const subject = `Confirmation of Scheduling Request`;
    const html = `
    <p>Dear ${studentName}, <br/> <br/>
    You have requested a ${topicName} class with ${teacherName}. The proposed time slots provided
    are:<br/>
    ● Option 1: ${dateOf1} at ${timeOf1}<br/>
    ● Option 2: ${dateOf2} at ${timeOf2}<br/><br/>
    You will receive a response from ${teacherName} within 1 working day.<br/><br/>

    <b>Student Comments :</b> ${comment}.<br/><br/>
    Yours sincerely,<br/>
    Ascend Now Team
    </p>
    `
    return { subject, html };
}

exports.bookedClassReminderInMinutes = (teacher,student,topicName) => {
    const subject = `New Course will start in 15 minutes from now  ${student.name}`;
    const html = `
    <p>Dear ${student.name}, <br/>
    Your ${topicName} class with the teacher ${teacher.name} will commence 15 minutes<br/><br/>
    See you soon!<br/><br/>
    Yours sincerely,<br/>
    Ascend Now Team<br/>
    All the best for the new Course.<br/>
    </p>
    `
    

    return { subject, html };
}

exports.onTeacherIsAbscent = (teacher,student,topicName,bookedTime) => {
    const subject = `class cancellation`;
    const html = `
    <p>Dear ${student.name}, <br/>
    Since teacher ${teacher.name} was unavaliable to take the class on the topic ${topicName} which was booked on ${bookedTime}.<br/><br/>
    <b>Class is cancelled.<br/><br/></b>
    you will get a next class reminder shortly<br/><br/>
    Yours sincerely,<br/>
    Ascend Now Team<br/>
    All the best for the new Course.<br/>
    </p>
    `
    

    return { subject, html };
}

exports.OnNotificationToStudentOnTeacherAbscent = (teacher,student,topicName,ScheduleDateTime) => {
    const subject = `class has been cancelled  ${student.name}`;
    const html = `
    <p>Dear ${student.name}, <br/><br/>
    Since teacher ${teacher.name} unavailability for the class which was scheduled at ${ScheduleDateTime} for the topic ${topicName}. Class is cancelled.<br/><br/>
    <b>Class is cancelled.<br/><br/></b>
    Yours sincerely,<br/>
    Ascend Now Team   
    </p>
    `
    return { subject, html };
}

exports.oncancelOnStudentTeacherDintJoinTheClass = (teacher,student,topicName,ScheduleDateTime) => {
    const subject = `you dint join the class ${student.name}`;
    const html = `
    <p>Dear ${student.name}, <br/><br/>
    Since teacher ${teacher.name} unavailability and even you dint join the class which was scheduled at ${ScheduleDateTime} for the topic ${topicName}.<br/><br/>
    <b>Class is cancelled.<br/><br/></b>
    Yours sincerely,<br/>
    Ascend Now Team   
    </p>
    `
    return { subject, html };
}


exports.studentsParentWelcomeMailTemplate = (student, password) => {
    const subject = `Welcome Mail to Ascend Now ${student.name}'s parent`;
    const html = `
    <p> Dear  ${student.name}'s parent, <br/>
    Thank you for choosing Ascend Now!<br/><br/>

    To access the Ascend Now platform please visit: <a href="${process.env.CLIENT_BASE_URL}">login</a><br/><br/>

    Your login details are as follows:<br/>
    ● Username: <b>${student.secondary_email}</b><br/>
    ● Password: <b>${password}</b><br/><br/>

    Welcome to the community!<br/>
    Ascend Now Team
    </p>
    `

    return { subject, html };
}