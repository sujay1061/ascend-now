const { Student } = require("../../models");
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

/**
 * @param { batch_id }
 */

_getStudentsByDeliveryChannel = async (channel) => {
    return await Student.findAll({ where: { delivery_channel: channel, is_deleted: false }});
}

module.exports = {
    _getStudentsByDeliveryChannel
}