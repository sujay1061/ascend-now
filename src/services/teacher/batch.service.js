const { BatchStudent, Student, Batch, ClassAttendance, CourseScheduled } = require("../../models");
const Sequelize = require('sequelize');
const { getSignedUrlFromS3 } = require("../../_helpers/aws_s3_file_upload");
const Op = Sequelize.Op;

/**
 * @param { batch_id }
 */
_getAllStudentsByBatchId = async (req, res, next) => {
    if(req.user) {
        const batchStudents = await BatchStudent.findAndCountAll({
            where: { batch_id: req.params.id },
            include: [{
                model: Student
            }]
        });
        const attendances = await ClassAttendance.findOne({
            include: [ { model: CourseScheduled, where: { batch_id: req.params.id }}]
        })
        batchStudents.attendanceExist = attendances ? true : false;

        const batchStudentsWithSignedUrl = [];
        await batchStudents.rows.forEach(async (batch) => {
            batch.student.imageUrl = batch.student.imageUrl ? await getSignedUrlFromS3(batch.student.imageUrl) : batch.student.imageUrl;
            await batchStudentsWithSignedUrl.push(batch);
        });
        return await batchStudentsWithSignedUrl;
    } else {
        throw "Invalid token, please login again";
    }
}

module.exports = {
    _getAllStudentsByBatchId
}