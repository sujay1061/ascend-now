const { TeacherTopicFeedback, Student, TopicAssignment, Topic, ClassActivity, ClassAttendance, Teacher, CourseScheduled, StudentPoint, Account } = require("../../models");
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const { generatePdf } = require("../../_helpers/pdfCreator");
const { studentFeedback, sendViewReportMail } = require("../../notification/job-processors/teacher/teacher.processor");

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { TOKEN_SECRET, REFRESH_TOKEN_SECRET, REFRESH_TOKEN_LIFE, TOKEN_LIFE } = process.env;

/**
 * @param { batch_id }
 */
_getAllTopicFeedbacks = async (req, res, next) => {
    if(req.user) {
        return await TeacherTopicFeedback.findAndCountAll({});
    } else {
        throw "Invalid token, please login again";
    }
}

_createTopicFeedback = async (req) => {
    if(req.user) {
        var studentPerformance=req.body;
        req.body.teacher_account_id = req.user.id;
     


        const { topic_id, student_account_id, teacher_account_id ,comment} = req.body;
    
            var courseScheduled = await ClassAttendance.findOne({
                where: { topic_id: topic_id, student_id:student_account_id },
                include: [
                    { model: CourseScheduled },
                 
                ]
            });

        const student = await Student.findOne({ where: { id: student_account_id }});
        const teacher = await Teacher.findOne({ where: { id: teacher_account_id }});
        const topic = await Topic.findOne({ where: { id: topic_id }});
        const studentPoints= await StudentPoint.findAll({where:{student_account_id:student_account_id}});
        var userAccount = await Account.findOne({
            where: { email: student.primary_email, is_deleted: false },
          });

        var TotalPoints=0;
        studentPoints.forEach(studentId =>{
            TotalPoints=TotalPoints+studentId.points
        })
       
        var now=new Date(courseScheduled.class_scheduled.schedule_datetime)
        var date = now.toLocaleDateString();
        // var time = now.toLocaleTimeString();
        
        if(!student) {
            throw "Student selected not found";
        } else if(!topic) {
            throw "Topic selected not found";
        } else {
            const feedback = await TeacherTopicFeedback.findOne({ 
                where: { topic_id, student_account_id, teacher_account_id }
            });
            if(feedback) {
                throw "Feedback already submitted";
            } else {
               
                // var studentPdfCreationDeatils={
                //     studentPerformance,
                //     student,
                //     topic,
                //     teacher,
                //     date,
                //     TotalPoints,
                //     comment
                    
                 
                // }
            //    await generatePdf(studentPdfCreationDeatils)
            var createdTeacherTopicFeedback=  await TeacherTopicFeedback.create(req.body);
               createdTeacherTopicFeedback = createdTeacherTopicFeedback.toJSON();
               userAccount=userAccount.toJSON()
               
               const token = jwt.sign({
                   id:userAccount.id,
                teacherTopicFeedback_id: createdTeacherTopicFeedback.id,
                student_id: createdTeacherTopicFeedback.student_account_id,
                topic_id:createdTeacherTopicFeedback.topic_id,
                teacher_id:createdTeacherTopicFeedback.teacher_account_id
              }, TOKEN_SECRET);

              sendViewReportMail(student,teacher,topic,token)
              return createdTeacherTopicFeedback;

            
                
            }
        }
    } else {
        throw "Invalid token, please login again";
    }
}

_getTeacherTopicFeedbackByStudentCredentials = async (req, res, next) => {
    if(req.user) {
        let indexIs;
        student_account_id = req.user.student_id;
        topic_id = req.user.topic_id;
        teacher_account_id = req.user.teacher_id;
        studentPassword = req.params.studentPassword
        var courseScheduled = await ClassAttendance.findOne({
            where: { topic_id: topic_id, student_id:student_account_id },
            include: [
                { model: CourseScheduled },
             
            ]
        });
    let attendances = await ClassAttendance.findAndCountAll({
        where: { student_id:  req.user.student_id },
        include: [
            {
                model: CourseScheduled,
                required: true,
                where: { teacher_account_id: req.user.teacher_id, is_completed: true },
                include: [
                    {
                        model: Topic,
                        required: true,
                    },
                ]
            }
        ]
    });
    await attendances.rows.forEach(async (attendance, index) => {
        if(attendance.class_scheduled.topic.id==req.user.topic_id) {
            console.log(attendance.class_scheduled.topic.id)
            console.log(index+1) 
            indexIs=index+1

        }
    });
       
    TotalTopicCovered=attendances.count;
        const student = await Student.findOne({ where: { id: student_account_id }});
        const teacher = await Teacher.findOne({ where: { id: teacher_account_id }});
        const topic = await Topic.findOne({ where: { id: topic_id }});
        const studentPoints= await StudentPoint.findAll({where:{student_account_id:student_account_id}});
        var TotalPoints=0;
        studentPoints.forEach(studentId =>{
            TotalPoints=TotalPoints+studentId.points
        })
       
        var now=new Date(courseScheduled.class_scheduled.schedule_datetime)
        var date = now.toLocaleDateString();
        var userAccount = await Account.findOne({
            where: { id: req.user.id, is_deleted: false }
          });
          userAccount = userAccount.toJSON();
          const validPass =  await bcrypt.compare(studentPassword, userAccount.password);
        if(validPass)
        {
            let feedback = await TeacherTopicFeedback.findOne({ 
                where: { topic_id, student_account_id}
            });

             feedback = feedback.toJSON();
            feedback.students= student;
            feedback.teachers= teacher;
            feedback.topics= topic;
            feedback.TotalPoints= TotalPoints;
            feedback.date= date;
            feedback.TotalTopicCoveredCount=TotalTopicCovered;
            feedback.currectTopicCount=indexIs
            return feedback
        }  
        else
        {
            throw "Invalid password";
        }
    } else {
        throw "Invalid token, please login again";
    }
}

module.exports = {
    _getAllTopicFeedbacks,
     _createTopicFeedback,
     _getTeacherTopicFeedbackByStudentCredentials
}