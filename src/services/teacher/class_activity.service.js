const { ClassActivity } = require("../../models");

_getAllActivitesByTopicId = async (req) => {
    return await ClassActivity.findAll({
        where: { topic_id: req.params.id }
    })
}

module.exports = {
    _getAllActivitesByTopicId
}