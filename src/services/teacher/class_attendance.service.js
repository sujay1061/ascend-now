const { ClassAttendance, CourseScheduled, Topic, TopicAssignment,
    FeedbackQuestion,BatchFeedback, StudentPoint, Batch, Course } = require("../../models");

/** 
 * { topic_id, course_scheduled_id, attendance: [ { student_id, attended }, { student_id, attended } ] }
 */
_createNewClassAttendance = async (req) => {
    const { course_scheduled_id, topic_id, attendance } = req.body;
    const courseSchedule = await CourseScheduled.findOne({ where: { id: course_scheduled_id } });
    const classAttendance = await ClassAttendance.findOne({ 
        where: { course_scheduled_id, attended: true }
    });
    const topic = await Topic.findOne({ where: {id: topic_id }});

    if (classAttendance) {
        throw "Attendance already submitted for this class";
    } else if (!courseSchedule) {
        throw "Selected Course Scheduled not found";
    } else if(!topic) {
        throw "Topic selected not found";
    } else if (!attendance) {
        throw "Attendance is required";
    } else {
        const createAttendance = [];
        attendance.forEach(({ student_id, attended }) => {
            createAttendance.push({
                course_scheduled_id,
                topic_id,
                student_id,
                attended
            })
        });
        return await ClassAttendance.bulkCreate(createAttendance);
    }
}

_addMarksToStudentByTopic = async (req) => {
    var attendance = await ClassAttendance.findOne({
        where: {
            student_id: req.body.student_id,
            topic_id: req.body.topic_id
        },
        include: [{
            model: Topic,
            include: [ { model: TopicAssignment } ],
        }, { model: CourseScheduled, include: [
            { model: Course },
            { model: Batch }
        ] }]
    });

    if (attendance) {
        attendance = attendance.toJSON();
        if(attendance.topic.topic_assignment.max_marks >= req.body.marks) {
            const points = {
                points: req.body.points,
                title: `${attendance.class_scheduled.course.name}-${attendance.topic.name}(${attendance.class_scheduled.batch.name})`,
                student_account_id: req.body.student_id,
                teacher_account_id: req.user.id
            };
            return await Promise.all([
                ClassAttendance.update({ marks: req.body.marks, comments:req.body.comments },  { where: { id: attendance.id }, returning: true, plain: true }),
                StudentPoint.create(points)
            ]);
        } else {
            throw `Marks provided should be less than ${attendance.topic.topic_assignment.max_marks}`;
        }
    } else {
        throw "Attendance not found";
    }
}

_getAllfeedbackQuestion = async (request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    return await FeedbackQuestion.findAndCountAll({
        where: (queryParams && search) ? { questions: { [Op.like]: `%${search}%` }, is_deleted: false } 
        : {is_deleted: false},
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
  
}

_addStudentFeedBack = async (feedbackDataForStudent) => {
    if(req.user) {
        feedbackDataForStudent.account_id=req.user.id
        return await BatchFeedback.create(feedbackDataForStudent);
    } else {
        throw "Invalid token, please login again";
    }
}

_addAssignmentFeedback = async (req) => {
    const { assignment_feedback } = req.body;
    var attendance = await ClassAttendance.findOne({
        where: { id: req.body.attendance_id },
        include: [{ 
            model: CourseScheduled,
            where: { teacher_account_id: req.user.id }
        }]
    });
    attendance = attendance.toJSON();
    if(attendance) {
        if(attendance.url) {
            return await ClassAttendance.update({ assignment_feedback }, { where: { id: req.body.attendance_id }, returning: true, plain: true});
        } else {
            throw "Assignment is not submitted yet";
        }
    } else {
        throw "Attendance not found";
    }
}

  

module.exports = {
    _createNewClassAttendance,
    _addMarksToStudentByTopic,
    _getAllfeedbackQuestion,
    _addStudentFeedBack,
    _addAssignmentFeedback
}