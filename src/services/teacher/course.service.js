const { Course } = require("../../models");

_getAllCourses = async (req) => {
    return await Course.findAll({
        order: [
            ['createdAt', 'DESC']
        ],
    });
}

module.exports = {
    _getAllCourses
}