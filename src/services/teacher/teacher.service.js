const { Teacher } = require('../../models');
const { getSignedUrlFromS3 } = require('../../_helpers/aws_s3_file_upload');

_updateTeacherAccountById = async (req) => {
    var teacherAccount = await Teacher.findOne({ where: { id: req.user.id }});
    teacherAccount = teacherAccount && teacherAccount.toJSON();
    const file = req.file
    req.body.imageUrl = file ? file.location : teacherAccount.imageUrl;
    await Teacher.update(req.body, { where: { id: req.user.id }, returning: true, plain: true });
    var teacher = await Teacher.findOne({ where: { id: req.user.id }});
    teacher = teacher.toJSON();
    teacher.imageUrl = await getSignedUrlFromS3(teacher.imageUrl);
    return await teacher;
}

module.exports = {
    _updateTeacherAccountById
}