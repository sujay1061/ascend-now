const { BookedClasses, BookedClassTimeslot, Student, Teacher, Topic, Course, AccountHasRole, Account,Role, StudentPoint, CourseScheduled ,Batch, BatchStudent,ClassAttendance} = require("../../models");
const { getLiveWebinarAccessTokens, createLiveWebinarBookedClass,createLiveWebinarClass, getLiveWebinarWidgerRecording , getAttendeeDetails } = require("../../_helpers/live_webinar");
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const { sendClassRecordingWidgetMail,postLessonUpdateEmail,confirmTheBookedClassMail , cancellingTheBookedClassMail, teacherBooksClass, teacherReminderMailInMinutes, teacherYouWereAbscent,cancelOnTeacherDintJoinTheClass,teacherBooksClassTeacherMail} = require("../../notification/job-processors/teacher/teacher.processor");
const {studentReminderMailInMinutes, teacherIsAbscent,cancelOnStudentDintJoinTheClass} = require("../../notification/job-processors/student/student.processor");
const { notificationToAdminTeacherIsAbscent} = require("../../notification/job-processors/admin/admin.processor");
const schedule = require('node-schedule');
const { getMultipleSignedUrlFromS3, getSignedUrlFromS3 } = require("../../_helpers/aws_s3_file_upload");
_getMyBookedClasses = async (req) => {
    if (req.user) {
        return await BookedClasses.findAndCountAll({
            where: { teacher_account_id: req.user.id, is_deleted: false },
            include: [
                { model: BookedClassTimeslot },
                { model: Student },
                { model: Teacher },
                { model: Topic, include: [{ model: Course }] }
            ],
            order: [
                [ {model: BookedClassTimeslot},'id','DESC']
                 ]
        });
    } else {
        throw "Invalid token, please login again";
    }
}

_rejectBookedClass = async (req) => {
    if (req.user) {
        const { comment, booked_id } = req.body;
        var bookedClass = await BookedClasses.findOne({
            where: {
                id: booked_id,
                is_deleted: false,
                teacher_account_id: req.user.id
            },
            include: [{
                model: BookedClassTimeslot
            },
            { model: Student }]
        });
        if (bookedClass) {
            bookedClass = bookedClass.toJSON();
            const { is_accepted, is_rejected, is_cancelled } = bookedClass;
            if (is_accepted || is_rejected || is_cancelled) {
                throw `Booked class is already ${is_accepted ? 'Accepted' : is_rejected ? 'Rejected' : is_cancelled ? 'Cancelled' : 'Processed'}`;
            } else {
                var rejectedClass = await BookedClasses.update(
                    { comment, is_requested: false, is_rejected: true },
                    { where: { id: booked_id }, returning: true, plain: true },
                );
                rejectedClass[1] = rejectedClass[1].toJSON();
                rejectedClass[1].booked_class_timeslots = bookedClass.booked_class_timeslots;
                cancellingTheBookedClassMail(bookedClass.student)
                return rejectedClass[1];
            }
        } else {
            throw "Booked Class not found";
        }
    } else {
        throw "Invalid token, please login again";
    }
}

_getMyConfirmedClasses = async (req) => {
    if (req.user) {
        let { year, month, pageSize, pageNumber } = req.query;
        if(year==undefined && month==undefined)
        {
            var confirmedClasses = await BookedClasses.findAndCountAll({
                where: { teacher_account_id: req.user.id, is_accepted: true, is_deleted: false },
                include: [
                    { model: BookedClassTimeslot },
                    { model: Student },
                    { model: Teacher },
                    { model: Topic, include: [{ model: Course }] }
                ],
                  order: [
                    [ {model:  BookedClassTimeslot},'id','ASC']
                     ]
            });
          await  confirmedClasses.rows.forEach(async (classes) =>{
                var urls = await getMultipleSignedUrlFromS3([classes.student.imageUrl, classes.teacher.imageUrl]);
                classes.student.imageUrl = urls[0];
                classes.teacher.imageUrl = urls[1];
            })   
            return await confirmedClasses
        }
        else
        {
            monthnew = parseInt(month)+1
            monthnew=monthnew.toString()
            const startDate = `${year}-${month}-01`;
            const endDate = `${year}-${monthnew}-01`;

            var confirmedClasses = await BookedClasses.findAndCountAll({
                where: { teacher_account_id: req.user.id, is_accepted: true, is_deleted: false },
                include: [
                    { model: BookedClassTimeslot,
                        where:{
                            start_date: 
                            { [Op.gte]: new Date(startDate), [Op.lt]: new Date(endDate) }
                        }
                    },
                    { model: Student },
                    { model: Teacher },
                    { model: Topic, include: [{ model: Course }] }
                ],
                  order: [
                    [ {model:  BookedClassTimeslot},'id','ASC']
                     ]
            });
          await  confirmedClasses.rows.forEach(async (classes) =>{
                var urls = await getMultipleSignedUrlFromS3([classes.student.imageUrl, classes.teacher.imageUrl]);
                classes.student.imageUrl = urls[0];
                classes.teacher.imageUrl = urls[1];
            })   
            return await confirmedClasses
        }

        
       
    } else {
        throw "Invalid token, please login again";
    }
}

_bookClassWithStudent = async (req) => {
    if (req.user) {
        var startDate_scheduler;
        const { topic_id, student_account_id, timeslots, hours, comment } = req.body;
        const topic = await Topic.findOne({ where: { id: topic_id } });
        const student = await Student.findOne({ where: { id: student_account_id } });
        const teacher = await Teacher.findOne({ where: { id: req.user.id } });
        const bookedClassesFound =await BookedClasses.findOne({ where: { student_account_id: student_account_id, topic_id : topic_id, is_cancelled:false } });
        if(bookedClassesFound)
        {
            throw "Student has already taken up with this topic";
        }
        else
        {
        if (!student) {
            throw "Selected student not found";
        } else if (!topic) {
            throw "Selected topic not found";
        } else {
            const start_date = new Date(timeslots['1'].start_date);
            const start_date_format = new Date(start_date);
            const minutes = hours == 2 ? 120 : hours == 1.5 ? 90 : 60;
            const end_date_format = start_date_format.setMinutes(start_date_format.getMinutes() + minutes);
            const end_date = new Date(end_date_format);
            const checkForTimeslot = await checkForBookedTimeslot(req, true);
            if (!checkForTimeslot) {
                const checkForStudentTimeslot = await checkForBookedTimeslot(req, false);
                if (!checkForStudentTimeslot) {
                    req.body.teacher_account_id = req.user.id;
                    req.body.created_by_account_id = req.user.account_id;
                    req.body.is_accepted = true;
                    req.body.is_requested = false;
                    req.body.booked_by = 'teacher';
                    return await BookedClasses.create(req.body)
                        .then(async (classes) => {
                            const timeslotPromises = [];
                            classes = classes.toJSON();
                            await Object.keys(timeslots).map(async (timeslot, index) => {
                                const timeslotObj = {
                                    slot_no: index + 1,
                                    start_date: start_date,
                                    end_date: end_date,
                                    booked_id: classes.id,
                                    is_confirmed: true,
                                    hours,
                                    created_by_account_id: req.user.id,
                                    date_time_booked:req.body.date_time_booked
                                }
                                await timeslotPromises.push(timeslotObj);
                                startDate_scheduler=timeslotObj.start_date
                            })
                            req.body.booked_class_id=classes.id;
                            req.body.delivery_channel='B2C';
                            req.body.batchName=`B2C`+student_account_id+req.user.id+req.body.topic_id;
                            req.body.scheduled_name=req.body.name;
                            req.body.schedule_datetime=startDate_scheduler;
                            req.body.teacher_account_id=req.user.id;
                            batchCreation={
                                name:req.body.batchName,
                                description:req.body.name,
                                location:{"address":"banagalore","pincode":560063},
                                students:[student_account_id]
                            }
                            return await BookedClassTimeslot.bulkCreate(timeslotPromises)
                            .then(async (booked_timeslots) => {
                                
                                classes.booked_timeslots = booked_timeslots;
                                return await Batch.create(batchCreation)
                                .then(async(batchDeatils)=>{
                                    const students = batchCreation.students;
                                    let createStudents = [];
                                    await students.forEach(async studentId => {
                                        const newStudentBatch = {
                                            student_account_id: studentId,
                                            batch_id: batchDeatils.id,
                                            marks: 0.00,
                                            marks_date: new Date()
                                        }
                                        createStudents.push(newStudentBatch);
                                    });
                                    await BatchStudent.bulkCreate(createStudents);
                                        const courseScheduleDeatils={
                                            schedule_datetime:startDate_scheduler,
                                            teacher_account_id:req.user.id,
                                            scheduled_name:req.body.name,
                                            course_id:teacher.course_id,
                                            topic_id:req.body.topic_id,
                                            batch_id:batchDeatils.id,
                                            createdAt:new Date(),
                                            updatedAt:new Date(),
                                            delivery_channel:"B2C",
                                            booked_class_id:req.body.booked_class_id

                                        }
                                    return await CourseScheduled.create(courseScheduleDeatils)
                                    .then(async (courseScheduleClass) => {
                                        start_date_found = new Date(start_date);
                                        scheduleDate = new Date(start_date_found-15*60000);
                                        const job = schedule.scheduleJob(scheduleDate, function () {
                                            teacherReminderMailInMinutes(teacher,student,topic.name)
                                            studentReminderMailInMinutes(teacher,student,topic.name)
                                        });

                                        // scheduleEndTime = new Date(end_date_format);
                                        console.log(scheduleEndTime+ " time to end ")
                                        var dateFound_mins = new Date(req.body.schedule_datetime);
                                   var     scheduleEndTime = new Date(dateFound_mins.getTime() + 5*60000);
                                let bothTeacherAndStudentAbsecent=0;
                                        const endClass = schedule.scheduleJob(scheduleEndTime,async function () {
                                          var bookedClassData = await BookedClasses.findOne({ where: { id: classes.id } });
                                          if(bookedClassData.presenter==null)
                                          {
                                            //   console.log("came to cancellation")
                                            //   bothTeacherAndStudentAbsecent=1;
                                              cancelOnStudentDintJoinTheClass(teacher,student,topic.name,start_date_found)
                                              cancelOnTeacherDintJoinTheClass(teacher,student,topic.name,start_date_found)
                                              BookedClasses.update({is_cancelled:true}, { where: { id: classes.id }});
                                          }
                                        //   if(bothTeacherAndStudentAbsecent==0)
                                        // {
                                        //     var     scheduleEndTime = new Date(start_date_found.getTime() + 5*60000);
                                        //     console.log(scheduleEndTime)
                                        //     const jobs = schedule.scheduleJob(scheduleEndTime, async function () {
                                        //      let  teacherPresent=await ClassAttendance.findAll({
                                        //             where: { course_scheduled_id: courseScheduleClass.id }
                                        //         })
                                        //         console.log(teacherPresent.length)
                                        //         if(teacherPresent.length==0)
                                        //         {
                                        //             teacherYouWereAbscent(teacher,student,topic.name,start_date_found)
                                        //             teacherIsAbscent(teacher,student,topic.name,start_date_found)
                                        //             notificationToAdminTeacherIsAbscent(teacher,student,topic.name,start_date_found)
                                        //             var completedClass =   await BookedClasses.update({is_cancelled:true}, { where: { id: classes.id }, returning: true, plain: true});
                                        //             var completedClassInfo = completedClass[1].toJSON();
                                        //             return await getLiveWebinarAccessTokens()
                                        //             .then(async (responseObj) => {
                                        //                 return await getLiveWebinarWidgerRecording(responseObj, completedClass[1].widget_id)
                                        //                     .then(async (response) => {
                                        //                         const { data, error } = response;
                                        //                         if (error) {
                                        //                             throw error.message;
                                        //                         } else {
                                        //                             completedClassInfo.recording_url = data.length > 0 ? data[0].url : null;
                                        //                             // adminUser = adminUser.toJSON();
                                        //                             // data.length && sendClassRecordingWidgetMail(completedClassInfo, adminUser);
                                        //                             return await completedClassInfo;
                                        //                         //   return classes
                                        //                         }
                                        //                     }) .catch((error) => {
                                        //                         throw error;
                                        //                     })
                                        //             })
                                        //             .catch((error) => {
                                        //                 throw error;
                                        //             })

                                        //         }
                                        //     });
                                        // }

                                        });
                                        

                                        

                                        teacherBooksClass(student,teacher,topic, req.body.date_time_booked,comment)
                                        teacherBooksClassTeacherMail(student,teacher,topic, req.body.date_time_booked,comment)
                                        return await classes;
                                    })
                                    .catch(async(error)=>{
                                        await CourseScheduled.destroy({ where: { booked_class_id: classes.id }, force: true })
                                    })
                                })
                                .catch(async(error)=>{
                                    await Batch.destroy({ where: { course_id: teacher.course_id,topic_id:req.body.topic_id }, force: true })
                                })



                            })
                            .catch(async (error) => {
                                await BookedClasses.destroy({ where: { id: classes.id }, force: true })
                                throw error;
                            });
                           
                           
                        })
                        .catch((error) => {
                            throw error;
                        });
                } else {
                    throw "You already have a class with this Student for the Timeslot";
                }
            } else {
                throw "This timeslot is already booked for the student";
            }
        }
    }
    } else {
        throw "Invalid token, please login again";
    }
}

async function checkForBookedTimeslot(req, is_accepted) {
    const { student_account_id, timeslots, hours } = req.body;
    const start_date = new Date(timeslots['1'].start_date);
    const start_date_format = new Date(start_date);
    const minutes = hours == 2 ? 120 : hours == 1.5 ? 90 : 60;
    const end_date = new Date(start_date_format.setMinutes(start_date_format.getMinutes() + minutes));
    const whereClause = is_accepted ? { student_account_id, is_deleted: false, is_accepted: true } : { teacher_account_id: req.user.id, is_deleted: false, student_account_id };
    return await BookedClasses.findOne({
        where: whereClause,
        include: [{
            model: BookedClassTimeslot,
            required: true,
            where: {
                [Op.or]: [
                    {
                        start_date: {
                            [Op.gte]: start_date,
                            [Op.lte]: end_date
                        },
                        end_date: {
                            [Op.gte]: start_date,
                            [Op.lte]: end_date
                        }
                    }
                ]
            }
        }]
    });
}

_getBookedClassesByDate = async (req) => {
    if (req.user) {
        let { year, month, day } = req.query;
        let nextDate = parseInt(day) + 1
        const dateMentioned = `${year}-${month}-${day}`;
        const nextDateFromDateMentioned = `${year}-${month}-${nextDate}`;
        if(day==31)
        {
            let nextMonth = parseInt(month) + 1
            nextDate=1;
            const nextDateFromDateMentionedEndOfMonth = `${year}-${nextMonth}-${nextDate}`;
            return await BookedClasses.findAndCountAll({
                where: { teacher_account_id: req.user.id, is_accepted: true, is_deleted: false },
                include: [{
                    model: BookedClassTimeslot,
                    where: { start_date: { [Op.gte]: dateMentioned, [Op.lt]: nextDateFromDateMentionedEndOfMonth } }
                },
                {
                    model: Topic,
                    include: [{ model: Course }]
                }],
            });
        }



        return await BookedClasses.findAndCountAll({
            where: { teacher_account_id: req.user.id, is_accepted: true,is_deleted: false },
            include: [{
                model: BookedClassTimeslot,
                where: { start_date: { [Op.gte]: dateMentioned, [Op.lt]: nextDateFromDateMentioned } }
            },
            {
                model: Topic,
                include: [{ model: Course }]
            }],
        });
    }
    else {
        throw "Invalid token, please login again";
    }
}

_fetchNextBookedClass = async (req, res, next) => {
    currentUtc = new Date();
    var dateFound_mins = new Date(currentUtc);
    var     scheduleEndTime = new Date(dateFound_mins.getTime() + 60*60000);   
    if (req.user) {
        return await BookedClasses.findAll({
            where: {
                teacher_account_id: req.user.id, 
                is_deleted: false,
                is_completed:false,
                is_rejected:false,
                is_cancelled:false,
                is_accepted:true
            },
            include: [{
                model: BookedClassTimeslot,
                required: true,
                where: {
                    [Op.or]: [
                        {
                            start_date: {
                                [Op.gte]: currentUtc,
                             
                            }
                        },
                        {
                            end_date: {
                                [Op.lte]: scheduleEndTime,
                                
                            }
                        }
                    ],
                     is_confirmed: true},  
            }],
            order: [
                [ {model: BookedClassTimeslot},'start_date','ASC']
                 ]
        }).then(async (courses) => {
            if (courses.length > 0) {
                courses[0] = courses[0] && courses[0].toJSON();
                const attendances = await ClassAttendance.findOne({
                    include: [{
                        model: CourseScheduled,
                        where: { booked_class_id: courses[0].id, topic_id: courses[0].topic_id }
                    }]
                });
                courses[0].attendanceExist = attendances ? true : false;
                return await courses[0];
            } else {
                throw "No classes found"
            }
        }).catch((error) => {
            throw error;
        });
      
       
      
    } else {
        throw "Invalid token, please login again";
    }
}

_startMyBookedClass = async(req,res,next) => {
    if(req.user)
    {
        var bookedClasses = await BookedClasses.findOne({
            where: { id: req.params.id, teacher_account_id: req.user.id },
            include: [{ model: BookedClassTimeslot},{ model: Topic }, { model: Teacher }, { model: CourseScheduled }]
        });

        if(bookedClasses)
        {
            bookedClasses = bookedClasses.toJSON();
            if (bookedClasses.teacher.delivery_channel === 'B2C') 
            {
                if (bookedClasses.presenter!=null && bookedClasses.attendee!=null) {
                    bookedClasses.batch_id = bookedClasses.class_scheduled.batch_id
                    return await bookedClasses;
                }
                else
                {
                // return bookedClasses.topic.duration_in_mins;
                return await getLiveWebinarAccessTokens()
                        .then(async (responseObj) => {
                            bookedClasses.booked_class_timeslots[0].name=bookedClasses.name
                            bookedClasses.booked_class_timeslots[0].duration_in_mins=bookedClasses.topic.duration_in_mins
                            return await createLiveWebinarBookedClass(responseObj, bookedClasses.booked_class_timeslots[0])
                                .then(async (response) => {
                                    const { data, error } = response;
                                    if (error) {
                                        throw error.message;
                                    } else {
                                        const updateDetails = {
                                            attendee: data.hosted_at.attendee,
                                            presenter: data.hosted_at.presenter,
                                            moderator: data.hosted_at.moderator,
                                            host: data.hosted_at.host,
                                            phone_attendee: data.hosted_at.phone_attendee,
                                            phone_presenter: data.hosted_at.phone_presenter,
                                            widget_id: data.id
                                        }
                                        
                                        var bookedClassUpdated = await BookedClasses.update(updateDetails, { where: { id: req.params.id }, returning: true, plain: true });
                                        bookedClassUpdated = bookedClassUpdated[1].toJSON();
                                        bookedClassUpdated.class_scheduled= bookedClasses.class_scheduled
                                        bookedClassUpdated.batch_id = bookedClasses.class_scheduled.batch_id
                            
                                        return await bookedClassUpdated;
                                    }
                                })
                                .catch((error) => {
                                    throw error;
                                })
                        })
                        .catch((error) => {
                            throw error;
                        })
                    }
            }
        }
        else {
            throw "Scheduled Class not found";
            } 
    }
    else {
        throw "Invalid token, please login again";
    }
}

_finishBookedClass = async (req, res, next) => {
    var bookedClasses = await BookedClasses.findOne({
        where: { id: req.params.id },
        include: [
            { model: Teacher },
            { model: Topic ,  include: [{
                model: Course
            }]},
            { model: Student }
        ]
    });

    var adminUser = await Account.findOne({
        where: { is_deleted: false },
        include: [{
            model: AccountHasRole,
            required: true,
            include: [{
                model: Role,
                required: true,
                where: { name: 'admin' }
            }]
        }]
    })
    if (bookedClasses) {
        bookedClasses = bookedClasses.toJSON();
        if (!bookedClasses.is_completed) {
           
                    const points = {
                        points: 100,
                        title:  `${bookedClasses.topic.course.name}-${bookedClasses.topic.name}`,
                        student_account_id: bookedClasses.student.id,
                        teacher_account_id: bookedClasses.teacher.id,
                        created_by_account_id: req.user.id
                    }
                
                await StudentPoint.create(points);
             
                var completedClass = await BookedClasses.update({ is_completed: true }, { where: { id: req.params.id }, returning: true, plain: true });
                var couseSchedule_Class = await CourseScheduled.update({ is_completed: true }, { where: { booked_class_id: req.params.id }, returning: true, plain: true });
                var completedClassInfo = completedClass[1].toJSON();
                if(bookedClasses.teacher.delivery_channel === 'B2C') {
                    return await getLiveWebinarAccessTokens()
                    .then(async (responseObj) => {
                        return await getLiveWebinarWidgerRecording(responseObj, completedClass[1].widget_id)
                            .then(async (response) => {
                                const { data, error } = response;
                                if (error) {
                                    throw error.message;
                                } else {
                                    completedClassInfo.recording_url = data.length > 0 ? data[0].url : null;
                                    adminUser = adminUser.toJSON();
                                    data.length && sendClassRecordingWidgetMail(completedClassInfo, adminUser);
                                    return await completedClassInfo;
                        
                                }
                            }) .catch((error) => {
                                throw error;
                            })
                        // return await getAttendeeDetails(responseObj,completedClassInfo.widget_id)
                        // .then(async (response) => {
                        //     // return response
                        //     const { data, error } = response;
                        //         if (error) {
                        //             throw error.message;
                        //         } else {
                        //             postLessonUpdateEmail(bookedClasses)
                        //             return await data;
                        //         } 
                        // })
                        //     .catch((error) => {
                        //         throw error;
                        //     })
                    })
                    .catch((error) => {
                        throw error;
                    })
                } else {
                    return completedClassInfo;
                }
            // } 
            // else {
            //     const scheduledDateTimeFormat = new Date(courseScheduled.schedule_datetime);
            //     const scheduledDateTime = new Date(scheduledDateTimeFormat.setMinutes(scheduledDateTimeFormat.getMinutes() + 30));
            //     const todayDate = new Date();
            //     if(scheduledDateTime >= todayDate) {
            //         const cancelledCourse = await CourseScheduled.update({ is_cancelled: true }, { where: { id: req.params.id }, returning: true, plain: true });
            //         return cancelledCourse[1];
            //     } else {
            //         throw "Can't finish the Class before 30 minutes of class start";
            //     }
            // }
        } else {
            throw "Class Already Completed";
        }
    } else {
        throw "Scheduled Class not found";
    }
}

_confirmBookedClass = async (req) => {
    if (req.user) {
        var schedule_date_time;
        var courseScheduleClassDetails
        const { confirmed_slot, booked_id, hours } = req.body;
        var bookedClass = await BookedClasses.findOne({
            where: {
                id: booked_id,
                is_deleted: false,
                teacher_account_id: req.user.id
            },
            include: [{
                model: BookedClassTimeslot,
                required: true,
                where: { id: confirmed_slot }
            },
                { model: Student },
                { model: Teacher }
            ]
        });
        const topic = await Topic.findOne({ where: { id: bookedClass.topic_id } });
// return bookedClass;
        if (bookedClass) {
            bookedClass = bookedClass.toJSON();
            var { is_accepted, is_rejected, is_cancelled, booked_class_timeslots } = bookedClass;
            var { start_date, end_date } = booked_class_timeslots[0];
            const end_date_format = new Date(start_date);
            const minutes = hours == 2 ? 120 : hours == 1.5 ? 90 : 60;
            end_date = new Date(end_date_format.setMinutes(end_date_format.getMinutes() + minutes));

            if (is_accepted || is_rejected || is_cancelled) {
                throw `Booked class is already ${is_accepted ? 'Accepted' : is_rejected ? 'Rejected' : is_cancelled ? 'Cancelled' : 'Processed'}`;
            } else {
                const checkForTimeslot = await BookedClasses.findOne({
                    where: { teacher_account_id: req.user.id, is_deleted: false, is_accepted: true },
                    include: [{
                        model: BookedClassTimeslot,
                        required: true,
                        where: {
                            [Op.or]: [
                                {
                                    start_date: {
                                        [Op.gte]: start_date,
                                        [Op.lte]: end_date
                                    }
                                },
                                {
                                    end_date: {
                                        [Op.gte]: start_date,
                                        [Op.lte]: end_date
                                    }
                                }
                            ]
                        }
                    }]
                });
               
                if (checkForTimeslot) {
                    throw "You have already accepted a class for this Timeslot";
                } else {
                    req.body.booked_class_id=bookedClass.id;
                    req.body.delivery_channel='B2C';
                    req.body.batchName=`B2C`+bookedClass.student_account_id+bookedClass.teacher_account_id+bookedClass.topic_id;
                    req.body.scheduled_name=bookedClass.name;
                    req.body.schedule_datetime=booked_class_timeslots[0].start_date;
                    req.body.teacher_account_id=bookedClass.teacher_account_id;
                    schedule_date_time=booked_class_timeslots[0].start_date;
                  
                    return await BookedClasses.update(
                                { is_requested: false, is_accepted: true },
                                { where: { id: booked_id }, returning: true, plain: true },
                            )
                            .then(async(classes) => {
                                return  await BookedClassTimeslot.update(
                            {
                                is_confirmed: true,
                                hours,
                                end_date,
                                date_time_booked:req.body.date_time_booked
                            },
                            { where: { id: bookedClass.booked_class_timeslots[0].id }, returning: true, plain: true }
                             )
                             .then(async(booked_timeslots) => {
                                classes.booked_timeslots = booked_timeslots;
                                batchCreation={
                                    name:req.body.batchName,
                                    description:"confirmed classes",
                                    location:{"address":"banagalore","pincode":560063},
                                    students:[bookedClass.student_account_id]
                                }  
                                
                                return await Batch.create(batchCreation)
                                .then(async(batchDeatils) => {
                                    const students = batchCreation.students;
                                    let createStudents = [];
                                    await students.forEach(async studentId => {
                                        const newStudentBatch = {
                                            student_account_id: studentId,
                                            batch_id: batchDeatils.id,
                                            marks: 0.00,
                                            marks_date: new Date()
                                        }
                                        createStudents.push(newStudentBatch);
                                    });
                                    await BatchStudent.bulkCreate(createStudents);
                                    const courseScheduleDeatils={
                                        schedule_datetime:schedule_date_time,
                                        teacher_account_id:bookedClass.teacher_account_id,
                                        scheduled_name:bookedClass.name,
                                        course_id:bookedClass.teacher.course_id,
                                        topic_id:bookedClass.topic_id,
                                        batch_id:batchDeatils.id,
                                        createdAt:new Date(),
                                        updatedAt:new Date(),
                                        delivery_channel:"B2C",
                                        booked_class_id:req.body.booked_class_id

                                    }
                                    
                                    return await CourseScheduled.create(courseScheduleDeatils)
                                    .then(async (courseScheduleClass) => {
                                      
                                         courseScheduleClassDetails=courseScheduleClass;
                                  
                                    confirmTheBookedClassMail(bookedClass.student,bookedClass.teacher,req.body.date_time_booked,topic)
                                    dateFound_date = new Date(schedule_date_time);
                                    scheduleDate = new Date(dateFound_date-15*60000);
                                    const job = schedule.scheduleJob(scheduleDate, function () {
                                        teacherReminderMailInMinutes(bookedClass.teacher,bookedClass.student,topic.name)
                                        studentReminderMailInMinutes(bookedClass.teacher,bookedClass.student,topic.name)
                                    });
                                    
                                      return await courseScheduleClassDetails;
                                    })
                                    .catch(async(error)=>{
                                        await CourseScheduled.destroy({ where: { booked_class_id: bookedClass.id }, force: true })
                                    })
                                })
                                .catch(async(error)=>{
                                    console.log("-----------error----------")
                                    return bookedClass
                                   
                                })

                         })
                            })
                }
            }
        } else {
            throw "Booked Class not found";
        }
    } else {
        throw "Invalid token, please login again";
    }
}


_getAllBookedClasses = async (request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber, filter } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    var currentDate = new Date();
    var todayDate = new Date();
    var start = new Date();
    start.setUTCHours(0,0,0,0);
    
    let whereClause = { is_deleted: false };
    let includeClause = [
            { model: Student },
            { model: Teacher },
            { model: Topic, include: [{ model: Course }] }
    ]

    switch (filter) {
        case 'completed':
            whereClause.is_completed = true
            whereClause.teacher_account_id=request.user.id
            includeClause.push({ model: BookedClassTimeslot })
            break;
        case 'upcoming':
            whereClause = [
                { is_deleted: { [Op.is]: false }, is_completed: false, teacher_account_id:request.user.id, is_cancelled:false  }
            ];
            includeClause.push({ model: BookedClassTimeslot, where: Sequelize.where(Sequelize.fn('date', Sequelize.col('start_date')), '=', currentDate)})
            break;
        case 'future':
           
            whereClause.is_completed = false;
            whereClause.is_cancelled = false;
            whereClause.teacher_account_id=request.user.id;
            start.setDate(start.getDate() + 1);
            includeClause.push({ model: BookedClassTimeslot, where: {start_date : { [Op.gte]: start }} })
            // currentDate.setDate(currentDate.getDate() + 1);
            // console.log(currentDate)
            // console.log(todayDate)
            // whereClause.start_date = { [Op.gte]: currentDate }           
            // includeClause.push({ model: BookedClassTimeslot, where: {start_date : {  [Op.between]: [todayDate, currentDate], }} })
            break;
        default:
            break;
    }

    var bookedClasses = await BookedClasses.findAll({
        where: (queryParams && filter) ? whereClause : { is_deleted: false },
        include: includeClause,
        order: [
            ['createdAt', 'ASC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
    let dataObj = {};
    dataObj.count = bookedClasses.length;
    dataObj.rows = bookedClasses;
    dataObj.rows  = dataObj.rows.map(async(classes) => {
        classes = classes.toJSON();
        classes.student.imageUrl  = classes.student.imageUrl ? await getSignedUrlFromS3(classes.student.imageUrl):classes.student.imageUrl
        classes.teacher.imageUrl =  classes.teacher.imageUrl ? await getSignedUrlFromS3(classes.teacher.imageUrl):classes.teacher.imageUrl
        return await classes;
    });
    dataObj.rows  = await Promise.all(dataObj.rows );
    return await dataObj;
}

_getLatestBookings = async (req) => {
    if (req.user) {
        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();

        const startDate = `${year}-${month}-${day}`;
        

        return await BookedClasses.findAndCountAll({
            where: { teacher_account_id: req.user.id,  is_deleted: false },
            include: [
                { model: BookedClassTimeslot,
                    where:{
                        start_date: 
                        { [Op.gte]: new Date(startDate)}
                    }
                },
                { model: Student },
                { model: Teacher },
                { model: Topic, include: [{ model: Course }] }
            ],
              order: [
                [ {model:  BookedClassTimeslot},'start_date','ASC']
                 ]
        });


    } else {
        throw "Invalid token, please login again";
    }
}

_getPastBookings = async (req) => {
    if (req.user) {
        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();

        const startDate = `${year}-${month}-${day}`;
        return await BookedClasses.findAndCountAll({
            where: { teacher_account_id: req.user.id, is_deleted: false },
            include: [
                { model: BookedClassTimeslot,
                    where:{
                        start_date: 
                        { [Op.lt]: new Date(startDate)}
                    }
                },
                { model: Student },
                { model: Teacher },
                { model: Topic, include: [{ model: Course }] }
            ],
              order: [
                [ {model:  BookedClassTimeslot},'start_date','DESC']
                 ]
        });


    } else {
        throw "Invalid token, please login again";
    }
}

module.exports = {
    _getMyBookedClasses,
    _confirmBookedClass,
    _rejectBookedClass,
    _getMyConfirmedClasses,
    _bookClassWithStudent,
    _getBookedClassesByDate,
    _fetchNextBookedClass,
    _startMyBookedClass,
    _finishBookedClass,
    _getAllBookedClasses,
    _getLatestBookings,
    _getPastBookings
}