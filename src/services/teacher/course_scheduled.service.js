const { CourseScheduled, ClassAttendance, BatchStudent, Student, Account, Course, Teacher, Batch, Topic, TopicAssignment, StudentPoint, TeacherTopicFeedback, Institute, InstituteHasStudent, AccountHasRole, Role,Quiz, QuizQuestion,StudentQuizAnswer } = require("../../models");
const Sequelize = require('sequelize');
const { getSignedUrlFromS3, getMultipleSignedUrlFromS3 } = require("../../_helpers/aws_s3_file_upload");
const Op = Sequelize.Op;
const { getLiveWebinarAccessTokens, createLiveWebinarClass, getLiveWebinarWidgerRecording } = require("../../_helpers/live_webinar");
const moment=require('moment');
const { sendClassRecordingWidgetMail } = require("../../notification/job-processors/admin/admin.processor");
/** Class Finished --> update is_completed status in course_scheduled table
 * @param{ id }
 * @body { topic_id }
 */


_finishScheduledClass = async (req, res, next) => {
    var courseScheduled = await CourseScheduled.findOne({
        where: { id: req.params.id },
        include: [
            { model: ClassAttendance },
            { model: Teacher },
            { model: Topic },
            { model: Course },
            { model: Batch }
        ]
    });

    var adminUser = await Account.findOne({
        where: { is_deleted: false },
        include: [{
            model: AccountHasRole,
            required: true,
            include: [{
                model: Role,
                required: true,
                where: { name: 'admin' }
            }]
        }]
    })
    if (courseScheduled) {
        courseScheduled = courseScheduled.toJSON();
        if (!courseScheduled.is_completed) {
            if(courseScheduled.class_attendances.length > 0) {
                const attendedStudents = courseScheduled.class_attendances.filter((attendance) => attendance.attended);
                if (attendedStudents.length > 0) {
                    courseScheduled.class_attendances.forEach(async attendance => {
                        const points = {
                            points: 100,
                            title: `${courseScheduled.course.name}-${courseScheduled.topic.name}(${courseScheduled.batch.name})`,
                            student_account_id: attendance.student_id,
                            teacher_account_id: req.user.id,
                            created_by_account_id: req.user.id
                        }
                        attendance.topic_id = req.body.topic_id;
                        await ClassAttendance.update(attendance, { where: { id: attendance.id } });
                        attendance.attended && await StudentPoint.create(points);
                    });
                    var completedClass = await CourseScheduled.update({ is_completed: true }, { where: { id: req.params.id }, returning: true, plain: true });
                    var completedClassInfo = completedClass[1].toJSON();
    
                    if(courseScheduled.teacher.delivery_channel === 'B2B') {
                        return await getLiveWebinarAccessTokens()
                        .then(async (responseObj) => {
                            return await getLiveWebinarWidgerRecording(responseObj, completedClass[1].widget_id)
                                .then(async (response) => {
                                    const { data, error } = response;
                                    if (error) {
                                        throw error.message;
                                    } else {
                                        completedClassInfo.recording_url = data.length > 0 ? data[0].url : null;
                                        adminUser = adminUser.toJSON();
                                        data.length && sendClassRecordingWidgetMail(completedClassInfo, adminUser);
                                        return await completedClassInfo;
                                    }
                                })
                                .catch((error) => {
                                    throw error;
                                })
                        })
                        .catch((error) => {
                            throw error;
                        })
                    } else {
                        return completedClassInfo;
                    }
                } else {
                    const scheduledDateTimeFormat = new Date(courseScheduled.schedule_datetime);
                    const scheduledDateTime = new Date(scheduledDateTimeFormat.setMinutes(scheduledDateTimeFormat.getMinutes() + 30));
                    const todayDate = new Date();
                    if(scheduledDateTimeFormat.getTime() < todayDate.getTime() || scheduledDateTime.getTime() >= todayDate.getTime()) {
                        const cancelledCourse = await CourseScheduled.update({ is_cancelled: true }, { where: { id: req.params.id }, returning: true, plain: true });
                        return cancelledCourse[1];
                    } else {
                        throw "Can't finish the Class before 30 minutes of class start";
                    }
                }
            } else {
                throw "Please provide attendance for the class";
            }
        } else {
            throw "Class Already Completed";
        }
    } else {
        throw "Scheduled Class not found";
    }
}

// _fetchNextScheduledClass = async (req, res, next) => {
//     currentUtc = new Date();
//     var dateFound_mins = new Date(currentUtc);
//    var scheduleStartMinTime = new Date(dateFound_mins-60*60000);
//    var     scheduleStartPlusTime = new Date(dateFound_mins.getTime() + 60*60000);
 

//     // var dateFound_mins = new Date(currentUtc);
                   

//     // console.log(currentUtc) 
//     // console.log(scheduleStartTime) 
 

//     // var courseScheduled = await CourseScheduled.findOne({
//     //     where: {
//     //         schedule_datetime: { [Op.gte]:new Date() },
//     //             teacher_account_id: req.user.id,
//     //             is_completed: false
//     //         },
//     //     include: [{ model: Topic }, { model: Teacher }]
//     // });

//     // var dateFound_mins = new Date(currentUtc);
//     // topic_duration = new Date(dateFound_mins-courseScheduled.topic.duration_in_mins*60000);

//     // console.log(dateFound_mins)
//     // console.log(scheduleStartTime)
//     // return courseScheduled.topic.duration_in_mins;

//     if (req.user) {
//         return await CourseScheduled.findAll({
//             where: {
//             schedule_datetime: { 
//                 // [Op.between]: [new Date(), scheduleStartTime],
//                     [Op.gte]:new Date(),
//                 // [Op.lte]:scheduleStartTime,
//             },
            
//                 teacher_account_id: req.user.id,
//                 is_completed: false
//             },
//             order: [
//                 ['schedule_datetime', 'ASC']
//             ],
//         }).then(async (courses) => {
//             if (courses.length > 0) {
//                 courses[0] = courses[0] && courses[0].toJSON();
//                 const attendances = await ClassAttendance.findOne({
//                     include: [{
//                         model: CourseScheduled,
//                         where: { batch_id: courses[0].batch_id, topic_id: courses[0].topic_id }
//                     }]
//                 });
//                 courses[0].attendanceExist = attendances ? true : false;
//                 return await courses[0];
//             } else {
//                 throw "No classes found"
//             }
//         }).catch((error) => {
//             throw error;
//         });
//     } else {
//         throw "Invalid token, please login again";
//     }
// }
_fetchNextScheduledClass = async (req, res, next) => {
    currentUtc = new Date();
    var dateFound_mins = new Date(currentUtc);
   var scheduleStartMinTime = new Date(dateFound_mins-60*60000);
   var     scheduleStartPlusTime = new Date(dateFound_mins.getTime() + 60*60000);
    if (req.user) {
        return await CourseScheduled.findAll({
            where: {
                schedule_datetime: {
                    [Op.between]: [scheduleStartMinTime, scheduleStartPlusTime],
              },
                teacher_account_id: req.user.id,
                is_completed: false,
                is_cancelled:false
            },
            order: [
                ['schedule_datetime', 'ASC']
            ],
        }).then(async (courses) => {
            if (courses.length > 0) {
                courses[0] = courses[0] && courses[0].toJSON();
                const attendances = await ClassAttendance.findOne({
                    include: [{
                        model: CourseScheduled,
                        where: { batch_id: courses[0].batch_id, topic_id: courses[0].topic_id }
                    }]
                });
                courses[0].attendanceExist = attendances ? true : false;
                return await courses[0];
            } else {
                throw "No classes found"
            }
        }).catch((error) => {
            throw error;
        });
    } else {
        throw "Invalid token, please login again";
    }
}


/**
 * @param { batch_id }
 */
_getAllStudentsByBatchId = async (req, res, next) => {
    if (req.user) {
        const { batch_id } = req.params;
        return await BatchStudent.findAndCountAll({
            where: { batch_id }
        });
    } else {
        throw "Invalid token, please login again";
    }
}

_getMyStudentsInformation = async (req) => {
    if (req.user) {
        let { pageSize, pageNumber } = req.query;
        pageSize = pageSize ? pageSize : 100;
        pageNumber = pageNumber ? pageNumber : 1;

        const includeItems = await getIncludeClause(req)

        const attendances = await ClassAttendance.findAll({
            where: {},
            order: [
                ['createdAt', 'DESC']
            ],
            limit: pageSize,
            offset: (pageNumber - 1) * pageSize,
            include: includeItems
        });

        var newAttendance = {};
        var attendanceArray = []
        const institutes = [];
        const batches = [];

        await attendances.forEach(async (attendance) => {
            attendance = attendance.toJSON();
            await batches.push(attendance.class_scheduled.batch);
            await institutes.push(attendance.student.institute_has_student.institute);
        });

        var groupedAttedance = attendances.reduce(function (result, current) {
            result[current.student_id] = result[current.student_id] || [];
            result[current.student_id].push(current.toJSON());
            return result;
        }, {});

        attendanceArray = await Promise.all(
            Object.keys(groupedAttedance)
                .map(async (key) => {
                    var newObj = {};
                    const urls = []
                    newObj = await Promise.all(groupedAttedance[key].map(async (attendance) => {
                        newObj = attendance;
                        newObj.url = await getSignedUrlFromS3(newObj.url);
                        urls.push(await getSignedUrlFromS3(attendance.url));
                        newObj.urls = urls;
                        newObj.totalClasses = groupedAttedance[key].length;
                        const attendedCount = groupedAttedance[key].filter((att => att.attended));
                        newObj.classesAttended = attendedCount.length;
                        newObj.tillDateAttendance = (attendedCount.length * 100) / newObj.totalClasses;
                        const completed = groupedAttedance[key].filter((att => !att.topic.topic_assignment || att.url));
                        newObj.completedTopics = await (completed.length * 100) / newObj.totalClasses;
                        newObj.student.imageUrl = await getSignedUrlFromS3(attendance.student.imageUrl);
                        return newObj
                    }))
                    return newObj[0];
                }))

        newAttendance.attendances = attendanceArray;
        newAttendance.batches = [...new Set(batches.map(x => x.name))];
        newAttendance.institutes = [...new Set(institutes.map(x => x.name))];
        return await newAttendance;
    } else {
        throw "Invalid token, please login again";
    }
}

_monthlyCalendar = async (request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { year, month, pageSize, pageNumber } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;

    const startDate = `${year}-${month}-01`;
    const endDate = `${year}-${month}-${new Date(2021, 03, 0).getDate()}`;

    return await CourseScheduled.findAndCountAll({
        where: {
            schedule_datetime: { [Op.gte]: new Date(startDate), [Op.lte]: new Date(endDate) },
            teacher_account_id: request.user.id,
            is_deleted: false
        },
        include: [{
            model: Course
        }, {
            model: Batch, include: [{
                model: BatchStudent,
                include: [{ model: Student }]
            }]
        },
        { model: Teacher, include: [{ model: Account }] },
        { model: Topic }],
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
}

_MyStudentsDetailsById = async (req) => {
    if (req.user) {
        let attendances = await ClassAttendance.findAll({
            where: { student_id: req.params.id, attended: true },
            include: [
                {
                    model: CourseScheduled,
                    required: true,
                    where: { teacher_account_id: req.user.id, is_completed: true },
                    include: [
                        {
                            model: Topic,
                            required: true,
                            include: [
                                { 
                                    model: TopicAssignment,
                                    required: true,
                                },
                                { 
                                    model: TeacherTopicFeedback,
                                    
                                }
                            ]
                        },
                    ]
                }
            ]
        });
   
        var quizzes = await Quiz.findAll({
            include: [
                { model: QuizQuestion },
                {
                    model: StudentQuizAnswer,
                    required: true,
                    where: { is_submitted: true, student_account_id: req.params.id }
                }
            ]
        });

    console.log("came here")
  
    const batches = [];

    await attendances.forEach(async (attendance) => {
        attendance = attendance.toJSON();
        await batches.push(attendance);
    });

    await batches.forEach(async (attendance) => {
        
        attendance.quizzes=""
        
    });
    attendances=batches
    //  return attendances;   
       
    var filteredTeacherFeedback = [];
        await attendances.forEach(async attendance => {
            console.log("-----step1------")
            if(attendance.class_scheduled.topic.teacher_topic_feedbacks.length > 0) {
                // await attendance.class_scheduled.topic.teacher_topic_feedbacks.forEach(async (feedback, index) => {
                //     console.log("-----step2------")
                //     feedback = feedback.toJSON()

                //     if(feedback.student_account_i== req.params.id) {
                //         console.log("-----step3------")
                //         filteredTeacherFeedback.push(attendance);
                        
                //     }
                //   else  if(feedback.student_account_i!= req.params.id) {
                //         console.log("-----spliceiing-----" + index)
                //         attendance.class_scheduled.topic.teacher_topic_feedbacks.splice(feedback,1)
                //     }    
                // })
                var i = 0;
                while (i < attendance.class_scheduled.topic.teacher_topic_feedbacks.length)
                {
                    if (attendance.class_scheduled.topic.teacher_topic_feedbacks[i].dataValues.student_account_i != req.params.id)
                    {
                        attendance.class_scheduled.topic.teacher_topic_feedbacks.splice(i, 1);
                    }
                    else
                    {
                        ++i;
                    }
                }
            } 

            else {
                console.log("-----step1- else bpart-----")
                attendance.class_scheduled.topic.teacher_topic_feedbacks.length = 0
               await filteredTeacherFeedback.push(attendance);
               
            }
        });
        // return attendances;

        // filteredTeacherFeedback= attendances.filter(dataFound => dataFound.class_scheduled.topic.teacher_topic_feedback ? dataFound.class_scheduled.topic.teacher_topic_feedback.student_account_id == req.params.id : true);
        const newAttendances = [];
        await attendances.forEach(async (attendance,index) => {
            const urls = await getMultipleSignedUrlFromS3([attendance.url, attendance.class_scheduled.topic.topic_assignment.url]);
            attendance.url = urls[0];
            attendance.class_scheduled.topic.topic_assignment.url = urls[1];
                for(var j=0;j<quizzes.length;j++)
                {
                    if(attendance.topic_id==quizzes[j].topic_id)
                    {
                        attendance.quizzes= quizzes[j];
                    }
                }
               


            await newAttendances.push(attendance);
        });

        return await newAttendances;
    } else {
        throw "Invalid token, please login again";
    }
}

_getTeacherTotalClasses = async (req) => {
    if (req.user) {
        return await CourseScheduled.count({
            where: { teacher_account_id: req.user.id, is_completed: true }
        });
    } else {
        throw "Invalid token, please login again";
    }
}

_startClass = async (req) => {
    if (req.user) {
        var courseScheduled = await CourseScheduled.findOne({
            where: { id: req.body.course_scheduled_id, teacher_account_id: req.user.id },
            include: [{ model: Topic }, { model: Teacher }]
        });
        if (courseScheduled) {
            courseScheduled = courseScheduled.toJSON();
            console.log(courseScheduled.teacher);
            if (courseScheduled.teacher.delivery_channel === 'B2B') {
                if (courseScheduled.presenter && courseScheduled.attendee) {
                    courseScheduled.batchStudents = await getBatchStudentsWithSignedUrl(courseScheduled.batch_id);
                    return await courseScheduled;
                } else {
                    return await getLiveWebinarAccessTokens()
                        .then(async (responseObj) => {
                            return await createLiveWebinarClass(responseObj, courseScheduled)
                                .then(async (response) => {
                                    const { data, error } = response;
                                    if (error) {
                                        throw error.message;
                                    } else {
                                        const updateDetails = {
                                            attendee: data.hosted_at.attendee,
                                            presenter: data.hosted_at.presenter,
                                            moderator: data.hosted_at.moderator,
                                            host: data.hosted_at.host,
                                            phone_attendee: data.hosted_at.phone_attendee,
                                            phone_presenter: data.hosted_at.phone_presenter,
                                            widget_id: data.id
                                        }
                                        var courseUpdated = await CourseScheduled.update(updateDetails, { where: { id: courseScheduled.id }, returning: true, plain: true });
                                        courseUpdated = courseUpdated[1].toJSON();
                                        courseUpdated.batchStudents = await getBatchStudentsWithSignedUrl(courseUpdated.batch_id);
                                        return await courseUpdated;
                                    }
                                })
                                .catch((error) => {
                                    throw error;
                                })
                        })
                        .catch((error) => {
                            throw error;
                        })
                }
            } else {
                return await getBatchStudentsWithSignedUrl(courseScheduled.batch_id);
            }
        } else {
            throw "Scheduled Class not found";
        }
    } else {
        throw "Invalid token, please login again";
    }
}

getBatchStudentsWithSignedUrl = async (batch_id) => {
    const batchStudents = await BatchStudent.findAndCountAll({
        where: { batch_id },
        include: [{
            model: Student
        }]
    });
    const attendances = await ClassAttendance.findOne({
        include: [{ model: CourseScheduled, where: { batch_id } }]
    })
    batchStudents.attendanceExist = attendances ? true : false;

    const batchStudentsWithSignedUrl = [];
    await batchStudents.rows.forEach(async (batch) => {
        batch.student.imageUrl = batch.student.imageUrl ? await getSignedUrlFromS3(batch.student.imageUrl) : batch.student.imageUrl;
        await batchStudentsWithSignedUrl.push(batch);
    });
    return await batchStudentsWithSignedUrl;
}

getIncludeClause = (req) => {
    const queryParams = Object.keys(req.query).length !== 0 ? true : false;
    let { institute, batch } = req.query;
    if (queryParams && batch && institute) {
        return [{
            model: CourseScheduled,
            required: true,
            where: { teacher_account_id: req.user.id, is_completed: true, is_deleted: false },
            include: [{
                model: Batch,
                required: true,
                where: (queryParams && batch) ?
                    { name: { [Op.like]: `%${batch}%` }, is_deleted: false }
                    : { is_deleted: false },
            }]
        }, {
            model: Student,
            required: true,
            include: [{
                model: InstituteHasStudent,
                required: true,
                include: [{
                    model: Institute,
                    required: true,
                    where: (queryParams && institute) ?
                        { name: { [Op.like]: `%${institute}%` }, is_deleted: false }
                        : { is_deleted: false },
                }]
            }]
        }, {
            model: Topic,
            include: [{ model: TopicAssignment }]
        }]
    } else if (queryParams && batch) {
        return [{
            model: CourseScheduled,
            required: true,
            where: { teacher_account_id: req.user.id, is_completed: true, is_deleted: false },
            include: [{
                model: Batch,
                required: true,
                where: (queryParams && batch) ?
                    { name: { [Op.like]: `%${batch}%` }, is_deleted: false }
                    : { is_deleted: false },
            }]
        }, {
            model: Student,
            include: [{
                model: InstituteHasStudent,
                include: [{ model: Institute }]
            }]
        }, {
            model: Topic,
            include: [{ model: TopicAssignment }]
        }]
    } else if (queryParams && institute) {
        return [{
            model: CourseScheduled,
            where: { teacher_account_id: req.user.id, is_completed: true, is_deleted: false },
            include: [{ model: Batch }]
        }, {
            model: Student,
            required: true,
            include: [{
                model: InstituteHasStudent,
                required: true,
                include: [{
                    model: Institute,
                    required: true,
                    where: (queryParams && institute) ?
                        { name: { [Op.like]: `%${institute}%` }, is_deleted: false }
                        : { is_deleted: false },
                }]
            }]
        }, {
            model: Topic,
            include: [{ model: TopicAssignment }]
        }]
    } else {
        return [{
            model: CourseScheduled,
            where: { teacher_account_id: req.user.id, is_completed: true, is_deleted: false },
            include: [{ model: Batch }]
        }, {
            model: Student,
            include: [{
                model: InstituteHasStudent,
                include: [{ model: Institute }]
            }]
        }, {
            model: Topic,
            include: [{ model: TopicAssignment }]
        }]
    }
}

_myCompletedClasses = async (req) => {
    return await CourseScheduled.count({
        where: { teacher_account_id: req.user.id, is_completed: true }
    })
}

_myScheduledClassForTheDate = async (req) => {
    if (req.user)
    {
        let { year, month, day } = req.query;
        let  nextDate = parseInt(day)+1
        const dateMentioned = `${year}-${month}-${day}`;
        const nextDateFromDateMentioned = `${year}-${month}-${nextDate}`;
        if(day==31)
        {
            let nextMonth = parseInt(month) + 1
            nextDate=1;
            const nextDateFromDateMentionedEndOfMonth = `${year}-${nextMonth}-${nextDate}`;

            return await CourseScheduled.findAndCountAll({
                where: {
                    schedule_datetime: { [Op.gte]: dateMentioned , [Op.lt]: nextDateFromDateMentionedEndOfMonth},
                    teacher_account_id: req.user.id,
                    is_deleted: false
                },
                include: [{
                    model: Course
                },
                {
                    model: Topic
                },
                {
                    model: Batch,
                    include: [{
                        model: BatchStudent
                    }]
                }],
            })
        }
       return await CourseScheduled.findAndCountAll({
            where: {
                schedule_datetime: { [Op.gt]: dateMentioned , [Op.lte]:nextDateFromDateMentioned },
                teacher_account_id: req.user.id,
                is_deleted: false
            },
            include: [{
                model: Course
            },
            {
                model: Topic
            },
            {
                model: Batch,
                include: [{
                    model: BatchStudent
                }]
            }],
        })
    }
    else
    {
        throw "Invalid token, please login again";
    }
}

_enableQuiz = async (req) => {
    if(req.user) {
       
        return await await CourseScheduled.update({is_quiz_enabled:true}, { where: { teacher_account_id: req.user.id,id: req.body.course_scheduled_id} });

    } else {
        throw "Invalid token, please login again";
    }

    
}

_getInClassQuizQuestionByTopicId = async (req) => {
    if (req.user) {
        var quiz = await Quiz.findOne({
            where: { topic_id: req.params.id, is_deleted: false, quiz_type:0},
            include: [{
                model: QuizQuestion
            }]
        });
        if (quiz) {
            return await quiz; 
        } else {
            throw "Quiz not found for this Topic";
        }
    } else {
        throw "Invalid token, please login again";
    }
}

_getAllCourseSchedules = async (request) => {
    console.log(request.user.id)
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber, filter } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    var currentDate = new Date();
    var start = new Date();
    start.setUTCHours(0,0,0,0);
    

    let whereClause = { is_deleted: false,teacher_account_id:request.user.id };
    let includeClause = [
        { model: Course },
        { model: Batch },
        { model: Topic }]
    switch (filter) {
        case 'completed':
            whereClause.is_completed = true
            break;
        case 'upcoming':
            whereClause = [
                Sequelize.where(Sequelize.fn('date', Sequelize.col('schedule_datetime')), '=', currentDate),
                { is_deleted: { [Op.is]: false }, is_completed: false, is_cancelled:false, teacher_account_id:request.user.id }
            ];
            break;
        case 'future':
            whereClause.is_completed = false;
            whereClause.is_cancelled = false;
            start.setDate(start.getDate() + 1);
            whereClause.schedule_datetime = { [Op.gte]: start }
        //     whereClause.schedule_datetime= {
        //         [Op.between]: [todayDate, currentDate],
        //   }
            break;
        default:
            break;
    }

    var courseScheduleds = await CourseScheduled.findAll({
        where: (queryParams && filter) ? whereClause : { teacher_account_id:request.user.id,is_deleted: false },
        include: includeClause,
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
    let dataObj = {};
    dataObj.count = courseScheduleds.length;
    dataObj.rows = courseScheduleds;
    return dataObj;
}

_getLatestScheduleds = async (req) => {
    if (req.user) {
        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();

        const startDate = `${year}-${month}-${day}`;
        
        return await CourseScheduled.findAndCountAll({
            where: {
                schedule_datetime: { [Op.gte]: new Date(startDate) },
                teacher_account_id: req.user.id,
                is_deleted: false
            },
            include: [{
                model: Course
            }, {
                model: Batch, include: [{
                    model: BatchStudent,
                    include: [{ model: Student }]
                }]
            },
            { model: Teacher, include: [{ model: Account }] },
            { model: Topic }],
            order: [
                ['schedule_datetime', 'ASC']
            ],
            // limit: pageSize,
            // offset: (pageNumber - 1) * pageSize
        });


    } else {
        throw "Invalid token, please login again";
    }
}

_getPastScheduleds = async (req) => {
    if (req.user) {
        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();

        const startDate = `${year}-${month}-${day}`;
        return await CourseScheduled.findAndCountAll({
            where: {
                schedule_datetime: { [Op.lt]: new Date(startDate) },
                teacher_account_id: req.user.id,
                is_deleted: false
            },
            include: [{
                model: Course
            }, {
                model: Batch, include: [{
                    model: BatchStudent,
                    include: [{ model: Student }]
                }]
            },
            { model: Teacher, include: [{ model: Account }] },
            { model: Topic }],
            order: [
                ['schedule_datetime', 'DESC']
            ],
            // limit: pageSize,
            // offset: (pageNumber - 1) * pageSize
        });


    } else {
        throw "Invalid token, please login again";
    }
}

module.exports = {
    _finishScheduledClass,
    _fetchNextScheduledClass,
    _getAllStudentsByBatchId,
    _getMyStudentsInformation,
    _MyStudentsDetailsById,
    _monthlyCalendar,
    _getTeacherTotalClasses,
    _startClass,
    _myCompletedClasses,
    _myScheduledClassForTheDate,
    _enableQuiz,
    _getInClassQuizQuestionByTopicId,
    _getAllCourseSchedules,
    _getLatestScheduleds,
    _getPastScheduleds
}