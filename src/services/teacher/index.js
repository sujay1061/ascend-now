const teacher_class_attendance = require('./class_attendance.service');
const teacher_course_scheduled = require('./course_scheduled.service');
const teacher_batch = require('./batch.service');
const teacher_topic = require('./topic.service');
const teacher_profile = require('./teacher.service');
const teacher_class_activities = require('./class_activity.service');
const teacher_topic_feedback = require('./teacher_topic_feedback.service');
const teacher_student_point = require('./student_point.service');
const teacher_booked_classes = require('./booked_classes');
const teacher_student = require('./student.service');
const teacher_course = require('./course.service');

module.exports = {
  teacher_class_attendance,
  teacher_course_scheduled,
  teacher_batch,
  teacher_topic,
  teacher_profile,
  teacher_class_activities,
  teacher_topic_feedback,
  teacher_student_point,
  teacher_booked_classes,
  teacher_student,
  teacher_course
};
