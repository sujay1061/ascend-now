const { StudentPoint } = require("../../models");
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

/**
 * @param { batch_id }
 */
_createNewPoints = async (req, res, next) => {
    if(req.user) {
        return await StudentPoint.create(req.body);
    } else {
        throw "Invalid token, please login again";
    }
}

module.exports = {
    _createNewPoints
}