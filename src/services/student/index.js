const student_course_scheduled = require('./course_scheduled.service');
const student_profile = require('./student.service');
const student_points = require('./student_point.service');
const student_quiz = require('./quiz.service');
const student_booked_classes = require('./booked_classes');
const student_teacher = require('./teacher.service');
const student_topic = require('./topic.service');
const student_activity = require('./class_activity.service');

module.exports = {
  student_course_scheduled,
  student_profile,
  student_points,
  student_quiz,
  student_booked_classes,
  student_teacher,
  student_topic,
  student_activity
};
