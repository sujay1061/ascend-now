const { BookedClasses, Course, Topic, Teacher, BookedClassTimeslot, Student, CourseScheduled } = require("../../models");
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const { getLiveWebinarAccessTokens, createLiveWebinarClass, createLiveWebinarBookedClass } = require("../../_helpers/live_webinar");
const { requestedSlotsWithTeacher } = require("../../notification/job-processors/student/student.processor");
const { getSignedUrlFromS3,getMultipleSignedUrlFromS3 } = require("../../_helpers/aws_s3_file_upload");
_getMyBookedClasses = async (req) => {
    if (req.user) {
        return await BookedClasses.findAndCountAll({
            where: { student_account_id: req.user.id, is_deleted: false },
            include: [
                { model: BookedClassTimeslot },
                { model: Student },
                { model: Teacher },
                { model: Topic, include: [{ model: Course }] }
            ],
            order: [
                [ {model: BookedClassTimeslot},'id','ASC']
                 ]
        });
    } else {
        throw "Invalid token, please login again";
    }
}

// _getMyConfirmedClasses = async (req) => {
//     if (req.user) {
//         return await BookedClasses.findAndCountAll({
//             where: { student_account_id: req.user.id, is_accepted: true },
//             include: [
//                 { model: BookedClassTimeslot },
//                 { model: Student },
//                 { model: Teacher },
//                 { model: Topic, include: [{ model: Course }] }
//             ]
//         });
//     } else {
//         throw "Invalid token, please login again";
//     }
// }

_getMyConfirmedClasses = async (req) => {
    if (req.user) {
        let { year, month, pageSize, pageNumber } = req.query;
        if(year==undefined && month==undefined)
        {
            return await BookedClasses.findAndCountAll({
                where: { student_account_id: req.user.id, is_accepted: true },
                include: [
                    { model: BookedClassTimeslot },
                    { model: Student },
                    { model: Teacher },
                    { model: Topic, include: [{ model: Course }] }
                ]
            });
        }
        else
        {
            
            monthnew = parseInt(month)+1
            monthnew=monthnew.toString()
            const startDate = `${year}-${month}-01`;
            const endDate = `${year}-${monthnew}-01`;
            return await BookedClasses.findAndCountAll({
                where: { student_account_id: req.user.id, is_accepted: true },
                include: [
                    { model: BookedClassTimeslot,
                        where:{
                            start_date: 
                            { [Op.gte]: new Date(startDate), [Op.lt]: new Date(endDate) }
                        }
                    },
                    { model: Student },
                    { model: Teacher },
                    { model: Topic, include: [{ model: Course }] }
                ]
            });
        }
 
    } else {
        throw "Invalid token, please login again";
    }
}

_bookClassWithTeacher = async (req) => {
    if (req.user) {
        const { topic_id, teacher_account_id, timeslots, comment } = req.body;
        const topic = await Topic.findOne({ where: { id: topic_id } });
        const teacher = await Teacher.findOne({ where: { id: teacher_account_id } });
        const student = await Student.findOne({ where: { id: req.user.id } });
        if (!teacher) {
            throw "Selected teacher not found";
        } else if (!topic) {
            throw "Selected topic not found";
        } else {
            const checkForTimeslot = await checkForBookedTimeslot(req, true);
            if (!checkForTimeslot) {
                const checkForStudentTimeslot = await checkForBookedTimeslot(req, false);
                if(!checkForStudentTimeslot) {
                    req.body.student_account_id = req.user.id;
                    req.body.created_by_account_id = req.user.account_id;
                    req.body.booked_by = 'student';
                    return await BookedClasses.create(req.body)
                        .then(async (classes) => {
                            const timeslotPromises = [];
                            classes = classes.toJSON();
                            
                            requestedSlotsWithTeacher(student,teacher,topic,req.body.timeslots,comment)
                            await Object.keys(timeslots).map(async (timeslot, index) => {
                                const timeslotObj = {
                                    slot_no: index + 1,
                                    start_date: timeslots[timeslot].start_date,
                                    booked_id: classes.id,
                                    created_by_account_id: req.user.id
                                }
                                await timeslotPromises.push(timeslotObj);
                            })
                            return await BookedClassTimeslot.bulkCreate(timeslotPromises)
                                .then(async (booked_timeslots) => {
                                    classes.booked_timeslots = booked_timeslots;
                                    return await classes;
                                })
                                .catch(async (error) => {
                                    await BookedClasses.destroy({ where: { id: classes.id }, force: true })
                                    throw error;
                                });
                        })
                        .catch((error) => {
                            throw error;
                        });
                } else {
                    throw "You have already requested for this timeslot";   
                }
            } else {
                throw "This timeslot is already booked for Teacher";            
            }
        }
    } else {
        throw "Invalid token, please login again";
    }
}

async function checkForBookedTimeslot(req, is_accepted) {
    const { teacher_account_id, timeslots } = req.body;
    const whereClause = is_accepted ? { teacher_account_id, is_deleted: false, is_accepted: true } : { teacher_account_id, is_deleted: false, student_account_id: req.user.id }
    return await BookedClasses.findOne({
        where: whereClause,
        include: [{
            model: BookedClassTimeslot,
            required: true,
            where: {
                [Op.or]: [
                    {
                        start_date: new Date(timeslots['1'].start_date)
                    },
                    {
                        start_date: new Date(timeslots['2'].start_date)
                    }
                ]
            }
        }]
    });
}

_getBookedClassesByDate = async (req) => {
    if (req.user) {
        let { year, month, day } = req.query;
        let nextDate = parseInt(day) + 1
        const dateMentioned = `${year}-${month}-${day}`;
        const nextDateFromDateMentioned = `${year}-${month}-${nextDate}`;

        if(day==31)
        {
            let nextMonth = parseInt(month) + 1
            nextDate=1;
            const nextDateFromDateMentionedEndOfMonth = `${year}-${nextMonth}-${nextDate}`;
            return await BookedClasses.findAndCountAll({
                where: { student_account_id: req.user.id, is_accepted: true, is_deleted: false },
                include: [{
                    model: BookedClassTimeslot,
                    where: { start_date: { [Op.gte]: dateMentioned, [Op.lt]: nextDateFromDateMentionedEndOfMonth } }
                },
                {
                    model: Topic,
                    include: [{ model: Course }]
                }],
            });
        }



        return await BookedClasses.findAndCountAll({
            where: { student_account_id: req.user.id, is_accepted: true , is_deleted: false },
            include: [{
                model: BookedClassTimeslot,
                where: { start_date: { [Op.gte]: dateMentioned, [Op.lt]: nextDateFromDateMentioned}  }
            },
            {
                model: Topic,
                include: [{ model: Course }]
            }],
        });
    }
    else {
        throw "Invalid token, please login again";
    }
}

_fetchMyUpcomingBookedClasses = async (req) => {
    currentUtc = new Date();
    var dateFound_mins = new Date(currentUtc);
    var     scheduleEndTime = new Date(dateFound_mins.getTime() + 60*60000);  
    if (req.user) {
        return await BookedClasses.findAll({
            
            where: {
                student_account_id: req.user.id, 
                is_deleted: false,
                is_completed:false,
                is_rejected:false,
                is_cancelled:false

            },
            include: [{
                model: BookedClassTimeslot,
                required: true,
                where: {
                    [Op.or]: [
                        {
                            start_date: {
                                [Op.gte]: currentUtc,
                             
                            }
                        },
                        {
                            end_date: {
                                [Op.lte]: scheduleEndTime,
                                
                            }
                        }
                    ], is_confirmed:true
                },
                 
            }],
            order: [
                [ {model: BookedClassTimeslot},'start_date','ASC']
                 ]
        }).then(async (courses) => {
            if (courses.length > 0) {
               
                return await courses[0];
            } else {
                throw "No classes found"
            }
        }).catch((error) => {
            throw error;
        });
    }
    else {
        throw "Invalid token, please login again";
    }
}

_joinMyBookedClass = async(req,res,next) => {
    if(req.user)
    {
        var bookedClasses = await BookedClasses.findOne({
            where: { id: req.params.id, student_account_id: req.user.id },
            include: [{ model: BookedClassTimeslot},
                { model: Topic,
                 include: [{
                         model: Course
                        }] 
                }, 
            { model: Student }]
        });
        var courseScheduleds = await  CourseScheduled.findOne({
            where: { booked_class_id: bookedClasses.id }
        });
        
// return courseScheduleds;
        if(bookedClasses)
        {
            bookedClasses = bookedClasses.toJSON();
            if (bookedClasses.student.delivery_channel === 'B2C') 
            {
                if (bookedClasses.presenter!=null && bookedClasses.attendee!=null) {
                    bookedClasses.courseScheduled_Id = courseScheduleds.id
                    return await bookedClasses;
                } 
                else{
                // return bookedClasses.topic.duration_in_mins;
                return await getLiveWebinarAccessTokens()
                        .then(async (responseObj) => {
                            bookedClasses.booked_class_timeslots[0].name=bookedClasses.name
                            bookedClasses.booked_class_timeslots[0].duration_in_mins=bookedClasses.topic.duration_in_mins
                            return await createLiveWebinarBookedClass(responseObj, bookedClasses.booked_class_timeslots[0])
                                .then(async (response) => {
                                    const { data, error } = response;
                                    if (error) {
                                        throw error.message;
                                    } else {
                                        const updateDetails = {
                                            attendee: data.hosted_at.attendee,
                                            presenter: data.hosted_at.presenter,
                                            moderator: data.hosted_at.moderator,
                                            host: data.hosted_at.host,
                                            phone_attendee: data.hosted_at.phone_attendee,
                                            phone_presenter: data.hosted_at.phone_presenter,
                                            widget_id: data.id
                                        }
                                        
                                        var bookedClassUpdated = await BookedClasses.update(updateDetails, { where: { id: req.params.id }, returning: true, plain: true });
                                        bookedClassUpdated = bookedClassUpdated[1].toJSON();
                                        bookedClassUpdated.courseScheduled_Id = courseScheduleds.id
                                        return await bookedClassUpdated;
                                    }
                                })
                                .catch((error) => {
                                    throw error;
                                })
                        })
                        .catch((error) => {
                            throw error;
                        })
                    }
            }
        }
        else {
            throw "Scheduled Class not found";
            } 
    }
    else {
        throw "Invalid token, please login again";
    }
}

_getAllBookedClass = async (request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber, filter } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    var currentDate = new Date();
    let whereClause = { is_deleted: false };
    let includeClause = [
            { model: Student },
            { model: Teacher },
            { model: Topic, include: [{ model: Course }] }
    ]
    switch (filter) {
        case 'completed':
            whereClause.is_completed = true
            whereClause.student_account_id=request.user.id
            includeClause.push({ model: BookedClassTimeslot })
            break;
        case 'upcoming':
            whereClause = [
                { is_deleted: { [Op.is]: false }, is_completed: false, student_account_id:request.user.id }
            ];
            includeClause.push({ model: BookedClassTimeslot, where: Sequelize.where(Sequelize.fn('date', Sequelize.col('start_date')), '=', currentDate)})
            break;
        case 'future':
            whereClause.is_completed = false;
            whereClause.student_account_id=request.user.id
            currentDate.setDate(currentDate.getDate() + 1);
            // whereClause.start_date = { [Op.gte]: currentDate }
            includeClause.push({ model: BookedClassTimeslot, where: {start_date : { [Op.gte]: currentDate }} })
            break;
        default:
            break;
    }

    var bookedClasses = await BookedClasses.findAll({
        where: (queryParams && filter) ? whereClause : { is_deleted: false },
        include: includeClause,
        order: [
            ['createdAt', 'ASC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
    let dataObj = {};
    dataObj.count = bookedClasses.length;
    dataObj.rows = bookedClasses;
    dataObj.rows  = dataObj.rows.map(async(classes) => {
        classes = classes.toJSON();
        classes.student.imageUrl  = classes.student.imageUrl ? await getSignedUrlFromS3(classes.student.imageUrl):classes.student.imageUrl
        classes.teacher.imageUrl =  classes.teacher.imageUrl ? await getSignedUrlFromS3(classes.teacher.imageUrl):classes.teacher.imageUrl
        return await classes;
    });
    dataObj.rows  = await Promise.all(dataObj.rows );
    return await dataObj;
}

_getLatestBookings = async (req) => {
    if (req.user) {
        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();

        const startDate = `${year}-${month}-${day}`;
        

        return await BookedClasses.findAndCountAll({
            where: { student_account_id: req.user.id, is_deleted: false },
            include: [
                { model: BookedClassTimeslot,
                    where:{
                        start_date: 
                        { [Op.gte]: new Date(startDate)}
                    }
                },
                { model: Student },
                { model: Teacher },
                { model: Topic, include: [{ model: Course }] }
            ],
              order: [
                [ {model:  BookedClassTimeslot},'start_date','ASC']
                 ]
        });


    } else {
        throw "Invalid token, please login again";
    }
}

_getPastBookings = async (req) => {
    if (req.user) {
        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();

        const startDate = `${year}-${month}-${day}`;
        return await BookedClasses.findAndCountAll({
            where: { student_account_id: req.user.id, is_deleted: false },
            include: [
                { model: BookedClassTimeslot,
                    where:{
                        start_date: 
                        { [Op.lt]: new Date(startDate)}
                    }
                },
                { model: Student },
                { model: Teacher },
                { model: Topic, include: [{ model: Course }] }
            ],
              order: [
                [ {model:  BookedClassTimeslot},'start_date','DESC']
                 ]
        });


    } else {
        throw "Invalid token, please login again";
    }
}

module.exports = {
    _getMyBookedClasses,
    _bookClassWithTeacher,
    _getMyConfirmedClasses,
    _getBookedClassesByDate,
    _fetchMyUpcomingBookedClasses,
    _joinMyBookedClass,
    _getAllBookedClass,
    _getLatestBookings,
    _getPastBookings
}