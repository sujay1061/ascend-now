const { StudentPoint } = require("../../models");

_getMyPointsHistory = async (req) => {
    if(req.user) {
        return await StudentPoint.findAndCountAll({ where: { student_account_id: req.user.id },
            order: [
                ['createdAt', 'DESC']
            ]
        });
    } else {
        throw "Invalid token, please login again";
    }
}

module.exports = {
    _getMyPointsHistory
}