const { Topic, Course } = require("../../models");
const { getSignedUrlFromS3 } = require("../../_helpers/aws_s3_file_upload");

_getTopicsById = async (req) => {
    var topic = await Topic.findOne({ where: { id: req.params.id } , include: [{ model: Course }]});
    topic = topic.toJSON();
    topic.topic_attachment = topic.topic_attachment ? await getSignedUrlFromS3(topic.topic_attachment) : topic.topic_attachment;
    topic.topic_script = topic.topic_script ? await getSignedUrlFromS3(topic.topic_script) : "";
    return topic;
}

_getAllTopicsByCourseId = async (req) => {
    return await Topic.findAll({ 
        where: { course_id: req.params.id },
        order: [
            ['createdAt', 'DESC']
        ],
    });
}

module.exports = {
    _getTopicsById,
    _getAllTopicsByCourseId
}