const { Teacher } = require("../../models");

_getAllTeachersByDeliveryChannel = async (req) => {
    return await Teacher.findAll({
        where: { delivery_channel: req.params.channel },
        order: [
            ['createdAt', 'DESC']
        ],
    });
}

module.exports = {
    _getAllTeachersByDeliveryChannel
}