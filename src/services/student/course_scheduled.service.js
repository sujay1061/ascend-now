const { CourseScheduled, ClassAttendance, Course, Batch, BatchStudent, Student, Teacher, Account, TopicAssignment, Topic, TeacherTopicFeedback } = require("../../models");
const Sequelize = require('sequelize');
const { getSignedUrlFromS3 } = require("../../_helpers/aws_s3_file_upload");
const { getLiveWebinarAccessTokens, createLiveWebinarClass } = require("../../_helpers/live_webinar");
const Op = Sequelize.Op;

_uploadopicAssignmentReport = async (req, res, next) => {
    if (req.user) {
        const studentAttendance = await ClassAttendance.findOne({
            where: {
                course_scheduled_id: req.body.course_scheduled_id,
                student_id: req.user.id,
                topic_id: req.params.topicId
            }
        });
        const topicAssignment = await TopicAssignment.findOne({
            where: { topic_id: req.params.topicId, is_deleted: false }
        });
        if (topicAssignment) {
            if (studentAttendance) {
                const file = req.file
                req.body.url = file ? file.location : studentAttendance.url;
                await ClassAttendance.update(req.body, { where: { id: studentAttendance.id }, returning: true, plain: true });
                var attendance = await ClassAttendance.findOne({ where: { id: studentAttendance.id } });
                attendance.url = await getSignedUrlFromS3(attendance.url);
                return await attendance;
            } else {
                throw "Attendance not found";
            }
        } else {
            throw "Assignment is not available for this topic";
        }
    } else {
        throw "Invalid token, please login again";
    }
}

_monthlyCalendar = async (request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { year, month, pageSize, pageNumber } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;

    const startDate = `${year}-${month}-01`;
    const endDate = `${year}-${month}-${new Date(2021, 03, 0).getDate()}`;

    return await CourseScheduled.findAndCountAll({
        where: { schedule_datetime: { [Op.gte]: new Date(startDate), [Op.lte]: new Date(endDate) } },
        include: [{
            model: Course
        }, {
            model: Batch, include: [{
                model: BatchStudent,
                where: { student_account_id: request.user.id },
                include: [{ model: Student }]
            }]
        },
        { model: Teacher, include: [{ model: Account }] },
        { model: Topic }],
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
}

_getAllCourseSchedules = async (request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber, filter } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    var currentDate = new Date();

    let whereClause = { is_deleted: false };
    let includeClause = [
        { model: Course },
        {
            model: Batch,
            include: [
                {
                    model: BatchStudent,
                    where: { student_account_id: request.user.id },
                    include: [{ model: Student }]
                }]
        },
        { model: Teacher, include: [{ model: Account }] },
        { model: Topic, include: [{ model: TeacherTopicFeedback }] }]
    switch (filter) {
        case 'completed':
            whereClause.is_completed = true
            includeClause.push({ model: ClassAttendance, where: { student_id: request.user.id } })
            break;
        case 'upcoming':
            whereClause = [
                Sequelize.where(Sequelize.fn('date', Sequelize.col('schedule_datetime')), '=', currentDate),
                { is_deleted: { [Op.is]: false }, is_completed: false }
            ];
            break;
        case 'future':
            whereClause.is_completed = false;
            currentDate.setDate(currentDate.getDate() + 1);
            whereClause.schedule_datetime = { [Op.gte]: currentDate }
            break;
        default:
            break;
    }

    var courseScheduleds = await CourseScheduled.findAll({
        where: (queryParams && filter) ? whereClause : { is_deleted: false },
        include: includeClause,
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
    let dataObj = {};
    dataObj.count = courseScheduleds.length;
    dataObj.rows = courseScheduleds;
    return dataObj;
}


_getMyAssignments = async (req, res, next) => {
    if (req.user) {
        var attendances = await ClassAttendance.findAll({
            where: { student_id: req.user.id, url: null, attended: true },
            include: [
                {
                    model: Topic,
                    required: true,
                    include: [
                        {
                            model: TopicAssignment,
                            required: true,
                        }
                    ],
                },
                {
                    model: CourseScheduled,
                    include: [
                        { model: Batch },
                        { model: Course }

                    ]
                }
            ],
        });
        const newAttendances = [];
        attendances.forEach(async (attendance) => {
            attendance = attendance.toJSON();
            attendance.topic.topic_assignment.url = await getSignedUrlFromS3(attendance.topic.topic_assignment.url);
            await newAttendances.push(attendance);
        });
        return await newAttendances;
    } else {
        throw "Invalid token, please login again";
    }
}

_getAllSubmittedAssignments = async (req) => {
    if (req.user) {
        const attendances = await ClassAttendance.findAll({
            where: {
                url: { [Op.ne]: null },
                student_id: req.user.id
            },
            include: [
                {
                    model: Topic,
                    include: [
                        { model: TopicAssignment }
                    ],
                },
                {
                    model: CourseScheduled,
                    include: [
                        { model: Batch },
                        { model: Course }

                    ]
                }
            ]
        });
        const updateAttendance = [];
        await attendances.forEach(async (attendance) => {
            attendance = attendance.toJSON();
            attendance.url = await getSignedUrlFromS3(attendance.url);
            await updateAttendance.push(attendance);
        });
        return updateAttendance;
    }
    else {
        throw "Invalid token, please login again";
    }
}

_nextClass = async (req) => {
    currentUtc = new Date();
    var dateFound_mins = new Date(currentUtc);
   var scheduleStartMinTime = new Date(dateFound_mins-60*60000);
   var     scheduleStartPlusTime = new Date(dateFound_mins.getTime() + 60*60000);
    if (req.user) {
        const courseScheduled = await CourseScheduled.findOne({
            where: {
                // schedule_datetime: { [Op.gte]: new Date() },
                schedule_datetime: {
                    [Op.between]: [scheduleStartMinTime, scheduleStartPlusTime],
              },
                is_completed: false,
                is_cancelled:false,
                is_deleted: false
            },
            include: [{
                model: Batch,
                required: true,
                include: [{
                    model: BatchStudent,
                    required: true,
                    where: { student_account_id: req.user.id }
                }]
            }],
            order: [
                ['schedule_datetime', 'ASC']
            ]
        });
        if (courseScheduled) {
            return await courseScheduled;
        } else {
            throw "No Classes found";
        }
    } else {
        throw "Invalid token, please login again";
    }
}

_joinClass = async (req) => {
    if (req.user) {
        var courseScheduled = await CourseScheduled.findOne({
            where: { id: req.body.course_scheduled_id },
            include: [
                { model: Topic },
                { model: Course },
                { model: Teacher },
                {
                    model: Batch,
                    required: true,
                    include: [{
                        model: BatchStudent,
                        required: true,
                        where: { student_account_id: req.user.id }
                    }]
                }
            ]
        });
        if (courseScheduled) {
            courseScheduled = courseScheduled.toJSON();
            if (courseScheduled.teacher.delivery_channel === 'B2B') {
                if (courseScheduled.presenter && courseScheduled.attendee) {
                    return await courseScheduled;
                } else {
                    return await getLiveWebinarAccessTokens()
                        .then(async (responseObj) => {
                            return await createLiveWebinarClass(responseObj, courseScheduled)
                                .then(async (response) => {
                                    const { data, error } = response;
                                    if (error) {
                                        throw error.message;
                                    } else {
                                        const updateDetails = {
                                            attendee: data.hosted_at.attendee,
                                            presenter: data.hosted_at.presenter,
                                            moderator: data.hosted_at.moderator,
                                            host: data.hosted_at.host,
                                            phone_attendee: data.hosted_at.phone_attendee,
                                            phone_presenter: data.hosted_at.phone_presenter,
                                            widget_id: data.id
                                        }
                                        var courseUpdated = await CourseScheduled.update(updateDetails, { where: { id: courseScheduled.id }, returning: true, plain: true });
                                        courseUpdated = courseUpdated[1].toJSON();
                                        return await courseUpdated;
                                    }
                                })
                                .catch((error) => {
                                    throw error;
                                })
                        })
                        .catch((error) => {
                            throw error;
                        })
                }
            } else {
                return await courseScheduled;
            }
        } else {
            throw "Scheduled Class not found";
        }
    } else {
        throw "Invalid token, please login again";
    }
}

_myClassesScheduledForTheDate = async (req) => {
    if (req.user) {
        let { year, month, day } = req.query;
        let nextDate = parseInt(day) + 1
        const dateMentioned = `${year}-${month}-${day}`;
        const nextDateFromDateMentioned = `${year}-${month}-${nextDate}`;
        if(day==31)
        {
            let nextMonth = parseInt(month) + 1
            nextDate=1;
            const nextDateFromDateMentionedEndOfMonth = `${year}-${nextMonth}-${nextDate}`;
            return await CourseScheduled.findAndCountAll({
                where: { 
                    schedule_datetime: { [Op.gte]: dateMentioned, [Op.lt]: nextDateFromDateMentionedEndOfMonth },
                    is_deleted: false
                },
                include: [{
                    model: Course
                },
                {
                    model: Topic
                },
                {
                    model: Batch,
                    required: true,
                    include: [{
                        model: BatchStudent,
                        required: true,
                        where: { student_account_id: req.user.id },
    
                    }]
                }],
            });
        }


        return await CourseScheduled.findAndCountAll({
            where: { 
                schedule_datetime: { [Op.gte]: dateMentioned, [Op.lt]: nextDateFromDateMentioned },
                is_deleted: false
            },
            include: [{
                model: Course
            },
            {
                model: Topic
            },
            {
                model: Batch,
                required: true,
                include: [{
                    model: BatchStudent,
                    required: true,
                    where: { student_account_id: req.user.id },

                }]
            }],
        });
    }
    else {
        throw "Invalid token, please login again";
    }
}

_getLatestScheduleds = async (req) => {
    if (req.user) {
        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();

        const startDate = `${year}-${month}-${day}`;
        
        return await CourseScheduled.findAndCountAll({
            where: { schedule_datetime: { [Op.gte]: new Date(startDate) } },
            include: [{
                model: Course
            }, {
                model: Batch, include: [{
                    model: BatchStudent,
                    where: { student_account_id: req.user.id },
                    include: [{ model: Student }]
                }]
            },
            { model: Teacher, include: [{ model: Account }] },
            { model: Topic }],
            order: [
                ['schedule_datetime', 'ASC']
            ],
            // limit: pageSize,
            // offset: (pageNumber - 1) * pageSize
        });


    } else {
        throw "Invalid token, please login again";
    }
}

_getPastScheduleds = async (req) => {
    if (req.user) {
        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();

        const startDate = `${year}-${month}-${day}`;
        return await CourseScheduled.findAndCountAll({
            where: { schedule_datetime: { [Op.lt]: new Date(startDate) } },
            include: [{
                model: Course
            }, {
                model: Batch, include: [{
                    model: BatchStudent,
                    where: { student_account_id: req.user.id },
                    include: [{ model: Student }]
                }]
            },
            { model: Teacher, include: [{ model: Account }] },
            { model: Topic }],
            order: [
                ['schedule_datetime', 'DESC']
            ],
            // limit: pageSize,
            // offset: (pageNumber - 1) * pageSize
        });


    } else {
        throw "Invalid token, please login again";
    }
}

module.exports = {
    _uploadopicAssignmentReport,
    _monthlyCalendar,
    _getAllCourseSchedules,
    _getMyAssignments,
    _getAllSubmittedAssignments,
    _joinClass,
    _nextClass,
    _myClassesScheduledForTheDate,
    _getLatestScheduleds,
    _getPastScheduleds
}