const { Student, Account, Institute, Role, InstituteHasStudent, AccountHasRole } = require('../../models');
const { getSignedUrlFromS3 } = require('../../_helpers/aws_s3_file_upload');
const bcrypt = require('bcryptjs');
const BCRYPT_SALT_ROUNDS = 12;
const Sequelize = require('sequelize');
const { studentWelcomeMail } = require('../../notification/job-processors/student/student.processor');
const { generateRandomPassword } = require('../../_helpers/password_helper');
const Op = Sequelize.Op;

_updateStudentAccountById = async (req) => {
    var studentAccount = await Student.findOne({ where: { id: req.user.id } });
    studentAccount = studentAccount && studentAccount.toJSON();
    const file = req.file
    req.body.imageUrl = file ? file.location : studentAccount.imageUrl;
    await Student.update(req.body, { where: { id: req.user.id }, returning: true, plain: true });
    var student = await Student.findOne({ where: { id: req.user.id } });
    student = student.toJSON();
    student.imageUrl = await getSignedUrlFromS3(student.imageUrl);
    return await student;
}

_registerStudentForB2C = async (req) => {
    let request = req.body;
    return await Promise.all([
        Student.findOne({
            where: {
                [Op.or]: [
                    { primary_email: request.primary_email }
                ],
            }
        }),
        Account.findOne({ where: { email: request.primary_email } }),
        Institute.findOne({ where: { name: "AscendNow" } }),
        Role.findOne({ where: { name: "student" } })
    ])
        .then(async ([studentAccount, accountMain, instituteAccount, roleInstance]) => {
            if (studentAccount || accountMain) {
                throw "Student Already Exist";
            } else if (!instituteAccount) {
                throw "Institute selected does not exist";
            } else if (!roleInstance) {
                throw "Role provided does not exist";
            } else {
                const randomPassword = await generateRandomPassword();
                const hashPassword = await bcrypt.hash(randomPassword, BCRYPT_SALT_ROUNDS);
                request.password = hashPassword;
                request.email = request.primary_email;
                request.delivery_channel = 'B2C';
                return await Account.create(request)
                    .then(async (account) => {
                        account = account.toJSON();
                        request.code = `${instituteAccount.code}_${request.name.substring(0, 4)}_${account.id}`;
                        const account_id = account.id;
                        const role_id = roleInstance.id;
                        request.account_id = account_id;
                        return await AccountHasRole.create({ account_id, role_id })
                            .then(async (accountHasRole) => {
                                return await Student.create(request)
                                    .then(async (student) => {
                                        return await InstituteHasStudent.create({
                                            institute_id: request.institute_id,
                                            student_account_id: student.id
                                        })
                                            .then(async (instituteHasStudent) => {
                                                studentWelcomeMail(student.toJSON(), randomPassword);
                                                return student;
                                            })
                                            .catch(async (error) => {
                                                await InstituteHasStudent.destroy({
                                                    where: {
                                                        institute_id: request.institute_id,
                                                        student_account_id: student.id
                                                    }, force: true
                                                });
                                                throw error;
                                            })
                                    })
                                    .catch(async (error) => {
                                        await Student.destroy({ where: { primary_email: request.email }, force: true });
                                        throw error;
                                    })
                            })
                            .catch(async (error) => {
                                await AccountHasRole.destroy({ where: { account_id, role_id }, force: true });
                                throw error;
                            });
                    })
                    .catch(async (error) => {
                        await Account.destroy({ where: { email: request.email }, force: true });
                        throw error;
                    })
            }
        })
        .catch((error) => {
            throw error;
        })
}

module.exports = {
    _updateStudentAccountById,
    _registerStudentForB2C
}