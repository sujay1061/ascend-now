const { QuizQuestion, StudentQuizAnswer, Quiz, Topic, ClassAttendance,CourseScheduled} = require("../../models");
const { getSignedUrlFromS3 } = require("../../_helpers/aws_s3_file_upload");

_getQuizQuestionByTopicId = async (req) => {
    let { pageSize, pageNumber } = req.query;
    pageSize = pageSize ? pageSize : 10;
    pageNumber = pageNumber ? pageNumber : 1;
    if (req.user) {
        var quiz = await Quiz.findOne({
            where: { topic_id: req.params.id, is_deleted: false },
            include: [{
                model: QuizQuestion,
                attributes: ['id', 'question', 'type', 'attachment_type', 'points', 'url', 'options', 'quiz_id'],
                limit: pageSize,
                offset: (pageNumber - 1) * pageSize,
                order: [
                    ['createdAt', 'ASC']
                ],
            }],
            order: [
                ['createdAt', 'ASC']
            ],
        });
        if (quiz) {
            quiz = quiz.toJSON();
            const student_quiz_answers = await StudentQuizAnswer.findAll({
                where: { student_account_id: req.user.id, quiz_id: quiz.id }
            })
            quiz.question_count = quiz.quiz_questions ? quiz.quiz_questions.length : 0;
            quiz.student_quiz_answers = student_quiz_answers;
            const quiz_questions = [];
            await quiz.quiz_questions.forEach(async (question) => {
                question.url = await getSignedUrlFromS3(question.url);
                await quiz_questions.push(question);
            });
            quiz.quiz_questions = quiz_questions;
            return await quiz; 
        } else {
            throw "Quiz not found for this Topic";
        }
    } else {
        throw "Invalid token, please login again";
    }
}

_createQuizAnswer = async (req) => {
    const quiz = await Quiz.findOne({ where: { id: req.body.quiz_id } });
    if (quiz) {
        req.body.student_account_id = req.user.id;
        const { student_account_id, quiz_id, is_submitted } = req.body;
        var studentAnswer = await StudentQuizAnswer.findOne({
            where: { student_account_id, quiz_id }
        });
        if (is_submitted) {
            if (studentAnswer) {
                studentAnswer = studentAnswer.toJSON();
                if (studentAnswer.is_submitted) {
                    throw "Quiz already submitted";
                } 
            } else {
                await QuizQuestion.findAll({
                    where: { quiz_id: req.body.quiz_id }
                }).then(async (questions) => {
                    const option_selected_list = req.body.options_selected;
                    var final_points = 0;
                    if (option_selected_list.length === questions.length) {
                        const finalArr = await questions.filter(({ id, answer }) =>
                            option_selected_list.some(({ question_id, option_selected }) => question_id === id && answer === option_selected));
                        await finalArr.forEach(question => final_points += question.points);
                        req.body.final_points = final_points;
                         await StudentQuizAnswer.create(req.body);
                        return {
                            questions,
                            option_selected_list,
                            final_points
                        };
                    } else {
                        throw "Options are not enough to calculate points";
                    }
                }).catch((error) => {
                    throw error;
                })
            }
        } 
        else {
                    throw "student din't submit the quiz"
        }
    } else {
        throw "Quiz not found";
    }
}

_getMyQuizzes = async (req, res, next) => {
    if (req.user) {
        var quizzes = await Quiz.findAndCountAll({
            where: { is_published: true, quiz_type: 1 },
            include: [
                { model: QuizQuestion },
                {
                    model: StudentQuizAnswer
                },
                {
                    model: Topic,
                    required: true,
                    include: [{
                        model: ClassAttendance,
                        required: true,
                        where: { student_id: req.user.id, attended: true }
                    }]
                }
            ]
        });

        quizzes.rows = quizzes.rows.filter(({student_quiz_answers}) => student_quiz_answers.length > 0 ? (!student_quiz_answers[0].is_submitted && student_quiz_answers[0].student_account_id === req.user.id) : true);
        quizzes.count = quizzes.rows.length;
        return await quizzes;
    } else {
        throw "Invalid login session, please login again";
    }
}

_getMyCompletedQuizzes = async (req, res, next) => {
    if (req.user) {
        var quizzes = await Quiz.findAndCountAll({
            include: [
                { model: QuizQuestion },
                {
                    model: StudentQuizAnswer,
                    required: true,
                    where: { is_submitted: true, student_account_id: req.user.id }
                },
                {
                    model: Topic,
                    required: true,
                    include: [{
                        model: ClassAttendance,
                        required: true,
                        where: { student_id: req.user.id, attended: true }
                    }]
                }
            ]
        });

        quizzes = await quizzes.rows.filter((quiz) => quiz.topic);
        return await quizzes;
    } else {
        throw "Invalid login session, please login again";
    }
}

_getInClassQuizQuestionByTopicId = async (req) => {
    let { pageSize, pageNumber } = req.query;
    pageSize = pageSize ? pageSize : 10;
    pageNumber = pageNumber ? pageNumber : 1;
    if (req.user) {
        var quiz = await Quiz.findOne({
            where: { topic_id: req.params.id, is_deleted: false, quiz_type:0 },
            include: [{
                model: QuizQuestion,
                attributes: ['id', 'question', 'type', 'attachment_type', 'points', 'url', 'options', 'quiz_id'],
                limit: pageSize,
                offset: (pageNumber - 1) * pageSize,
                order: [
                    ['createdAt', 'ASC']
                ],
            }],
            order: [
                ['createdAt', 'ASC']
            ],
        });
        if (quiz) {
            quiz = quiz.toJSON();
            const student_quiz_answers = await StudentQuizAnswer.findAll({
                where: { student_account_id: req.user.id, quiz_id: quiz.id }
            })
            quiz.question_count = quiz.quiz_questions ? quiz.quiz_questions.length : 0;
            quiz.student_quiz_answers = student_quiz_answers;
            const quiz_questions = [];
            await quiz.quiz_questions.forEach(async (question) => {
                question.url = await getSignedUrlFromS3(question.url);
                await quiz_questions.push(question);
            });
            quiz.quiz_questions = quiz_questions;
            return await quiz; 
        } else {
            throw "Quiz not found for this Topic";
        }
    } else {
        throw "Invalid token, please login again";
    }
}

_getMyInClassQuiz = async (req, res, next) => {
    if (req.user) {
       return await Quiz.findAll({
            include: [
                { model: QuizQuestion },
                {
                    model: Topic,
                    where: { id: req.query.topic_id },
                    required: true,
                    include: [{
                        model: CourseScheduled,
                        where: { id:req.query.course_scheduled_id,is_quiz_enabled:true }
                    }]
                }
            ]
        }).then(async (courses) => {
            if (courses.length > 0) {
                courses[0] = courses[0] && courses[0].toJSON();
                const attendances = await Quiz.findOne({
                    where: { topic_id: req.query.topic_id},
                    include: [{
                        model: StudentQuizAnswer,
                        where: { is_submitted: true, student_account_id: req.user.id }
                    }]
                });
                courses[0].quizCompleted = attendances ? true : false;
                return await courses[0];
            } else {
                throw "No Quiz found"
            }
        }).catch((error) => {
            throw error;
        });
        // return await quizzes;
    } else {
        throw "Invalid login session, please login again";
    }
}

module.exports = {
    _getQuizQuestionByTopicId,
    _createQuizAnswer,
    _getMyQuizzes,
    _getMyCompletedQuizzes,
    _getInClassQuizQuestionByTopicId,
    _getMyInClassQuiz
}
