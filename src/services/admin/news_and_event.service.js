const { NewsAndEvent } = require('../../models');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

_getAllNewsAndEvents = async (request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    return await NewsAndEvent.findAndCountAll({ 
        where: (queryParams && search) ? { title: { [Op.like]: `%${search}%` }, is_deleted: false } : 
        {is_deleted: false},
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
}

_getNewsAndEventById = async (params) => {
    return await NewsAndEvent.findOne({ where: { id: params.id } });
}

_createNewNewsAndEvent = async (data) => {
    return await NewsAndEvent.create(data);
}

_updateNewsAndEventById = async (req) => {
    return await NewsAndEvent.update(req.body, { where: { id: req.params.id }, returning: true, plain: true });
}

_deleteNewsAndEventById = async (params) => {
    return await NewsAndEvent.update({ is_deleted: true }, { where: { id: params.id }, returning: true, plain: true });
}

_getNewsAndEventsForAWeek = async () => {
    const sevenDaysFromNow = new Date(new Date().setDate(new Date().getDate() + 6));
    var today=new Date(new Date().setDate(new Date().getDate() - 1))
   
    return await NewsAndEvent.findAndCountAll({ 
        where: {
            schedule_datetime: {
                [Op.between]: [today, sevenDaysFromNow],
          }},
        order: [
            ['createdAt', 'DESC']
        ]
    });
}

module.exports = {
    _getAllNewsAndEvents,
    _getNewsAndEventById,
    _createNewNewsAndEvent,
    _updateNewsAndEventById,
    _deleteNewsAndEventById,
    _getNewsAndEventsForAWeek
}