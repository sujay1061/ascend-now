const {BatchStudent} = require("../../models");
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

_getAllBatchStudent=async(request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    return await BatchStudent.findAndCountAll({
        where: (queryParams && search) ? {
            [Op.or]: [
                {
                    [Op.or]: [
                        { name: { [Op.like]: `%${search}%` }, is_deleted: false }
                    ]
                }

            ],
        } : {is_deleted: false},
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize

    });
}

_getBatchStudentById=async(batchId) =>{
    return await BatchStudent.findOne({where: {id:batchId}});
}

_createNewBatchStudent = async (batchStudentdata) => {
  
    return await BatchStudent.create(batchStudentdata);
}

_updateBatchStudentById = async (batchId,BatchStudentData) => {
    return await BatchStudent.update(BatchStudentData, { where: { id: batchId }, returning: true, plain: true });
}

_deleteBatchStudentById = async (batchId) => {
    return await BatchStudent.update({ is_deleted: true }, { where: { id:batchId }, returning: true, plain: true });
}



module.exports={
    _getAllBatchStudent,
    _getBatchStudentById,
    _createNewBatchStudent,
    _updateBatchStudentById,
    _deleteBatchStudentById
}

