const { Topic, Course } = require('../../models');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const { getSignedUrlFromS3 } = require("../../_helpers/aws_s3_file_upload")

_getAllTopics = async (request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    var topics = await Topic.findAndCountAll({
        where: (queryParams && search) ? 
            { name: { [Op.like]: `%${search}%` }, is_deleted: false }
         : {is_deleted: false},
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize,
        include: [{
            model: Course
        }],
    });
    topics.rows = topics.rows.map(async(topic) => {
        topic = topic.toJSON();
        topic.topic_attachment = topic.topic_attachment ? await getSignedUrlFromS3(topic.topic_attachment) : topic.topic_attachment;
        topic.topic_script = topic.topic_script ? await getSignedUrlFromS3(topic.topic_script) : topic.topic_script;
        return await topic;
    });
    topics.rows = await Promise.all(topics.rows);
    return await topics;
}

_getTopicById = async (topicId) => {
    var topic = await Topic.findOne({ where: { id: topicId } });
    topic = topic && topic.toJSON();
    topic.topic_attachment = topic.topic_attachment ? await getSignedUrlFromS3(topic.topic_attachment) : topic.topic_attachment;
    topic.topic_script = topic.topic_script ? await getSignedUrlFromS3(topic.topic_script) : "";
    return topic;

}

_createNewTopic = async (topicData) => {
    topicData.code="TOPIC"+ Math.floor(100 + Math.random() * 900)
    const course = await Course.findOne({ where: { id: topicData.course_id }});
    if(course) {
        return await Topic.create(topicData);
    } else {
        throw "Course Selected not found";
    }
}

_updateTopicById = async (req) => {

    var topic = await Topic.findOne({ where: { id: req.params.id, is_deleted: false } });
    if(topic) {
        topic = topic.toJSON();
        const { topic_attachment, topic_script } = req.files;
        req.body.topic_attachment = (req.files && topic_attachment) ? topic_attachment[0].location : topic.topic_attachment;
        req.body.topic_script = (req.files && topic_script) ? topic_script[0].location : topic.topic_script;
        req.body.updated_by_account_id = req.user.account_id;

        if(req.body.course_id) {
            const course = await Course.findOne({ where: { id: req.body.course_id }});
            if(course) {
                return await Topic.update(req.body, { where: { id: req.params.id }, returning: true, plain: true });
            } else {
                throw "Course Selected not found";
            }
        } else {
            return await Topic.update(req.body, { where: { id: req.params.id }, returning: true, plain: true });
        }
    } else {
        throw "Topic not found";
    }
}

_deleteTopicById = async (topicId) => {
    return await Topic.update({ is_deleted: true }, { where: { id: topicId }, returning: true, plain: true });
}

_getAllTopicsByCourseId = async (courseId) => {
    return await Topic.findAll({ 
        where: { course_id: courseId },
        order: [
            ['createdAt', 'DESC']
        ],
    });
}

_downloadMaterial = async (url) => {
    return url ? await getSignedUrlFromS3(url) : url;
}

_getSignedUrl = async (url) => {
    return url ? await getSignedUrlFromS3(url) : url;
}

module.exports = {
    _getAllTopics,
    _getTopicById,
    _createNewTopic,
    _updateTopicById,
    _deleteTopicById,
    _getAllTopicsByCourseId,
    _downloadMaterial,
    _getSignedUrl
}