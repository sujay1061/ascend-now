const { CourseScheduled, Course, Account, CourseHasTeacher,
    AccountHasRole, Role, Teacher, Batch, BatchStudent, Student, Topic,ClassAttendance
} = require('../../models');
const Sequelize = require('sequelize');
const { teacherCourseScheduleMail, teacherCourseSchedueldInMinutes, B2BTeacherWereAbscent } = require('../../notification/job-processors/teacher/teacher.processor');
const { studentCourseScheduleMail, studentCourseScheduleInMinutes,notificationToStudentOnTeacherAbscent } = require('../../notification/job-processors/student/student.processor');
const { cancelOnTeacherDintJoinTheClass,notificationToAdminTeacherB2BAbscent } = require('../../notification/job-processors/admin/admin.processor');
const Op = Sequelize.Op;
const moment = require('moment');
const schedule = require('node-schedule');

_getAllCourseSchedules = async (request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber, filter } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    var currentDate = new Date();

    let whereClause = { is_deleted: false, delivery_channel:'B2B' };
    switch (filter) {
        case 'completed':
            whereClause.is_completed = true
            break;
        case 'upcoming':
            whereClause = [
                Sequelize.where(Sequelize.fn('date', Sequelize.col('schedule_datetime')), '=', currentDate),
                { is_deleted: { [Op.is]: false }, is_completed: false }
            ];
            break;
        case 'future':
            whereClause.is_completed = false;
            currentDate.setDate(currentDate.getDate() + 1);
            whereClause.schedule_datetime = { [Op.gte]: currentDate }
            break;
        default:
            break;
    }

    return await CourseScheduled.findAndCountAll({
        where: (queryParams && filter) ? whereClause : { is_deleted: false },
        include: [{
            model: Course
        }, {
            model: Batch, include: [{
                model: BatchStudent,
                include: [{ model: Student, where:{ delivery_channel:'B2B'} }]
            }]
        },
        { model: Teacher,  where:{ delivery_channel:'B2B'}, include: [{ model: Account }] },
        { model: Topic }],
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
}

_getCourseScheduleById = async (params) => {
    return await CourseScheduled.findOne({
        where: { id: params.id },
        include: [{
            model: Course
        }, {
            model: Batch, include: [{
                model: BatchStudent,
                include: [{ model: Student }]
            }]
        },
        { model: Teacher, include: [{ model: Account }] },
        { model: Topic }],
    });
}

_createNewCourseSchedule = async (req) => {
    const course = await Course.findOne({ where: { id: req.body.course_id } });
    const account = await Teacher.findOne({
        where: { id: req.body.teacher_account_id },
        include: [
            {
                model: Account, include: [{ model: AccountHasRole, include: [{ model: Role }] }]
            }]
    });
    const batch = await Batch.findOne({
        where: { id: req.body.batch_id },
        include: [{
            model: BatchStudent,
            include: [{ model: Student }]
        }]
    });

    var topic = await Topic.findOne({ where: { id: req.body.topic_id } });
    topic = topic && topic.toJSON();

    var courseScheduled = await CourseScheduled.findOne({
        where: {
            batch_id: req.body.batch_id,
            topic_id: req.body.topic_id,
            is_deleted: false,
        }
    });
    courseScheduled = courseScheduled && courseScheduled.toJSON();

    const isClassNotExist = courseScheduled ? courseScheduled.is_cancelled : true;
    if (!course) {
        throw "Course selected deos not exist";
    } else if (!batch) {
        throw "Batch selected deos not exist";
    } else if (!topic) {
        throw "Topic selected deos not exist";
    } else if (!account || account.account.account_has_role.role.name !== 'teacher') {
        throw "Teacher selected deos not exist";
    } else {
        if (isClassNotExist) {
            // const utcDateTime=toISOWithUtc(req.body.schedule_datetime)
            
            
            const formatDate = new Date(req.body.schedule_datetime);
            const fromDateTime = formatDate;
            const formatNextDateTime = new Date(formatDate);
            const nextDateTime = new Date(formatNextDateTime.setMinutes(formatNextDateTime.getMinutes() + topic.duration_in_mins));
            const teacherClassExitForTimeslot = await CourseScheduled.findOne({
                where: {
                    teacher_account_id: req.body.teacher_account_id,
                    is_deleted: false,
                    is_cancelled: false,
                    schedule_datetime: {
                        [Op.gte]: fromDateTime,
                        [Op.lte]: nextDateTime
                    }
                }
            });
            if (teacherClassExitForTimeslot) {
                throw "This timeslot is already booked for teacher";
            } else {
                var studentsEmail = []
                for (var i = 0; i < batch.batch_students.length; i++) {
                    studentsEmail.push({ primary_email: batch.batch_students[i].student.primary_email, name: batch.batch_students[i].student.name });
                }
                // scheduleDate = moment(req.body.schedule_datetime).subtract(15, 'minutes').format()
                dateFound_date = new Date(req.body.schedule_datetime);
                scheduleDate = new Date(dateFound_date-15*60000);
                const job = schedule.scheduleJob(scheduleDate, function () {
                    teacherCourseSchedueldInMinutes(account,batch,topic)
                    studentCourseScheduleInMinutes(studentsEmail,account,topic.name)
                });
              
                // studentCourseScheduleMail(studentsEmail)
                // teacherCourseScheduleMail(account)
                studentCourseScheduleMail(studentsEmail,account,topic,req.body.date_time_schedule)
                teacherCourseScheduleMail(account,batch,topic,req.body.date_time_schedule)
             
                // return account
                // req.body.schedule_datetime=utcDateTime;
                // req.body.schedule_datetime = new Date(utcDateTime);
                return await Promise.all([
                    CourseScheduled.create(req.body),
                    CourseHasTeacher.create({
                        course_id: req.body.course_id,
                        account_id: req.body.teacher_account_id,
                        created_by_account_id: req.body.created_by_account_id
                    })
                ])
                    .then(([courseScheduled, courseHasTeacher]) => {
                        var courseScheduleds=courseScheduled.toJSON()
                        console.log(scheduleEndTime+ " time to end ")
                        var dateFound_mins = new Date(req.body.schedule_datetime);
                   var     scheduleEndTime = new Date(dateFound_mins.getTime() + 20*60000);
                   
                let bothTeacherAndStudentAbsecent=0;
                        const endClass = schedule.scheduleJob(scheduleEndTime,async function () {
                          var courseScheduledData = await CourseScheduled.findOne({ where: { id: courseScheduleds.id } });
                          if(courseScheduledData.presenter==null)
                          {
                              console.log("came to cancellation")
                              bothTeacherAndStudentAbsecent=1;
                              cancelOnTeacherDintJoinTheClass(account,studentsEmail,topic,req.body.schedule_datetime);
                              B2BTeacherWereAbscent(account,topic.name,batch,req.body.schedule_datetime);
                             CourseScheduled.update({is_cancelled:true}, { where: { id: courseScheduleds.id }});
                          }
                        //   if(bothTeacherAndStudentAbsecent==0)
                        //   {
                        //       var     teacherAbscentMailTriggerTime = new Date(dateFound_mins.getTime() + 5*60000);
                        //       console.log(teacherAbscentMailTriggerTime)
                        //       const jobs = schedule.scheduleJob(teacherAbscentMailTriggerTime, async function () {
                        //        let  teacherPresent=await ClassAttendance.findAll({
                        //               where: { course_scheduled_id: courseScheduleds.id }
                        //           })
                        //           console.log(teacherPresent.length)
                        //           if(teacherPresent.length==0)
                        //           {
                        //               B2BTeacherWereAbscent(account,topic.name,batch,dateFound_mins)
                        //               notificationToStudentOnTeacherAbscent(account,studentsEmail,topic.name,dateFound_mins)
                        //               notificationToAdminTeacherB2BAbscent(account,topic.name,batch,dateFound_mins)
                        //               CourseScheduled.update({is_cancelled:true}, { where: { id: courseScheduleds.id }});
                        //           }
                        //       });
                        //   }


                        });

                    //     if(bothTeacherAndStudentAbsecent==0)
                    // {
                    //     var     teacherAbscentMailTriggerTime = new Date(dateFound_mins.getTime() + 3*60000);
                    //     console.log(teacherAbscentMailTriggerTime)
                    //     const jobs = schedule.scheduleJob(teacherAbscentMailTriggerTime, async function () {
                    //      let  teacherPresent=await ClassAttendance.findAll({
                    //             where: { course_scheduled_id: courseScheduleds.id }
                    //         })
                    //         console.log(teacherPresent.length)
                    //         if(teacherPresent.length==0)
                    //         {
                    //             B2BTeacherWereAbscent(account,topic.name,batch,dateFound_mins)
                    //             notificationToStudentOnTeacherAbscent(account,studentsEmail,topic.name,dateFound_mins)
                    //             notificationToAdminTeacherB2BAbscent(account,topic.name,batch,dateFound_mins)
                    //             CourseScheduled.update({is_cancelled:true}, { where: { id: courseScheduleds.id }});
                    //         }
                    //     });
                    // }
                        return courseScheduled;
                    })
                    .catch((error) => {
                        throw error;
                    })
            }
        } else {
            throw "Class already scheduled for this topic and batch";
        }
    }
}

_updateCourseScheduleById = async (req) => {
    const course = await Course.findOne({ where: { id: req.body.course_id } });
    const account = await Teacher.findOne({
        where: { id: req.body.teacher_account_id },
        include: [
            {
                model: Account, include: [{ model: AccountHasRole, include: [{ model: Role }] }]
            }]
    });
    const courseScheduled = await CourseScheduled.findOne({
        where: {
            id: req.params.id, is_completed: false,
            is_deleted: false,
        },
        include: [{ model: CourseHasTeacher }]
    });
    const batch = await Batch.findOne({ where: { id: req.body.batch_id } });
    const topic = await Topic.findOne({ where: { id: req.body.topic_id } });
    if (!course) {
        throw "Course selected deos not exist";
    } else if (!batch) {
        throw "Batch selected deos not exist";
    } else if (!topic) {
        throw "Topic selected deos not exist";
    } else if (!account || account.account.account_has_role.role.name !== 'teacher') {
        throw "Teacher selected deos not exist";
    } else if (courseScheduled) {
        if (courseScheduled.course_has_teacher) {
            return await Promise.all([
                CourseScheduled.update(req.body, { where: { id: req.params.id }, returning: true, plain: true }),
                CourseHasTeacher.update({
                    account_id: req.body.teacher_account_id,
                    updated_by_account_id: req.body.updated_by_account_id
                }, { where: { id: courseScheduled.course_has_teacher.id } })
            ])
                .then(([courseScheduled, courseHasTeacher]) => {
                    return courseScheduled;
                })
                .catch((error) => {
                    throw error;
                })
        } else {
            return await Promise.all([
                CourseScheduled.update(req.body, { where: { id: req.params.id }, returning: true, plain: true }),
                CourseHasTeacher.create({
                    course_id: req.body.course_id,
                    account_id: req.body.teacher_account_id,
                    created_by_account_id: req.body.created_by_account_id
                })
            ])
                .then(([courseScheduled, courseHasTeacher]) => {
                    return courseScheduled;
                })
                .catch((error) => {
                    throw error;
                })
        }

    } else {
        throw "Class already scheduled for this topic and batch";
    }
}

_updateCourseCompletedById = async (params) => {
    return await CourseScheduled.update({ is_completed: true }, { where: { id: params.id }, returning: true, plain: true });
}

_monthlyCalendar = async (request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { year, month, pageSize, pageNumber } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;

    const startDate = `${year}-${month}-01`;
    const endDate = `${year}-${month}-${new Date(2021, 03, 0).getDate()}`;

    return await CourseScheduled.findAndCountAll({
        where: {
            schedule_datetime: { [Op.gte]: new Date(startDate), [Op.lte]: new Date(endDate) },
            is_deleted: false
        },
        include: [{
            model: Course
        }, {
            model: Batch, include: [{
                model: BatchStudent,
                include: [{ model: Student }]
            }]
        },
        { model: Teacher, include: [{ model: Account }] },
        { model: Topic }],
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
}

_deleteCourseScheduleById = async (params) => {
    return await CourseScheduled.update({ is_deleted: true }, { where: { id: params.id }, returning: true, plain: true });
}

_getTeacherScheduleForAWeek = async (req) => {
    var currentDate = new Date();
    var nextDate = new Date();
    const sevenDaysFromNow = nextDate.setDate(nextDate.getDate() + 7);
    return await CourseScheduled.findAndCountAll({
        where: {
            teacher_account_id: req.user.id,
            is_completed: false,
            is_deleted: false,
            schedule_datetime: {
                [Op.gte]: currentDate,
                [Op.lte]: sevenDaysFromNow
            }
        },
        include: [{
            model: Course
        }, {
            model: Batch
        }, {
            model: Topic
        }],
        order: [
            ['createdAt', 'DESC']
        ]
    });
}

function toISOWithUtc(datetime) {
    //convert timestamp to UTC fomrat
 var utcFormat=    moment(datetime).utc().format("YYYY-MM-DD HH:mm:ss");
 // gives the current time in UTC time zone
 var dateFOund= new Date(utcFormat).toUTCString();
 // converts back the utc time zone to timestamp
 var formatNextDateTime_o = new Date(dateFOund);
     return dateFOund
}


module.exports = {
    _getAllCourseSchedules,
    _getCourseScheduleById,
    _createNewCourseSchedule,
    _updateCourseScheduleById,
    _deleteCourseScheduleById,
    _updateCourseCompletedById,
    _getTeacherScheduleForAWeek,
    _monthlyCalendar
}