const { Course, Topic, Account } = require('../../models');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

_getAllCourses = async (request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    return await Course.findAndCountAll({
        where: (queryParams && search) ? {
            [Op.or]: [
                {
                    [Op.or]: [
                        { name: { [Op.like]: `%${search}%` }, is_deleted: false }
                    ]
                }
            ],
        } : {is_deleted: false},

        include:[{ model:Topic }],
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });

}

_getCourseById = async (courseId) => {
    return await Course.findOne({ where: { id: courseId } });

}

_createNewCourse = async (courseData) => {
   
    courseData.code="COURSE"+ Math.floor(100 + Math.random() * 900)
    return await Course.create(courseData);
 
}

_updateCourseById = async (courseId,courseUpdate) => {

    return await Course.update(courseUpdate, { where: { id: courseId }, returning: true, plain: true });

}


_deleteCourseById = async (CourseId) => {
    return await Course.update({ is_deleted: true }, { where: { id: CourseId}, returning: true, plain: true });
}

module.exports = {
    _getAllCourses,
    _getCourseById,
    _createNewCourse,
    _updateCourseById,
    _deleteCourseById
 
}