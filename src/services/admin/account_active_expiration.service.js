const { AccountActiveExpiration, Account } = require("../../models");


_getAllAccountSessions = async () => {
    return await AccountActiveExpiration.findAndCountAll({
        where: {},
        include: [{
            model: Account
        }]
    });
}

_getAllActiveSessions = async () => {
    return await AccountActiveExpiration.findAndCountAll({
        where: { is_active: true },
        include: [{
            model: Account
        }]
    });
}

module.exports = {
    _getAllAccountSessions,
    _getAllActiveSessions
}