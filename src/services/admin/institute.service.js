const { Institute, Account } = require('../../models');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

_getAllInstitutes = async (request) => {
  const queryParams = Object.keys(request.query).length !== 0 ? true : false;
  let { search, pageSize, pageNumber } = request.query;
  pageSize = pageSize ? pageSize : 100;
  pageNumber = pageNumber ? pageNumber : 1;

  return await Institute.findAndCountAll({
    where: (queryParams && search) ? {
      [Op.or]: [
        {
          [Op.or]: [
            { name: { [Op.like]: `%${search}%` }, is_deleted: false }
          ]
        }
      ]
    } : { is_deleted: false },
    order: [
      ['createdAt', 'DESC']
    ],
    limit: pageSize,
    offset: (pageNumber - 1) * pageSize

  });
}

_getInstitutesById = async (instituteId) => {

  return await Institute.findOne({ where: { id: instituteId } })
}

_createNewInstitute = async (institute) => {
  var codeName = institute.body.name.toUpperCase();
  var instituteSpliced = codeName.substring(0, 4);
  var random = Math.floor(1000 + Math.random() * 9000);
  institute.body.code = instituteSpliced + random;
  institute.body.created_by_account_id = institute.user.account_id;
  return await Institute.create(institute.body);
}

_updateInstituteById = async (instituteId, updateInstitute) => {
  return Institute.update(updateInstitute, {
    where: { id: instituteId }
  })
}

_deleteInstituteById = async (instituteId) => {
  return await Institute.update({ is_deleted: true }, { where: { id: instituteId }, returning: true, plain: true });
}

module.exports = {
  _getAllInstitutes,
  _getInstitutesById,
  _createNewInstitute,
  _updateInstituteById,
  _deleteInstituteById

}