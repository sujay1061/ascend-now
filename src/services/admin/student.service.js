const { Student, AccountHasRole, Role, Account, InstituteHasStudent, Institute, BatchStudent, Batch } = require('../../models');
const bcrypt = require('bcryptjs');
const BCRYPT_SALT_ROUNDS = 12;
const Sequelize = require('sequelize');
const { studentWelcomeMail, studentsParentWelcomeMail } = require('../../notification/job-processors/student/student.processor');
const { generateRandomPassword } = require('../../_helpers/password_helper');
const { where } = require('sequelize');
const Op = Sequelize.Op;

_getAllStudents = async (request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    return await Student.findAndCountAll({
        where: (queryParams && search) ? {
            [Op.or]: [
                { name: { [Op.like]: `%${search}%` }, is_deleted: false },
                { primary_email: { [Op.like]: `%${search}%` }, is_deleted: false },
                { primary_number: { [Op.like]: `%${search}%` }, is_deleted: false }
            ],
        } : { is_deleted: false },
        include: [{
            model: Account,
            include: [{
                model: AccountHasRole,
                include: [{ model: Role }]
            }],
        }, {
            model: InstituteHasStudent,
            include: [{
                model: Institute
            }]
        }],
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
}

_getStudentAccountById = async (params) => {
    return await Student.findOne({ where: { id: params.id } });
}

_createNewStudentAccount = async (req) => {
    let request = req.body;
    let sessionUser = req.user;
    const roleName = 'student';
    if (sessionUser) {
        return await Promise.all([
            Student.findOne({
                where: {
                    [Op.or]: [
                        { primary_email: request.primary_email },
                        { secondary_email: request.secondary_email }
                    ],
                }
            }),
            Account.findOne(
                { 
                    where: {
                        [Op.or]: [{ email: request.primary_email }, { secondary_email: request.secondary_email }],
                        }
             }),
            Institute.findOne({ where: { id: request.institute_id } }),
            Role.findOne({ where: { name: roleName } })
        ])
            .then(async ([studentAccount, accountMain, instituteAccount, roleInstance]) => {
                if (studentAccount || accountMain) {
                    throw "Student Already Exist";
                } else if (!instituteAccount) {
                    throw "Institute selected does not exist";
                } else if (!roleInstance) {
                    throw "Role provided does not exist";
                } else {
                    const randomPassword = await generateRandomPassword();
                    const hashPassword = await bcrypt.hash(randomPassword, BCRYPT_SALT_ROUNDS);
                    request.password = hashPassword;
                    request.email = request.primary_email;
                    request.secondary_email = request.secondary_email;
                    return await Account.create(request)
                        .then(async (account) => {
                            request.code = `${instituteAccount.code}_${request.name.substring(0, 4)}_${account.id}`;
                            request.code  = request.code.toUpperCase();
                            request.created_by_account_id = sessionUser.id;
                            const account_id = account.id;
                            const role_id = roleInstance.id;
                            request.account_id = account_id;
                            return await AccountHasRole.create({ account_id, role_id })
                                .then(async (accountHasRole) => {
                                    return await Student.create(request)
                                        .then(async (student) => {
                                            return await InstituteHasStudent.create({
                                                institute_id: request.institute_id,
                                                student_account_id: student.id
                                            })
                                                .then(async (instituteHasStudent) => {
                                                    studentWelcomeMail(student.toJSON(), randomPassword);
                                                    studentsParentWelcomeMail(student.toJSON(), randomPassword);
                                                    return student;
                                                })
                                                .catch(async (error) => {
                                                    await Student.destroy({ where: { primary_email: request.email }, force: true });
                                                    throw error;
                                                })
                                        })
                                        .catch(async (error) => {
                                            await AccountHasRole.destroy({ where: { account_id, role_id }, force: true });
                                            throw error;
                                        })
                                })
                                .catch(async (error) => {
                                    await Account.destroy({ where: { email: request.email }, force: true });
                                    throw error;
                                });
                        })
                        .catch(async (error) => {
                            throw error;
                        })
                }
            })
            .catch((error) => {
                throw error;
            })
    } else {
        throw "Invalid token, please login again!"
    }
}

_updateStudentAccountById = async (req) => {
    var studentAccount = await Student.findOne({
        where: { id: req.params.id }
    });
    if (studentAccount) {
        studentAccount = studentAccount.toJSON();
        if (!req.body.institute_id) {
            await Account.update({ email: req.body.primary_email,secondary_email: req.body.secondary_email },{ where: { id: studentAccount.account_id }, returning: true, plain: true })
            return await Student.update(req.body, { where: { id: req.params.id }, returning: true, plain: true })
        } else {
            await Promise.all([
                await Student.update(req.body, { where: { id: req.params.id }, returning: true, plain: true }),
                await Account.update( { email: req.body.primary_email,secondary_email: req.body.secondary_email },{ where: { id: studentAccount.account_id }, returning: true, plain: true }),
                await InstituteHasStudent.update(
                    { institute_id: req.body.institute_id },
                    { where: { student_account_id: req.params.id }, returning: true, plain: true }
                )
            ]);
            return await Student.findOne({
                where: { id: req.params.id },
                include: [{
                    model: InstituteHasStudent,
                    include: [{ model: Institute }]
                }]
            });
        }
    } else {
        throw "Selected student not found";
    }
}

_deleteStudentAccountById = async (params) => {
    const student = await Student.findOne({
        where: {
            id: params.id,
        },
        include: [{
            model: Account
        }]
    });
    if (student) {
        return await Promise.all([
            Student.update({ is_deleted: true }, { where: { id: params.id } }),
            Account.update({ is_deleted: true }, { where: { id: student.account.id } }),
            BatchStudent.update({ is_deleted: true }, { where: { student_account_id: params.id } })
        ]);
    } else {
        throw "Student not found";
    }
}

_getAllStudentBatches = async (req) => {
    return await Batch.findAndCountAll({
        include: [{
            model: BatchStudent,
            where: { student_account_id: req.params.studentId, is_deleted: false }
        }], where: { is_deleted: false }
    });
}

_getAllInActiveStudents = async (request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    return await Student.findAndCountAll({
        where: (queryParams && search) ? {
            [Op.or]: [
                { name: { [Op.like]: `%${search}%` }, is_deleted: true },
                { primary_email: { [Op.like]: `%${search}%` }, is_deleted: true },
                { primary_number: { [Op.like]: `%${search}%` }, is_deleted: true }
            ],
        } : { is_deleted: true },
        include: [{
            model: Account,
            include: [{
                model: AccountHasRole,
                include: [{ model: Role }]
            }],
        }, {
            model: InstituteHasStudent,
            include: [{
                model: Institute
            }]
        }],
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
}

_activateStudentAccountById = async (params) => {
    const student = await Student.findOne({
        where: {
            id: params.id,
        },
        include: [{
            model: Account
        }]
    });
    if (student) {
        return await Promise.all([
            Student.update({ is_deleted: false }, { where: { id: params.id } }),
            Account.update({ is_deleted: false }, { where: { id: student.account.id } })
            // BatchStudent.update({ is_deleted: true }, { where: { student_account_id: params.id } })
        ]);
    } else {
        throw "Student not found";
    }
}

module.exports = {
    _getAllStudents,
    _getStudentAccountById,
    _createNewStudentAccount,
    _updateStudentAccountById,
    _deleteStudentAccountById,
    _getAllStudentBatches,
    _getAllInActiveStudents,
    _activateStudentAccountById
}