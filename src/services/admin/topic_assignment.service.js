const { TopicAssignment, Account, AccountHasRole, Role, Topic } = require('../../models');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const { getSignedUrlFromS3 } = require("../../_helpers/aws_s3_file_upload")

_getAllTopicAssignment = async (request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    return await TopicAssignment.findAndCountAll({
        where: (queryParams && search) ?
            { name: { [Op.like]: `%${search}%` }, is_deleted: false } :
            { is_deleted: false },
        include: [{ model: Topic }],
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
}

_getTopicAssignmentById = async (TopicAssignmentId) => {
    var topicAssignment= await TopicAssignment.findOne({ where: { id: TopicAssignmentId } });
    topicAssignment = topicAssignment && topicAssignment.toJSON();
    topicAssignment.topicAssignment_attachment = topicAssignment.url ? await getSignedUrlFromS3(topicAssignment.url) : topicAssignment.url;
    return topicAssignment;

}

_createNewTopicAssignment = async (TopicAssignmentdata) => {
    const topic= await TopicAssignment.findOne({where:{topic_id:TopicAssignmentdata.topic_id}});
    if(!topic)
    {
     return await TopicAssignment.create(TopicAssignmentdata);
    }
    else
    {
     throw "topic assignment already present for this topic"
    }
    
 }

_updateTopicAssignmentById = async (body, TopicAssignmentId) => {
    return await TopicAssignment.update(body, { where: { id:TopicAssignmentId }, returning: true, plain: true });   
}

_deleteTopicAssignmentById = async (id) => {
    return await TopicAssignment.update({ is_deleted: true }, { where: { id }, returning: true, plain: true });
}


module.exports = {
    _getAllTopicAssignment,
    _getTopicAssignmentById,
    _createNewTopicAssignment,
    _updateTopicAssignmentById,
    _deleteTopicAssignmentById
}