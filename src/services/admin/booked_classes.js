const { BookedClasses, Topic, Teacher, BookedClassTimeslot, Student, Course, CourseScheduled ,Batch, BatchStudent,ClassAttendance} = require("../../models");
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const {cancelBookedClassByAdminForTeacher,cancelBookedClassByAdminForStudent} = require('../../notification/job-processors/admin/admin.processor');
_bookClassWithStudentTeacher = async (req) => {
    if (req.user) {
        var startDate_scheduler;
        const { topic_id, teacher_account_id, timeslots, student_account_id, hours } = req.body;
        req.body.is_requested = false;
        req.body.is_accepted = true;
        req.body.booked_by = 'admin';
        const topic = await Topic.findOne({ where: { id: topic_id, is_deleted: false } });
        const teacher = await Teacher.findOne({ where: { id: teacher_account_id, is_deleted: false, delivery_channel: 'B2C' } });
        const student = await Student.findOne({ where: { id: student_account_id, is_deleted: false, delivery_channel: 'B2C' }});
        const bookedClassesFound =await BookedClasses.findOne({ where: { student_account_id: student_account_id, topic_id : topic_id } });
        if (!teacher) {
            throw "Selected teacher not found";
        } else if (!student) {
            throw "Selected student not found";
        } else if (!topic) {
            throw "Selected topic not found";
        } 
        else if (bookedClassesFound) {
            throw "Student has already taken up with this topic";
        } 
        else {
            const start_date = new Date(timeslots['1'].start_date);
            const end_date_format = new Date(start_date);
            const minutes = hours == 2 ? 120 : hours == 1.5 ? 90 : 60;
            const end_date = new Date(end_date_format.setMinutes(end_date_format.getMinutes() + minutes));

            const checkForTimeslot = await BookedClasses.findOne({
                where: { teacher_account_id, student_account_id, is_deleted: false },
                include: [{
                    model: BookedClassTimeslot,
                    required: true,
                    where: {
                        [Op.or]: [
                            {
                                start_date: {
                                    [Op.gte]: start_date,
                                    [Op.lte]: end_date
                                },
                                end_date: {
                                    [Op.gte]: start_date,
                                    [Op.lte]: end_date
                                }
                            }
                        ]
                    }
                }]
            });
            if (!checkForTimeslot) {
                return await BookedClasses.create(req.body)
                    .then(async (classes) => {
                        const timeslotPromises = [];
                        classes = classes.toJSON();
                        await Object.keys(timeslots).map(async (timeslot, index) => {
                            const timeslotObj = {
                                slot_no: index + 1,
                                start_date: start_date,
                                end_date: end_date,
                                hours,
                                booked_id: classes.id,
                                created_by_account_id: req.user.id,
                                is_confirmed:true
                            }
                            startDate_scheduler=  timeslotObj.start_date
                            await timeslotPromises.push(timeslotObj);
                        })
                        req.body.booked_class_id=classes.id;
                        req.body.delivery_channel='B2C';
                        req.body.batchName=`B2C`+student_account_id+req.user.id+req.body.topic_id;
                        req.body.scheduled_name=req.body.name;
                        req.body.schedule_datetime=startDate_scheduler;
                        req.body.teacher_account_id=req.user.id;
                        batchCreation={
                            name:req.body.batchName,
                            description:req.body.name,
                            location:{"address":"banagalore","pincode":560063},
                            students:[student_account_id]
                        }
                        return await BookedClassTimeslot.bulkCreate(timeslotPromises)
                            .then(async (booked_timeslots) => {
                                classes.booked_timeslots = booked_timeslots;
                                return await Batch.create(batchCreation)
                                .then(async(batchDeatils)=>{
                                    const students = batchCreation.students;
                                    let createStudents = [];
                                    await students.forEach(async studentId => {
                                        const newStudentBatch = {
                                            student_account_id: studentId,
                                            batch_id: batchDeatils.id,
                                            marks: 0.00,
                                            marks_date: new Date()
                                        }
                                        createStudents.push(newStudentBatch);
                                    });
                                    await BatchStudent.bulkCreate(createStudents);
                                    const courseScheduleDeatils={
                                        schedule_datetime:startDate_scheduler,
                                        teacher_account_id:teacher_account_id,
                                        scheduled_name:req.body.name,
                                        course_id:teacher.course_id,
                                        topic_id:req.body.topic_id,
                                        batch_id:batchDeatils.id,
                                        createdAt:new Date(),
                                        updatedAt:new Date(),
                                        delivery_channel:"B2C",
                                        booked_class_id:req.body.booked_class_id

                                    }
                                    return await CourseScheduled.create(courseScheduleDeatils)
                                    .then(async (courseScheduleClass) => {
                                      return await classes;
                                    })
                                    .catch(async(error)=>{
                                        await CourseScheduled.destroy({ where: { booked_class_id: classes.id }, force: true })
                                    })
                                })
                                .catch(async(error)=>{
                                    await Batch.destroy({ where: { course_id: teacher.course_id,topic_id:req.body.topic_id }, force: true })
                                })
                            })
                            .catch(async (error) => {
                                await BookedClasses.destroy({ where: { id: classes.id }, force: true })
                                throw error;
                            });
                    })
                    .catch((error) => {
                        throw error;
                    });
            } else {
                throw "This timeslot is already booked for the teacher and student";
            }
        }
    } else {
        throw "Invalid token, please login again";
    }
}

_cancelClassWithStudentTeacher = async (req) => {
    if(req.user) {

        return await BookedClasses.findOne({ where: { id: req.params.id, is_completed: false }})
        .then(async (bookedClass) => {
            if(bookedClass.is_completed) {
                throw "Class is already completed";
            } else {
               const teacher= await Teacher.findOne({ where: { id: bookedClass.teacher_account_id }})
              const student=await  Student.findOne({ where: { id: bookedClass.student_account_id }})
             const topic= await  Topic.findOne({ where: { id: bookedClass.topic_id }})
              const courseSchedules=await  CourseScheduled.findOne({ where: { booked_class_id: req.params.id }})
                const updateQuery = {
                    is_requested: false,
                    is_accepted: false,
                    is_rejected: false,
                    is_cancelled: true
                }
                var data={
                    teacher,
                    student,
                    topic,
                    courseSchedules
                }
                cancelBookedClassByAdminForTeacher(teacher,student,topic,courseSchedules)
                cancelBookedClassByAdminForStudent(teacher,student,topic,courseSchedules)
                
                const cancelledClass = await BookedClasses.update(updateQuery, { where: { id: req.params.id }, returning: true, plain: true });
                return cancelledClass[1];
            }
        })
        .catch((error) => {
            throw error;
        })
    } else {
        throw "Invalid token, please login again";
    }
}

_getAllTeachersByDeliveryChannel = async (req) => {
    return await Teacher.findAll({
        where: { delivery_channel: req.params.channel, is_deleted: false },
        order: [
            ['createdAt', 'DESC']
        ],
    });
}

_getAllStudentsByDeliveryChannel = async (req) => {
    return await Student.findAll({ where: { delivery_channel: req.params.channel, is_deleted: false }});
}

_getAllSchedules = async (request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    return await BookedClasses.findAndCountAll({
        where: (queryParams && search) ? {
            [Op.or]: [
                {
                    [Op.or]: [
                        { name: { [Op.like]: `%${search}%` }, is_deleted: false }
                    ]
                }

            ],
        } : {is_deleted: false},
        include: [
            { model: BookedClassTimeslot},
            { model: Teacher },
            { model: Topic ,  include: [{
                model: Course
            }]},
            { model: Student }
        ],
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
}


module.exports = {
    _bookClassWithStudentTeacher,
    _cancelClassWithStudentTeacher,
    _getAllTeachersByDeliveryChannel,
    _getAllStudentsByDeliveryChannel,
    _getAllSchedules
}