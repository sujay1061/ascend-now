const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const BCRYPT_SALT_ROUNDS = 12;

const { Account, AccountHasRole, Role, Student, Teacher, AccountActiveExpiration } = require('../../models/index');
const {forgotPassword,successChangeOfPassword, successChangeOfPasswordParentMail} = require('../../notification/job-processors/account/account.processor');
const { getSignedUrlFromS3 } = require("../../_helpers/aws_s3_file_upload");
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const { TOKEN_SECRET, REFRESH_TOKEN_SECRET, REFRESH_TOKEN_LIFE, TOKEN_LIFE } = process.env;

_loginToAccount = async (request) => {
  var userAccount = await Account.findOne({
    where: { 
      // email: request.email, 
      [Op.or]: [{ email: request.email }, { secondary_email: request.email }],
      is_deleted: false },
    include: [{
      model: AccountHasRole,
      include: [{ model: Role }]
    }]
  });
  if (userAccount) {
    userAccount = userAccount.toJSON();
    const { account_has_role } = userAccount;
    const { role } = account_has_role;
   
    const accountProfile = role && role.name === 'student' ? Student : Teacher;
    var accountInfo = await accountProfile.findOne({
      where: {
        
         [Op.or]: [{ primary_email: request.email }, { secondary_email: request.email }],
        },
      include: [{
        model: Account,
        include: [{
          model: AccountHasRole,
          include: [{ model: Role }]
        }]
      }]
    }); 
    // return userAccount;
   
    accountInfo = accountInfo ? accountInfo.toJSON() : userAccount;
    // return accountInfo.account_has_role.role

    if(accountInfo&&accountInfo.account)
    {

        if(accountInfo.account.account_has_role.role.name==='student')
        { 
                  
                if(accountInfo.secondary_email== request.email) 
                {
                  console.log("came to parent")
                  accountInfo.is_parent=true
                }
                else
                {
                  console.log("came to student")
                  accountInfo.is_parent=false
                }
        }
      
    }
    
    
    accountInfo.imageUrl = accountInfo.imageUrl ? await getSignedUrlFromS3(accountInfo.imageUrl) : accountInfo.imageUrl;
    const validPass = await bcrypt.compare(request.password, userAccount.password)
    if (validPass) {

      const activeSession = {
        last_try_date: new Date(),
        account_id: userAccount.id
      }
      const sessionDetails = await AccountActiveExpiration.create(activeSession);
      const payload = {
        id: accountInfo.id,
        role: userAccount.account_has_role.role.name,
        sessionId: sessionDetails.id,
        account_id: userAccount.id,
        email:accountInfo.email,
        secondary_email:accountInfo.secondary_email
      };
      const token = jwt.sign(payload, TOKEN_SECRET, { expiresIn: TOKEN_LIFE });
      const refreshToken = jwt.sign(payload, REFRESH_TOKEN_SECRET, { expiresIn: REFRESH_TOKEN_LIFE });
      accountInfo.refreshToken = refreshToken;

      return { user: { ...accountInfo }, token }
    } else {
      throw "Invalid password";
    }
  } else {
    throw "Username does not exist";
  }
}

_ResetPassword = async (request) => {
  if (request.user) {
    var userAccount = await Account.findOne({
      where: { id: request.user.account_id, is_deleted: false }
    });
    if (userAccount)
    {
      userAccount = userAccount.toJSON();
      const validPass =  await bcrypt.compare(request.body.password, userAccount.password);
          if(validPass)
          {
            const hashPassword = await bcrypt.hash(request.body.new_password, BCRYPT_SALT_ROUNDS);
            userAccount.password = hashPassword;
            return await Account.update(userAccount, { where: { id:  request.user.account_id }, returning: true, plain: true })
          }
          else {
            throw "Invalid password";
          }
    }
    else
     { 
       throw "User not found";
       }
  
  }
  else {
    throw "Session not found";
  }

}

_forgotPassword = async (request) => {

    var userAccount = await Account.findOne({
      where: { email: request.body.email, is_deleted: false },
      include: [{
        model: AccountHasRole,
        include: [{ model: Role }]
      }]
    });
// console.log(userAccount.toJSON())
roleFind=userAccount.toJSON()
if(roleFind.account_has_role.role_id==3)
{
  // Invert when pushing to dev 2 for student and 3 for teacher
  console.log("teacher")
  var userDetails = await Teacher.findOne({
    where :{primary_email:request.body.email, is_deleted:false}
  })
  console.log(roleFind.account_has_role.role.name)
  console.log(userDetails.name)
}
if(roleFind.account_has_role.role_id==2)
{
   // Invert when pushing to dev 2 for student and 3 for teacher

  console.log("student")
  console.log(roleFind.account_has_role.role.name)
  var userDetails = await Student.findOne({
    where :{primary_email:request.body.email, is_deleted:false}
  })
  console.log(userDetails.name)
}
        if(userAccount)
        {
          userAccount = userAccount.toJSON();
          userName = userDetails.name;
          const token = jwt.sign({
            id: userAccount.id,
            role: userAccount.account_has_role.role.name,
            email:userAccount.email
          }, TOKEN_SECRET, { expiresIn: "10m" }); 
          forgotPassword(userAccount.email,userDetails.name,token)
        }
        else
        {
          throw "User not found";
        }
}


_forgotResetPassword = async (request) => {
  if (request.user) {
    var userAccount = await Account.findOne({
      where: { id: request.user.id, is_deleted: false },
      include: [{
        model: AccountHasRole,
        include: [{ model: Role }]
      }]
    });
    
    if (userAccount)
    {
      // userAccount = userAccount.toJSON();
      roleFind=userAccount.toJSON()
      console.log(roleFind)
    if(roleFind.account_has_role.role_id==3)
    {
      // Invert when pushing to dev 2 for student and 3 for teacher
      console.log("teacher")
      var userDetails = await Teacher.findOne({
        where :{primary_email:roleFind.email, is_deleted:false}
      })
      console.log(roleFind.account_has_role.role.name)
      console.log(userDetails.name)
    }
    if(roleFind.account_has_role.role_id==2)
    {
      // Invert when pushing to dev 2 for student and 3 for teacher

      console.log("student")
      console.log(roleFind.account_has_role.role.name)
      var userDetails = await Student.findOne({
        where :{primary_email:roleFind.email, is_deleted:false}
      })
      console.log(userDetails.name)
    }


            const hashPassword = await bcrypt.hash(request.body.new_password, BCRYPT_SALT_ROUNDS);
            roleFind.password = hashPassword;
            successChangeOfPassword(roleFind.email,userDetails.name,request.body.new_password)
            if(roleFind.account_has_role.role_id==2)
            {
              successChangeOfPasswordParentMail(userDetails.secondary_email,userDetails.name,request.body.new_password)
            }
            
            return await Account.update(roleFind, { where: { id:  request.user.id }, returning: true, plain: true })
    }
    else
     { 
       throw "User not found";
       }
  
  }
  else {
    throw "Session not found";
  }

}

_logoutFromAccount = async (request) => {
  if (request.user) {
    const accountSession = await AccountActiveExpiration.findByPk(request.user.sessionId);
    if (accountSession) {
      const timeNow = new Date();
      const last_try_date = new Date(accountSession.last_try_date);
      const duration_in_mins = Math.round((Math.abs(timeNow - last_try_date) / 1000) / 60);
      accountSession.duration_in_mins = duration_in_mins;
      accountSession.is_active = false;
      accountSession.save();
      return {};
    } else {
      throw "Session not found";
    }
  } else {
    throw "Session not found";
  }
}

_createNewAccount = async (request) => {
  const userAccount = await Account.findOne({ where: { email: request.email } });
  const roleDetails = await Role.findOne({ where: { name: request.role } })
  if (userAccount) {
    throw "Already Registered";
  } else if(!roleDetails) {
    throw "Role provided not found";
  } else {
    request.password = 'pace@123';
    const hashPassword = await bcrypt.hash(request.password, BCRYPT_SALT_ROUNDS);
    request.password = hashPassword;
      return await Account.create(request)
      .then(async(account) => {
        return await AccountHasRole.create({
          account_id: account.id,
          role_id: roleDetails.id
        })
      })
      .then((accountRole) => {
        return accountRole;      
      })
      .catch((err) => {
        throw err;
      })
  }
}

_refreshToken = async (req, res) => {
  let payload
  try{
      payload = jwt.verify(req.token, TOKEN_SECRET);
   }
  catch(e){
    throw e;
  }
  const { id, role, sessionId, account_id } = payload;
  const payloadInfo = { id, role, sessionId, account_id };

   //retrieve the refresh token from the users array
   let refreshToken = req.body.refreshToken;

   //verify the refresh token
   try{
       jwt.verify(refreshToken, REFRESH_TOKEN_SECRET)
   }
   catch(e){
       throw e;
   }

   let newToken = jwt.sign(payloadInfo, TOKEN_SECRET, { expiresIn: TOKEN_LIFE});

    return { token: newToken };
}

_TestloginToAccount = async (request) => {
  var userAccount = await Account.findOne({
    where: { email: request.email, is_deleted: false },
    include: [{
      model: AccountHasRole,
      include: [{ model: Role }]
    }]
  });
  if(userAccount&&userAccount.account_has_role)
  {
    // return userAccount;
    userAccount = userAccount.toJSON();
    const { account_has_role } = userAccount;
    const { role } = account_has_role;
   
    const accountProfile = role && role.name === 'student' ? Student : Teacher;

    var accountInfo = await accountProfile.findOne({
      where: { primary_email: request.email },
      include: [{
        model: Account,
        include: [{
          model: AccountHasRole,
          include: [{ model: Role }]
        }]
      }]
    });
    // console.log(userAccount)
    accountInfo = accountInfo ? accountInfo.toJSON() : userAccount;
    accountInfo.imageUrl = accountInfo.imageUrl ? await getSignedUrlFromS3(accountInfo.imageUrl) : accountInfo.imageUrl;

    const validPass = await bcrypt.compare(request.password, userAccount.password)
    if (validPass) {

      const activeSession = {
        last_try_date: new Date(),
        account_id: userAccount.id
      }
      const sessionDetails = await AccountActiveExpiration.create(activeSession);
      const payload = {
        id: accountInfo.id,
        role: userAccount.account_has_role.role.name,
        sessionId: sessionDetails.id,
        account_id: userAccount.id,
        email:accountInfo.email
      };
      const token = jwt.sign(payload, TOKEN_SECRET, { expiresIn: TOKEN_LIFE });
      const refreshToken = jwt.sign(payload, REFRESH_TOKEN_SECRET, { expiresIn: REFRESH_TOKEN_LIFE });
      accountInfo.refreshToken = refreshToken;

      return { user: { ...accountInfo }, token }
    } else {
      throw "Invalid password";
    }
  }
  
  else
  {
    throw "role not found";
  }
  // console.log(userAccount)
  // if (userAccount) {
  //   userAccount = userAccount.toJSON();
  //   const { account_has_role } = userAccount;
  //   // const { role } = account_has_role;
  //  return userAccount;
    // const accountProfile = role && role.name === 'student' ? Student : Teacher;

    // var accountInfo = await accountProfile.findOne({
    //   where: { primary_email: request.email },
    //   include: [{
    //     model: Account,
    //     include: [{
    //       model: AccountHasRole,
    //       include: [{ model: Role }]
    //     }]
    //   }]
    // });
    // console.log(userAccount)
    // accountInfo = accountInfo ? accountInfo.toJSON() : userAccount;
    // accountInfo.imageUrl = accountInfo.imageUrl ? await getSignedUrlFromS3(accountInfo.imageUrl) : accountInfo.imageUrl;

    // const validPass = await bcrypt.compare(request.password, userAccount.password)
    // if (validPass) {

    //   const activeSession = {
    //     last_try_date: new Date(),
    //     account_id: userAccount.id
    //   }
    //   const sessionDetails = await AccountActiveExpiration.create(activeSession);
    //   const payload = {
    //     id: accountInfo.id,
    //     role: userAccount.account_has_role.role.name,
    //     sessionId: sessionDetails.id,
    //     account_id: userAccount.id,
    //     email:accountInfo.email
    //   };
    //   const token = jwt.sign(payload, TOKEN_SECRET, { expiresIn: TOKEN_LIFE });
    //   const refreshToken = jwt.sign(payload, REFRESH_TOKEN_SECRET, { expiresIn: REFRESH_TOKEN_LIFE });
    //   accountInfo.refreshToken = refreshToken;

    //   return { user: { ...accountInfo }, token }
    // } 
    // else {
    //   throw "Invalid password";
    // }
  // } else {
  //   throw "Username does not exist";
  // }
}

module.exports = {
  _createNewAccount,
  _loginToAccount,
  _logoutFromAccount,
  _ResetPassword,
  _forgotPassword,
  _forgotResetPassword,
  _refreshToken,
  _TestloginToAccount
}





