const { Course, FeedbackQuestion} = require('../../models');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

_getAllfeedbackQuestion = async (request) => {
  const queryParams = Object.keys(request.query).length !== 0 ? true : false;
  let { search, pageSize, pageNumber } = request.query;
  pageSize = pageSize ? pageSize : 100;
  pageNumber = pageNumber ? pageNumber : 1;
  return await FeedbackQuestion.findAndCountAll({
      where: (queryParams && search) ? {
          [Op.or]: [
              {
                  [Op.or]: [
                      { questions: { [Op.like]: `%${search}%` }, is_deleted: false }
                  ]
              }
          ],
      } : {is_deleted: false},
      order: [
          ['createdAt', 'DESC']
      ],
      limit: pageSize,
      offset: (pageNumber - 1) * pageSize
  });

}

_getFeedBackQuestionById = async (feedbackId) => {
    return await FeedbackQuestion.findOne({ where: { id: feedbackId } });

}

_createNewFeedBackQuestion = async (feedbackData) => {
   
    return await FeedbackQuestion.create(feedbackData);
 
}

_updateFeedBackQuestionById = async (feedbackId,Updatedfeedback) => {
  return await FeedbackQuestion.update(Updatedfeedback, { where: { id: feedbackId }, returning: true});
}


_deleteFeedBackQuestionById = async (feedbackId) => {
    return await FeedbackQuestion.update({ is_deleted: true }, { where: { id: feedbackId}, returning: true, plain: true });
}

module.exports = {
  _getAllfeedbackQuestion,
    _createNewFeedBackQuestion,
    _getFeedBackQuestionById,
    _updateFeedBackQuestionById,
    _deleteFeedBackQuestionById
 
}