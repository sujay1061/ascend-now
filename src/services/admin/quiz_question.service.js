const { QuizQuestion, Topic, Quiz } = require('../../models');
const Sequelize = require('sequelize');
const { getSignedUrlFromS3 } = require('../../_helpers/aws_s3_file_upload');
const Op = Sequelize.Op;

_getAllQuizQuestions = async (request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    return await QuizQuestion.findAndCountAll({ 
        where: (queryParams && search) ? { title: { [Op.like]: `%${search}%` }, is_deleted: false } : 
        {is_deleted: false},
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
}

_getQuizQuestionById = async (params) => {
    return await QuizQuestion.findOne({ where: { id: params.id } });
}

_getAllQuizes = async (req) => {
    const queryParams = Object.keys(req.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber } = req.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    return await Quiz.findAndCountAll({ 
        where: (queryParams && search) ? { name: { [Op.like]: `%${search}%` }, is_deleted: false } : 
        {is_deleted: false},
        include: [ { model: Topic } ],
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
}

_createQuiz = async (req) => {
    const topic = await Topic.findOne({ where: { id: req.body.topic_id }});
    const quiz = await Quiz.findOne({ where: { topic_id: req.body.topic_id }});
    if(quiz) {
        throw "Quiz already created for this topic";
    } else {
        if(topic) {
            return await Quiz.create(req.body);
        } else {
            throw "Selected topic not found";
        }
    }
}

_updateQuizById = async (req) => {
    const topic = await Topic.findOne({ where: { id: req.body.topic_id }});
    if(topic) {
        return await Quiz.update(req.body, { where: { id: req.params.id }});
    } else {
        throw "Selected topic not found";
    }
}

_getAllQuizQuestionByQuizId = async (req) => {
    const quiz = await Quiz.findOne({
        where: { id: req.params.id, is_deleted: false },
        include: [{ model: QuizQuestion }, { model: Topic }]
    });

    quiz.quiz_questions = await quiz.quiz_questions.forEach(async (question) => {
        question.url = await getSignedUrlFromS3(question.url);
        return question;
    });

    return await quiz;
}

_createQuizQuestion = async (req) => {
    const file = req.file
    req.body.url = file ? file.location : "";
    const quiz = await Quiz.findOne({ where: { id: req.body.quiz_id }});
    if(quiz) {
        var quizQuestion = await QuizQuestion.create(req.body);
        quizQuestion.url = await getSignedUrlFromS3(quizQuestion.url);
        return quizQuestion;
    } else {
        throw "Selected quiz not found";
    }
}

_updateQuizQuestionById = async (req) => {
    const topic = req.body.topic_id ? await Topic.findOne({ where: { id: req.body.topic_id }}) : true;
    const quizQuestion = await QuizQuestion.findOne({ where: { id: req.params.id }});
    const file = req.file
    req.body.url = file ? file.location : quizQuestion.url;
    if(quizQuestion) {
        if(topic) {
            var question = await QuizQuestion.update(req.body, { where: { id: req.params.id }, returning: true, plain: true });
            question[1].url = await getSignedUrlFromS3(question[1].url);
            return question[1];
        } else {
            throw "Selected topic not found";
        }
    } else {
        throw "Quiz Question not found";
    }
}

_deleteQuizQuestionById = async (params) => {
    return await QuizQuestion.update({ is_deleted: true }, { where: { id: params.id }, returning: true, plain: true });
}

_getQuizQuestionByTopicId = async (req) => {   
    return await QuizQuestion.findAndCountAll({ 
        where: { topic_id: req.params.topicId },
        order: [
            ['createdAt', 'DESC']
        ]
    });
}

_publishQuiz = async (req) => {
    const quiz = await Quiz.findOne(
        { where: { id: req.params.id },
        include: [{ model: QuizQuestion }]
    });
    if(quiz.quiz_questions.length) {
        return await Quiz.update({ is_published: true }, { where: { id: req.params.id }, returning: true, plain: true });
    } else {
        throw "Quiz or Quiz Question not found";
    }
}

module.exports = {
    _getAllQuizQuestions,
    _getQuizQuestionById,
    _getQuizQuestionByTopicId,
    _getAllQuizes,
    _createQuiz,
    _updateQuizById,
    _createQuizQuestion,
    _updateQuizQuestionById,
    _deleteQuizQuestionById,
    _getAllQuizQuestionByQuizId,
    _publishQuiz
}