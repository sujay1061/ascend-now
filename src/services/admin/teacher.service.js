const { Teacher, Account, AccountHasRole, Role, Course, CourseHasTeacher } = require('../../models');
const bcrypt = require('bcryptjs');
const BCRYPT_SALT_ROUNDS = 12;
const Sequelize = require('sequelize');
const { generateRandomPassword } = require('../../_helpers/password_helper');
const { teacherWelcomeMail } = require('../../notification/job-processors/teacher/teacher.processor');
const Op = Sequelize.Op;

_getAllTeachers = async (request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    return await Teacher.findAndCountAll({
        where: (queryParams && search) ? {
            [Op.or]: [
                { name: { [Op.like]: `%${search}%` }, is_deleted: false },
                { primary_email: { [Op.like]: `%${search}%` }, is_deleted: false }
            ],
        } : { is_deleted: false },
        include: [{
            model: Account,
            include: [{
                model: AccountHasRole,
                include: [{ model: Role }]
            }]
        }],
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
}

_getTeacherAccountById = async (params) => {
    return await Teacher.findOne({ where: { id: params.id } });
}

_createNewTeacherAccount = async (req) => {
    let request = req.body;
    let sessionUser = req.user;
    const roleName = 'teacher';
    if (sessionUser) {
        if(req.body.delivery_channel=='B2B' || req.body.delivery_channel=='physical')
        {
            return await Promise.all([
                Teacher.findOne({
                    where: {
                        [Op.or]: [
                            { primary_email: request.primary_email }
                        ],
                    }
                }),
                Account.findOne({ where: { email: request.primary_email } }),
                Role.findOne({ where: { name: roleName } })
            ])
                .then(async ([teacherAccount, accountMain, roleInstance]) => {
                    if (teacherAccount || accountMain) {
                        throw "Email or Phone Number Already Exist";
                    } else if (!roleInstance) {
                        throw "Role provided not found";
                    } else {
                        const randomPassword = await generateRandomPassword();
                        const hashPassword = await bcrypt.hash(randomPassword, BCRYPT_SALT_ROUNDS);
                        request.password = hashPassword;
                        request.email = request.primary_email;
                        return await Account.create(request)
                            .then(async (account) => {
                                request.code = `${request.name.substring(0, 4)}_${account.id}`;
                                request.created_by_account_id = sessionUser.id;
                                const account_id = account.id;
                                const role_id = roleInstance.id;
                                request.account_id = account_id;
                                return await Promise.all([
                                    Teacher.create(request),
                                    AccountHasRole.create({ account_id, role_id })
                                ])
                            })
                            .then(([teacher]) => {
                                teacherWelcomeMail(teacher.toJSON(), randomPassword);
                                return teacher;
                            })
                            .catch(async (error) => {
                                await Account.destroy({ where: { email: request.email }, force: true });
                                await Teacher.destroy({ where: { primary_email: request.email }, force: true });
                                throw error;
                            })
                    }
                })
                .catch((error) => {
                    throw error;
                })
        }

        if(req.body.delivery_channel=='B2C' && req.body.course_id!=null)
        {
            return await Promise.all([
                Teacher.findOne({
                    where: {
                        [Op.or]: [
                            { primary_email: request.primary_email }
                        ],
                    }
                }),
                Account.findOne({ where: { email: request.primary_email } }),
                Role.findOne({ where: { name: roleName } })
            ])
                .then(async ([teacherAccount, accountMain, roleInstance]) => {
                    if (teacherAccount || accountMain) {
                        throw "Email or Phone Number Already Exist";
                    } else if (!roleInstance) {
                        throw "Role provided not found";
                    } else {
                        const randomPassword = await generateRandomPassword();
                        const hashPassword = await bcrypt.hash(randomPassword, BCRYPT_SALT_ROUNDS);
                        request.password = hashPassword;
                        request.email = request.primary_email;
                        return await Account.create(request)
                            .then(async (account) => {
                                request.code = `${request.name.substring(0, 4)}_${account.id}`;
                                request.created_by_account_id = sessionUser.id;
                                const account_id = account.id;
                                const role_id = roleInstance.id;
                                request.account_id = account_id;
                                return await Promise.all([
                                    Teacher.create(request),
                                    AccountHasRole.create({ account_id, role_id })
                                ])
                            })
                            .then(([teacher]) => {
                                teacherWelcomeMail(teacher.toJSON(), randomPassword);
                                return teacher;
                            })
                            .catch(async (error) => {
                                await Account.destroy({ where: { email: request.email }, force: true });
                                await Teacher.destroy({ where: { primary_email: request.email }, force: true });
                                throw error;
                            })
                    }
                })
                .catch((error) => {
                    throw error;
                }) 
        }
        if(req.body.delivery_channel=='B2C' && req.body.course_id==null)
        {
            throw "Course is mandatory for B2C"
        }
       
    } else {
        throw "Invalid token, please login again!"
    }
}

_updateTeacherAccountById = async (req) => {
    var teacherAccount = await Teacher.findOne({
        where: { id: req.params.id }
    });
    await Account.update({ email: req.body.primary_email,secondary_email: req.body.secondary_email},{ where: { id: teacherAccount.account_id }, returning: true, plain: true })
    return await Teacher.update(req.body, { where: { id: req.params.id }, returning: true, plain: true });
}

_deleteTeacherAccountById = async (params) => {
    // return await Teacher.update({ is_deleted: true }, { where: { id: params.id }, returning: true, plain: true });
    const teacher = await Teacher.findOne({
        where: {
            id: params.id,
        },
        include: [{
            model: Account
        }]
    });
    if (teacher) {
        return await Promise.all([
            Teacher.update({ is_deleted: true }, { where: { id: params.id } }),
            Account.update({ is_deleted: true }, { where: { id: teacher.account.id } }),
            // BatchStudent.update({ is_deleted: true }, { where: { student_account_id: params.id } })
        ]);
    } else {
        throw "Teacher not found";
    }
}

_getAllTeachersByDeliveryChannel = async (params) => {
    return await Teacher.findAll({
        where: { delivery_channel: params.channel },
        order: [
            ['createdAt', 'DESC']
        ],
    });
}

_getAllInActiveTeachers = async (request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    return await Teacher.findAndCountAll({
        where: (queryParams && search) ? {
            [Op.or]: [
                { name: { [Op.like]: `%${search}%` }, is_deleted: true },
                { primary_email: { [Op.like]: `%${search}%` }, is_deleted: true }
            ],
        } : { is_deleted: true },
        include: [{
            model: Account,
            include: [{
                model: AccountHasRole,
                include: [{ model: Role }]
            }]
        }],
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
}


_activateTeacherAccountById = async (params) => {
    // return await Teacher.update({ is_deleted: false }, { where: { id: params.id }, returning: true, plain: true });
    // Account.update({ is_deleted: true }, { where: { id: student.account.id } });


    const teacher = await Teacher.findOne({
        where: {
            id: params.id,
        },
        include: [{
            model: Account
        }]
    });
    if (teacher) {
        return await Promise.all([
            Teacher.update({ is_deleted: false }, { where: { id: params.id } }),
            Account.update({ is_deleted: false }, { where: { id: teacher.account.id } })
            // BatchStudent.update({ is_deleted: true }, { where: { student_account_id: params.id } })
        ]);
    } else {
        throw "Teacher not found";
    }
}

module.exports = {
    _getAllTeachers,
    _getTeacherAccountById,
    _createNewTeacherAccount,
    _updateTeacherAccountById,
    _deleteTeacherAccountById,
    _getAllTeachersByDeliveryChannel,
    _getAllInActiveTeachers,
    _activateTeacherAccountById
}