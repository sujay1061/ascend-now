const { ClassActivity, BatchStudent, Account, Student, Topic } = require('../../models');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

_getAllClassActivities = async (req) => {
    const queryParams = Object.keys(req.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber } = req.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    return await ClassActivity.findAndCountAll({
        where: (queryParams && search) ? 
        { name: { [Op.like]: `%${search}%` }, is_deleted: false }
         : {is_deleted: false},
        order: [
            ['createdAt', 'DESC']
        ],
        include: [{ model: Topic }],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
}

_getClassActivityById = async (req) => {
    return await ClassActivity.findOne({ 
        where: { id: req.params.id }
    });
}

_createNewClassActivity = async (req) => {
    return await ClassActivity.create(req.body);
}

_updateClassActivityById = async (req) => {
    return await ClassActivity.update(req.body, { where: { id: req.params.id }, returning: true, plain: true });
}

_deleteClassActivityById = async (req) => {
    return await ClassActivity.update({ is_deleted: true }, { where: { id: req.params.id }, returning: true, plain: true });
}

module.exports = {
    _getAllClassActivities,
    _getClassActivityById,
    _createNewClassActivity,
    _updateClassActivityById,
    _deleteClassActivityById,
}