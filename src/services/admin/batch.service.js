const { Batch, BatchStudent, Account, Student, Teacher } = require('../../models');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

_getAllBatch = async (request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    return await Batch.findAndCountAll({
        where: (queryParams && search) ? {
            [Op.or]: [
                {
                    [Op.or]: [
                        { name: { [Op.like]: `%${search}%` }, is_deleted: false }
                    ]
                },
                {
                    [Op.or]: [
                        { description: { [Op.like]: `%${search}%` }, is_deleted: false }
                    ]
                }

            ],
        } : {is_deleted: false},
        include: [{ 
            model: BatchStudent,
            where: { is_deleted: false },
            include: [{ 
                model: Student
            }]
        }],
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
}

_getBatchById = async (batchId) => {
    return await Batch.findOne({ 
        where: { id:batchId },
        include: [{ 
            model: BatchStudent,
            include: [{ model: Student }]
        }] 
    });
}

_createNewBatch = async (request) => {
    return await Batch.create(request)
    .then( async (batch) => {
        const students = request.students;
        let createStudents = [];
        await students.forEach(async studentId => {
            const newStudentBatch = {
                student_account_id: studentId,
                batch_id: batch.id,
                marks: 0.00,
                marks_date: new Date(),
                course_id:request.course_id
            }
            createStudents.push(newStudentBatch);
        });
        await BatchStudent.bulkCreate(createStudents);
        return await Batch.findByPk(batch.id);
    })
    .catch((err) => {
        throw err;
    })
}

_updateBatchById = async (batchId, batchData) => {
    const batch = await Batch.findOne({
        where: { id: batchId }, 
        include: { model: BatchStudent }
    });
    if(batch) {
        var studentsToRemove = batch.batch_students.filter(item1 => !batchData.students.some(item2 => (item2 === item1.student_account_id)));

        var studentsToAdd = batchData.students.filter(item1 => !batch.batch_students.some(item2 => (item2.student_account_id === item1)));

        let studentIdsToRemove = studentsToRemove.map(({ id }) => id);

        let createStudents = [];
        await studentsToAdd.forEach(async studentId => {
            const newStudentBatch = {
                student_account_id: studentId,
                batch_id: batch.id,
                marks: 0.00,
                marks_date: new Date()
            }
            createStudents.push(newStudentBatch);
        });
        await Batch.update(batchData,{ where:{ id:batchId } });
        await BatchStudent.destroy({ where: { id: studentIdsToRemove } });
        await BatchStudent.bulkCreate(createStudents);
        return await Batch.findOne({
            where: { id: batchId }, 
            include: { model: BatchStudent }
        });
    } else {
        throw "Batch not found";
    }
}

_deleteBatchById = async (batchId) => {
    return await Batch.update({ is_deleted: true }, { where: { id: batchId }, returning: true, plain: true });
}

_getStudentsByDeliveryChannel = async (channel) => {
    return await Student.findAll({ where: { delivery_channel: channel, is_deleted: false }});
}

_getTeachersByBatchDeliverChannel = async (req) => {
    var batch = await Batch.findOne({ 
        where: { id: req.params.id },
        include: [{ 
            model: BatchStudent,
            include: [{ model: Student }]
        }]
    });
    if(batch) {
        batch = batch.toJSON();
        var student = batch.batch_students[0].student ? batch.batch_students[0].student : batch.batch_students[1].student;
        return await Teacher.findAll({ where: { delivery_channel: student.delivery_channel }});
    } else {
        throw "Batch Selected not found";
    }
}

_getAllB2BBatch = async (request) => {
    const queryParams = Object.keys(request.query).length !== 0 ? true : false;
    let { search, pageSize, pageNumber } = request.query;
    pageSize = pageSize ? pageSize : 100;
    pageNumber = pageNumber ? pageNumber : 1;
    return await Batch.findAndCountAll({ 
        where: { 
          [Op.or]: [
            {
                [Op.or]: [
                    { name: { [Op.notLike]:'%B2C%' }, is_deleted: false }
                ]
            },
        ]
        },
        include: [{ 
            model: BatchStudent,
            where: { is_deleted: false },
            include: [{ 
                model: Student,
                where: {   [Op.or]: [
                    {
                        [Op.or]: [
                            { delivery_channel: { [Op.notLike]:'%B2C%' }, is_deleted: false }
                        ]
                    }
        
                ] },
            }],
            limit: pageSize,
        offset: (pageNumber - 1) * pageSize
        }],
        order: [
            ['createdAt', 'DESC']
        ],
        limit: pageSize,
        offset: (pageNumber - 1) * pageSize
    });
}

_getUniqueStudents = async (req) => {
   
if(req.user)
{
    var students = await Student.findAll({ where: { delivery_channel: req.params.delivery_channel, is_deleted: false }});
    var batchStudents = await BatchStudent.findAll({ where: {  is_deleted: false, course_id:req.params.courseId }});
    let result = students.filter(student => !batchStudents.some(batchStudent => student.id === batchStudent.student_account_id));
    return result
}
else {
    throw "Invalid token, pleas e login again";
}
  
}

module.exports = {
    _getAllBatch,
    _getBatchById,
    _createNewBatch,
    _updateBatchById,
    _deleteBatchById,
    _getStudentsByDeliveryChannel,
    _getTeachersByBatchDeliverChannel,
    _getAllB2BBatch,
    _getUniqueStudents
}