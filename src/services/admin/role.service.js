const { Role } = require('../../models');

_getAllRoles = async () => {
    return await Role.findAndCountAll({});
}

_getRoleById = async (params) => {
    return await Role.findOne({ where: { id: params.id } });
}

_createNewRole = async (data) => {
    return await Role.create(data);
}

_updateRoleById = async (req) => {
    return await Role.update(req.body, { where: { id: req.params.id }, returning: true, plain: true });
}

_deleteRoleById = async (params) => {
    return await Role.update({ is_deleted: true }, { where: { id: params.id }, returning: true, plain: true });
}

module.exports = {
    _getAllRoles,
    _getRoleById,
    _createNewRole,
    _updateRoleById,
    _deleteRoleById
}