import { sign } from "jsonwebtoken";
import { compareSync, hash } from "bcryptjs";
import User from "../config/db.config";
import { config } from "dotenv";
const BCRYPT_SALT_ROUNDS = 12;

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */
config({ path: ".env.dev" });
const secret = process.env.SESSION_SECRET;

module.exports = {
  authenticate,
  getAll,
  getById,
  create,
  update,
  deleteById,
  getAllUserByRole,
  updatePassword
};

async function authenticate({ email, password }) {
  const user = await User.findOne({ email });
  if (user && compareSync(password, user.password)) {
    const { password, ...userWithoutHash } = user.toObject();
    const token = sign({ sub: user.id }, secret);
    return {
      ...userWithoutHash,
      token,
    };
  }
}

async function getAll() {
  return await User.findAll({ where: { active: true } });
}

async function getAllUserByRole(roleParams) {
  var query = { roles: {$elemMatch: {role: roleParams.role} }};
  return await User.find(query).select("-password");
}

async function getById(id) {
  return await User.findById(id).select("-password");
}

async function create(userParam) {
  // validate
  if (await User.findOne({ email: userParam.email })) {
    throw 'email "' + userParam.email + '" is already taken';
  }
 const user = new User(userParam);
// hash password
  if (userParam.password) {
      await hash(userParam.password, BCRYPT_SALT_ROUNDS).then(function(hashedPassword){
       userParam.password = hashedPassword; 
     });
  }
  user.save();
}


async function update(id, userParam) {
  const user = await User.findById(id);

  // validate
  if (!user) throw "User not found";
  if (
    user.email !== userParam.email &&
    (await User.findOne({ email: userParam.email }))
  ) {
    throw 'email "' + userParam.email + '" is already taken';
  }

  // hash password if it was entered
  if (userParam.password) {
    userParam.password = hashSync(userParam.password, 10);
  }

  // copy userParam properties to user
  Object.assign(user, userParam);

  await user.save();
}

async function deleteById(id) {
  return await User.findByIdAndRemove(id);
}

async function updatePassword(id, userParam) {
  const user = await User.findById(id);

  // validate
  if (!user) throw "User not found";
 
  // hash password if it was entered
  if (user && compareSync(userParam.password, user.password)) {
    await hash(userParam.newpassword, BCRYPT_SALT_ROUNDS).then(function(hashedPassword){
        userParam.newpassword = hashedPassword;
        user.password = hashedPassword;
    });
    const userData =  await User.findByIdAndUpdate(id, {$set: user})
    await userData.save();
  }else{
    throw "Incorrect password"
  }

}