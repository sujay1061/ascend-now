const student = require('./student.service');
const teacher = require('./teacher.service');
const account = require('./account.services');
const role = require('./role.service');
const account_active_expiration = require('./account_active_expiration.service');
const course_schedule = require('./course_schedule.service');
const institute = require('./institute.service');
const batch = require('./batch.service');
const batch_student = require('./batch_student.service');
const news_and_event = require('./news_and_event.service');
const class_activity = require('./class_activities.service');
const feedback_question = require('./feedback_question.service');
const quiz_question = require('./quiz_question.service');
const booked_classes = require('./booked_classes');

module.exports = {
  student,
  teacher,
  account,
  role,
  account_active_expiration,
  institute,
  course_schedule,
  institute,
  batch,
  batch_student,
  news_and_event,
  class_activity,
  feedback_question,
  quiz_question,
  booked_classes
};
