const { check, validationResult } = require('express-validator');
const apiResponse = require('../_helpers/api-response');

exports.validateSignupRequest = [
    check('name')
    .notEmpty()
    .withMessage('name is required'),
    check('primary_email')
    .notEmpty(),
    check('secondary_email')
    .notEmpty()
    .withMessage('secondary email is required')
    .isEmail()
    .withMessage('Valid Email is required')
];

exports.validateSignInRequest = [
    check('email')
    .notEmpty()
    .isEmail()
    .withMessage('Valid Email is required'),
    check('password')
    .isLength({ min: 6 })
    .withMessage('Password must be at least 6 character')
];

exports.instituteValidator= [
    check('name').
    isString().
    isLength({ min:4 }).
    withMessage('Name should have minimum 4 character')
];

exports.locationValidator = [
    check('location.address')
    .notEmpty()
    .withMessage('Address is required in location'),
    check('location.city')
    .notEmpty()
    .withMessage('City is required in location'),
    check('location.state')
    .notEmpty()
    .withMessage('State is required in location'),
    check('location.country')
    .notEmpty()
    .withMessage('Country is required in location'),
    check('location.zipcode')
    .notEmpty()
    .withMessage('Zipcode is required in location')
];

exports.quizValidator = [
    check('name')
    .notEmpty()
    .withMessage('Name is required'),
    check('topic_id')
    .notEmpty()
    .withMessage('Topic Id is required')
];

exports.isRequestValidated = (req, res, next) => {
    const errors = validationResult(req);
    if(errors.array().length > 0) {
        return res.status(400).json(
            apiResponse({
              data: [],
              status: "BAD",
              errors: [],
              message: errors.array()[0].msg,
            })
          )
    }
    next();
}

exports.validateTeacherSignupRequest = [
    check('name')
    .notEmpty()
    .withMessage('name is required'),
    check('primary_email')
    .notEmpty()
    .isEmail()
    .withMessage('Valid Email is required')
];
