const { check, validationResult } = require('express-validator');
const apiResponse = require('../_helpers/api-response');

exports.bookClassWithTeacherValidator = [
    check('timeslots')
    .notEmpty()
    .withMessage('Time Slots required'),
    check('timeslots.1')
    .notEmpty()
    .withMessage('Timesot 1 is required'),
    check('timeslots.1.start_date')
    .notEmpty()
    .withMessage('Timesot 1\'s start date is required'),
    check('timeslots.2')
    .notEmpty()
    .withMessage('Timesot 2 is required'),
    check('timeslots.2.start_date')
    .notEmpty()
    .withMessage('Timesot 2\'s start date is required'),
    check('teacher_account_id')
    .notEmpty()
    .withMessage('Teacher is required'),
    check('topic_id')
    .notEmpty()
    .withMessage('Topic is required'),
];

exports.confirmBookedClassValidator = [
    check('confirmed_slot')
    .notEmpty()
    .withMessage('Confirmed Slot is required'),
    check('booked_id')
    .notEmpty()
    .withMessage('Booked Id is required')
];

exports.rejectBookedClassValidator = [
    check('comment')
    .notEmpty()
    .withMessage('Comment is required'),
    check('booked_id')
    .notEmpty()
    .withMessage('Booked Id is required')
];

exports.isRequestValidated = (req, res, next) => {
    const errors = validationResult(req);
    if(errors.array().length > 0) {
        return res.status(400).json(
            apiResponse({
              data: [],
              status: "BAD",
              errors: [],
              message: errors.array()[0].msg,
            })
          )
    }
    next();
}
