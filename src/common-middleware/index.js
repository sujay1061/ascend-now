const apiResponse = require("../_helpers/api-response");
const jwt = require('jsonwebtoken');

exports.requireSignIn = (req, res, next) => {
    if (req.headers.authorization) {
        const token = req.headers.authorization.split(" ")[1];
        const user = jwt.verify(token, process.env.TOKEN_SECRET);
        req.user = user;
        req.token = token
        next();
    } else {
        return res.status(400).json(
            apiResponse({
                data: [],
                status: "BAD",
                errors: [],
                message: 'Authorization required',
            })
        )
    }
}

exports.studentMiddleware = (req, res, next) => {
    if (req.user.role !== 'student') {
        return res.status(400).json(
            apiResponse({
                data: [],
                status: "BAD",
                errors: [],
                message: 'Student Access Denied',
            })
        )
    }
    next();
}

exports.teacherMiddleware = (req, res, next) => {
    if (req.user.role !== 'teacher') {
        return res.status(400).json(
            apiResponse({
                data: [],
                status: "BAD",
                errors: [],
                message: 'Teacher Access Denied',
            })
        )
    }
    next();
}

exports.adminMiddleware = (req, res, next) => {
    if (req.user.role !== 'admin') {
        return res.status(400).json(
            apiResponse({
                data: [],
                status: "BAD",
                errors: [],
                message: 'Admin Access Denied',
            })
        )
    }
    next();
}    
