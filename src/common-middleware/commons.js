exports.getFormattedDateTime = (date) => {
    var localDate = date.toLocaleDateString().split("/");
    var y = localDate.splice(-1)[0];
    localDate.splice(0, 0, y);
    var result = localDate.join("-");
    var localTime = date.toLocaleTimeString().split(' ')[0]
    return `${result} ${localTime}`;
}