const Router = require('express');
const { student_profile } = require('../../controllers/student');

const {
    requireSignIn,
    studentMiddleware
} = require('../../common-middleware');
const upload = require('../../_helpers/aws_s3_file_upload');

const { 
    validateSignupRequest,
    locationValidator,
    isRequestValidated
} = require('../../validators');

const {
    updateStudentAccountById,
    registerStudentForB2C
} = student_profile;

const router = Router();

router.patch('/updateStudentAccount', requireSignIn, studentMiddleware, upload.single('imageUrl'), updateStudentAccountById);

router.post('/registerStudent', validateSignupRequest, locationValidator, isRequestValidated, registerStudentForB2C)

module.exports = router;
