const Router = require('express');
const { student_quiz } = require('../../controllers/student');

const { 
    requireSignIn, 
    studentMiddleware
} = require('../../common-middleware');

const {
    getQuizQuestionByTopicId,
    createQuizAnswer,
    getMyQuizzes,
    getMyCompletedQuizzes,
    getInClassQuizQuestionByTopicId,
    getMyInClassQuiz
} = student_quiz;

const router = Router();

router.get('/getQuizQuestionByTopicId/:id', requireSignIn, studentMiddleware, getQuizQuestionByTopicId);

router.post('/createQuizAnswer', requireSignIn, studentMiddleware, createQuizAnswer);

router.get('/getMyQuizzes', requireSignIn, studentMiddleware, getMyQuizzes);

router.get('/completedQuizzes', requireSignIn, studentMiddleware, getMyCompletedQuizzes);

router.get('/getInClassQuizQuestionByTopicId/:id', requireSignIn, studentMiddleware, getInClassQuizQuestionByTopicId);

router.get('/getMyInClassQuiz', requireSignIn, studentMiddleware, getMyInClassQuiz);

module.exports = router;
