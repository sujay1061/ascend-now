const Router = require('express');
const { student_topic } = require('../../controllers/student');

const { 
    requireSignIn, 
    studentMiddleware
} = require('../../common-middleware');

const {
    getTopicsById,
    getAllTopicsByCourseId
} = student_topic;

const router = Router();

router.get('/getTopicById/:id', requireSignIn, studentMiddleware, getTopicsById);

router.get('/getAllTopicsByCourseId/:id', requireSignIn, studentMiddleware, getAllTopicsByCourseId);

module.exports = router;
