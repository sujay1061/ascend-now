const Router = require('express');
const { student_points } = require('../../controllers/student');

const { 
    requireSignIn, 
    studentMiddleware
} = require('../../common-middleware');

const {
    getMyPointsHistory
} = student_points;

const router = Router();

router.get('/getMyPointsHistory', requireSignIn, studentMiddleware, getMyPointsHistory);

module.exports = router;
