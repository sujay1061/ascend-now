const Router = require('express');
const { student_booked_classes } = require('../../controllers/student');

const { 
    requireSignIn, 
    studentMiddleware
} = require('../../common-middleware');
const { 
    bookClassWithTeacherValidator, 
    isRequestValidated 
} = require('../../validators/booked_class_validators');

const {
    getMyBookedClasses,
    bookClassWithTeacher,
    getMyConfirmedClasses,
    getBookedClassesByDate,
    fetchMyUpcomingBookedClasses,
    joinMyBookedClass,
    getAllBookedClass,
    getLatestBookings,
    getPastBookings
} = student_booked_classes;

const router = Router();

router.get('/getMyBookedClasses', requireSignIn, studentMiddleware, getMyBookedClasses);

router.post('/bookClassWithTeacher', requireSignIn, studentMiddleware, bookClassWithTeacherValidator, isRequestValidated, bookClassWithTeacher);

router.get('/getMyConfirmedClasses', requireSignIn, studentMiddleware, getMyConfirmedClasses);

router.get('/getBookedClassesByDate', requireSignIn, studentMiddleware, getBookedClassesByDate);

router.get('/fetchMyUpcomingBookedClasses', requireSignIn, studentMiddleware, fetchMyUpcomingBookedClasses);

//id here refers to booked_id
router.get('/joinMyBookedClass/:id', requireSignIn, studentMiddleware, joinMyBookedClass);

router.get('/getAllBookedClass', requireSignIn, studentMiddleware, getAllBookedClass);

router.get('/getLatestBookings', requireSignIn, studentMiddleware, getLatestBookings);

router.get('/getPastBookings', requireSignIn, studentMiddleware, getPastBookings);

module.exports = router;
