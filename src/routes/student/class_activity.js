const Router = require('express');
const { student_activity } = require('../../controllers/student');

const { 
    requireSignIn, 
    studentMiddleware
} = require('../../common-middleware');

const {
    getAllActivitiesByTopicId
} = student_activity;

const router = Router();

// Start Class
router.get('/getAllActivitiesByTopicId/:id', requireSignIn, studentMiddleware, getAllActivitiesByTopicId);

module.exports = router;
