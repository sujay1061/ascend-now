const Router = require('express');
const { student_course_scheduled } = require('../../controllers/student');

const { 
    requireSignIn, 
    studentMiddleware
} = require('../../common-middleware');

const upload = require('../../_helpers/aws_s3_file_upload');

const {
    uploadopicAssignmentReport,
    monthlyCalendar,
    getAllCourseSchedules,
    getMyAssignments,
    getAllSubmittedAssignments,
    joinClass,
    nextClass,
    myClassesScheduledForTheDate,
    getLatestScheduleds,
    getPastScheduleds
} = student_course_scheduled;

const router = Router();

router.patch('/uploadTopicAssignmentReport/:topicId', requireSignIn, studentMiddleware, upload.single('url'), uploadopicAssignmentReport);

router.get('/monthlyCalendar', requireSignIn, studentMiddleware, monthlyCalendar);

router.get('/getAllCourseSchedules', requireSignIn, studentMiddleware, getAllCourseSchedules);

router.get('/getMyAssignments', requireSignIn, studentMiddleware, getMyAssignments);

router.get('/getAllSubmittedAssignments', requireSignIn, studentMiddleware, getAllSubmittedAssignments);

router.post('/joinClass', requireSignIn, studentMiddleware, joinClass);

router.get('/nextClass', requireSignIn, studentMiddleware, nextClass);

router.get('/myClassesScheduledForTheDate', requireSignIn, studentMiddleware, myClassesScheduledForTheDate);

router.get('/getLatestScheduleds', requireSignIn, studentMiddleware, getLatestScheduleds);

router.get('/getPastScheduleds', requireSignIn, studentMiddleware, getPastScheduleds);



module.exports = router;
