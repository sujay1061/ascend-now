const Router = require('express');
const { student_teacher } = require('../../controllers/student');

const {
    requireSignIn,
    studentMiddleware
} = require('../../common-middleware');

const {
    getAllTeachersByDeliveryChannel
} = student_teacher;

const router = Router();

router.get('/getAllTeachersByDeliveryChannel/:channel', requireSignIn, studentMiddleware, getAllTeachersByDeliveryChannel);

module.exports = router;
