const Router = require('express');
const { account_active_expiration } = require('../../controllers/admin');

const { 
    requireSignIn, 
    adminMiddleware 
} = require('../../common-middleware');

const {
    getAllAccountSessions,
    getAllActiveSessions
} = account_active_expiration;

const router = Router();

router.get('/getAllSessions', getAllAccountSessions);

router.get('/getActiveSessions', getAllActiveSessions);

module.exports = router;
