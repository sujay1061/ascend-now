const Router = require('express');
const { topic } = require('../../controllers/admin');

const {
    requireSignIn,
    adminMiddleware
} = require('../../common-middleware');
const upload = require('../../_helpers/aws_s3_file_upload');

const {
    getAllTopics,
    getTopicById,
    createNewTopic,
    updateTopicById,
    deleteTopicById,
    getAllTopicsByCourseId,
    downloadMaterial,
    getSignedUrl
} = topic;

const router = Router();

router.get('/getAllTopics', requireSignIn, adminMiddleware, getAllTopics);

router.get('/getTopicById/:id', requireSignIn, adminMiddleware, getTopicById);

router.post('/createNewtopic', requireSignIn, adminMiddleware, upload.fields([
    { name: 'topic_attachment', maxCount: 1 }, { name: 'topic_script', maxCount: 1 }]), createNewTopic);

router.patch('/updateTopicById/:id', requireSignIn, adminMiddleware, upload.fields([
    { name: 'topic_attachment', maxCount: 1 }, { name: 'topic_script', maxCount: 1 }]), updateTopicById);

router.delete('/deleteTopicById/:id', requireSignIn, adminMiddleware, deleteTopicById);

router.get('/getAllTopicsByCourseId/:id', requireSignIn, adminMiddleware, getAllTopicsByCourseId);

router.get('/downloadMaterial', requireSignIn, adminMiddleware, downloadMaterial);

router.post('/getSignedUrl', requireSignIn, getSignedUrl);

module.exports = router;
