const Router = require('express');
const { account } = require('../../controllers/admin');

const { 
    requireSignIn, 
    adminMiddleware 
} = require('../../common-middleware');
const { validateSignInRequest, validateSignupRequest, isRequestValidated } = require('../../validators');

const {
    loginToAccount,
    createNewAccount,
    logoutFromAccount,
    resetPassword,
    forgotPassword,
    forgotResetPassword,
    refreshToken,
    TestloginToAccount
} = account;

const router = Router();

router.post('/login', validateSignInRequest, isRequestValidated, loginToAccount);

router.post('/logout', requireSignIn, logoutFromAccount);

router.post('/register', createNewAccount);

router.post('/resetPassword',  requireSignIn, resetPassword);

router.post('/forgotPassword' , forgotPassword);

router.post('/forgotResetPassword' , requireSignIn ,forgotResetPassword);

router.post('/refreshToken', requireSignIn, refreshToken);

router.post('/Testlogin', validateSignInRequest, isRequestValidated, TestloginToAccount);

module.exports = router;
