const Router = require('express');
const { feedback_question } = require('../../controllers/admin');

const { 
    requireSignIn, 
    adminMiddleware 
} = require('../../common-middleware');

const {
    getAllfeedbackQuestion,
    createNewFeedBackQuestion,
    getFeedBackQuestionById,
    updateFeedBackQuestionById,
    deleteFeedBackQuestionById
} = feedback_question;

const router = Router();

router.get('/getAllfeedbackQuestion', requireSignIn, adminMiddleware, getAllfeedbackQuestion);

router.post('/createNewFeedBackQuestion', requireSignIn, adminMiddleware, createNewFeedBackQuestion);

router.get('/getFeedBackQuestionById/:id', requireSignIn, adminMiddleware, getFeedBackQuestionById);

router.patch('/updateFeedBackQuestionById/:id', requireSignIn, adminMiddleware, updateFeedBackQuestionById);

router.delete('/deleteFeedBackQuestionById/:id', requireSignIn, adminMiddleware, deleteFeedBackQuestionById);




module.exports = router;
