const Router = require('express');
const { teacher } = require('../../controllers/admin');

const {
    requireSignIn,
    adminMiddleware
} = require('../../common-middleware');
const { validateSignupRequest, isRequestValidated, locationValidator, validateTeacherSignupRequest } = require('../../validators');

const {
    getAllTeacherAccounts,
    getTeacherAccountById,
    createNewTeacherAccount,
    updateTeacherAccountById,
    deleteTeacherAccountById,
    getAllTeachersByDeliveryChannel,
    getAllInActiveTeachers,
    activateTeacherAccountById
} = teacher;

const router = Router();

router.get('/getAllTeachers', requireSignIn, adminMiddleware, getAllTeacherAccounts);

router.get('/getTeacherById/:id', requireSignIn, adminMiddleware, getTeacherAccountById);

router.post('/createTeacher', requireSignIn, adminMiddleware, validateTeacherSignupRequest, locationValidator, isRequestValidated, createNewTeacherAccount);

router.patch('/updateTeacherById/:id', requireSignIn, adminMiddleware, validateTeacherSignupRequest, locationValidator, isRequestValidated, updateTeacherAccountById);

router.delete('/deleteTeacherById/:id', requireSignIn, adminMiddleware, deleteTeacherAccountById);

router.get('/getAllTeachersByDeliveryChannel/:channel', requireSignIn, adminMiddleware, getAllTeachersByDeliveryChannel);

router.get('/getAllInActiveTeachers', requireSignIn, adminMiddleware, getAllInActiveTeachers);

router.patch('/activateTeacherAccountById/:id', requireSignIn, adminMiddleware, activateTeacherAccountById);


module.exports = router;
