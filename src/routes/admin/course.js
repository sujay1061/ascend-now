const Router = require('express');
const { course } = require('../../controllers/admin');

const { 
    requireSignIn, 
    adminMiddleware 
} = require('../../common-middleware');

const {
    getAllCourses,
    getCourseById,
    createNewCourse,
    updateCourseById,
    deleteCourseById
} = course;

const router = Router();

router.get('/getAllCourses', requireSignIn, adminMiddleware, getAllCourses);

router.get('/getCourseById/:id', requireSignIn, adminMiddleware, getCourseById);

router.post('/createCourse', requireSignIn, adminMiddleware, createNewCourse);

router.patch('/updateCourseById/:id', requireSignIn, adminMiddleware, updateCourseById);

router.delete('/deleteCourseById/:id', requireSignIn, adminMiddleware, deleteCourseById);

module.exports = router;
