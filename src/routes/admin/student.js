const Router = require('express');
const { student } = require('../../controllers/admin');

const {
    requireSignIn,
    adminMiddleware
} = require('../../common-middleware');
const { validateSignupRequest, isRequestValidated, locationValidator } = require('../../validators');

const {
    getAllStudentAccounts,
    getStudentAccountById,
    createNewStudentAccount,
    updateStudentAccountById,
    deleteStudentAccountById,
    getAllStudentBatches,
    getAllInActiveStudents,
    activateStudentAccountById
} = student;

const router = Router();

router.get('/getAllStudents', requireSignIn, adminMiddleware, getAllStudentAccounts);

router.get('/getStudentById/:id', requireSignIn, adminMiddleware, getStudentAccountById);

router.post('/createStudent', requireSignIn, adminMiddleware, validateSignupRequest, locationValidator, isRequestValidated, createNewStudentAccount);

router.patch('/updateStudentById/:id', requireSignIn, validateSignupRequest,adminMiddleware, updateStudentAccountById);

router.delete('/deleteStudentById/:id', requireSignIn, adminMiddleware, deleteStudentAccountById);

router.get('/getAllStudentBatches/:studentId', requireSignIn, adminMiddleware, getAllStudentBatches);

router.get('/getAllInActiveStudents', requireSignIn, adminMiddleware, getAllInActiveStudents);

router.patch('/activateStudentAccountById/:id', requireSignIn, adminMiddleware, activateStudentAccountById);

module.exports = router;
