const student = require('./student');
const teacher = require('./teacher');
const account = require('./account');
const role = require('./role');
const institute = require('./institute');
const course = require('./course');
const topic = require('./topic');
const topic_assignment = require('./topic_assignment');
const account_active_expiration = require('./account_active_expiration');
const course_schedule = require('./course_schedule');
const batch_student = require('./batch_student');
const batch = require('./batch');
const news_and_event = require('./news_and_event');
const class_activity = require('./class_activities');
const feedback_question = require('./feedback_question');
const quiz_question = require('./quiz_question');
const booked_classes = require('./booked_classes');

module.exports = {
  student,
  teacher,
  account,
  role,
  institute,
  course,
  topic,
  topic_assignment,
  account_active_expiration,
  course_schedule,
  batch_student,
  batch,
  news_and_event,
  class_activity,
  feedback_question,
  quiz_question,
  booked_classes
};
