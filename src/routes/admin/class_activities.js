const Router = require('express');
const { class_activity } = require('../../controllers/admin');

const { 
    requireSignIn, 
    adminMiddleware
} = require('../../common-middleware');

const {
    getAllClassActivities,
    getClassActivityById,
    createNewClassActivity,
    updateClassActivityById,
    deleteClassActivityById,
} = class_activity;

const router = Router();

router.get('/getAllClassActivities', requireSignIn, adminMiddleware, getAllClassActivities);

router.post('/createNewClassActivity', requireSignIn, adminMiddleware, createNewClassActivity);

router.get('/getClassActivityById/:id', requireSignIn, adminMiddleware, getClassActivityById);

router.patch('/updateClassActivityById/:id', requireSignIn, adminMiddleware, updateClassActivityById);

router.delete('/deleteClassActivityById/:id', requireSignIn, adminMiddleware, deleteClassActivityById);

module.exports = router;
