const Router = require('express');
const { batch_student } = require('../../controllers/admin');

const {
    requireSignIn,
    adminMiddleware
} = require('../../common-middleware');

const {
    getAllBatchStudents,
    getBatchStudentById,
    createNewBatchStudent,
    updateBatchStudentById,
    deleteBatchStudentById
} = batch_student;

const router = Router();

router.get('/getAllBatchStudents', requireSignIn, adminMiddleware, getAllBatchStudents);

router.get('/getBatchStudentById/:id', requireSignIn, adminMiddleware, getBatchStudentById);

router.post('/createNewBatchStudent',requireSignIn, adminMiddleware, createNewBatchStudent);

router.patch('/updateBatchStudentById/:id',requireSignIn, adminMiddleware, updateBatchStudentById);

router.delete('/deleteBatchStudentById/:id',  requireSignIn, adminMiddleware, deleteBatchStudentById);

module.exports = router;