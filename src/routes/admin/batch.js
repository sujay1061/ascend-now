const Router = require('express');
const { batch } = require('../../controllers/admin');

const {
    requireSignIn,
    adminMiddleware
} = require('../../common-middleware');
const { locationValidator, isRequestValidated } = require('../../validators');

const {
    getAllBatch,
    getBatchById,
    createNewBatch,
    updateBatchById,
    deleteBatchById,
    getStudentsByDeliveryChannel,
    getTeachersByBatchDeliverChannel,
    getAllB2BBatch,
    getUniqueStudents
} = batch;

const router = Router();

router.get('/getAllBatch', requireSignIn, adminMiddleware,getAllBatch);

router.get('/getBatchById/:id', requireSignIn, adminMiddleware,getBatchById);

router.post('/createBatch', requireSignIn, adminMiddleware, createNewBatch);

router.patch('/updateBatchById/:id',requireSignIn, adminMiddleware, updateBatchById);

router.delete('/deleteBatchById/:id',requireSignIn, adminMiddleware, deleteBatchById);

router.get('/getStudentsByDeliveryChannel/:channel', requireSignIn, adminMiddleware,getStudentsByDeliveryChannel);

router.get('/getTeachersByBatchDeliverChannel/:id', requireSignIn, adminMiddleware, getTeachersByBatchDeliverChannel);

router.get('/getAllB2BBatch', requireSignIn, adminMiddleware, getAllB2BBatch);

router.get('/getUniqueB2BStudents/:courseId/:delivery_channel', requireSignIn, adminMiddleware, getUniqueStudents);

module.exports = router;
