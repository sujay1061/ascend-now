const Router = require('express');
const { course_schedule } = require('../../controllers/admin');

const { 
    requireSignIn, 
    adminMiddleware ,
    teacherMiddleware
} = require('../../common-middleware');
 


const {
    getAllCourseSchedules,
    getCourseScheduleById,
    createNewCourseSchedule,
    updateCourseScheduleById,
    deleteCourseScheduleById,
    updateCourseCompletedById,
    getTeacherScheduleForAWeek,
    monthlyCalendar
} = course_schedule;

const router = Router();

router.get('/getAllCourseSchedules', requireSignIn, adminMiddleware, getAllCourseSchedules);

router.post('/createCourseSchedule', requireSignIn, adminMiddleware, createNewCourseSchedule);

router.get('/getCourseScheduleById/:id', requireSignIn, adminMiddleware, getCourseScheduleById);

router.get('/getTeacherScheduleForAWeek', requireSignIn, teacherMiddleware, getTeacherScheduleForAWeek);

router.patch('/updateCourseSchedule/:id', requireSignIn, adminMiddleware, updateCourseScheduleById);

router.patch('/updateCourseCompletedById/:id', requireSignIn, adminMiddleware, updateCourseCompletedById);

router.get('/monthlyCalendar', requireSignIn, adminMiddleware, monthlyCalendar);

router.delete('/deleteCourseSchedule/:id', requireSignIn, adminMiddleware, deleteCourseScheduleById);

module.exports = router;
