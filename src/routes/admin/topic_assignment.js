const Router = require('express');
const { topic_assignment } = require('../../controllers/admin');

const { 
    requireSignIn, 
    adminMiddleware 
} = require('../../common-middleware');
const upload = require('../../_helpers/aws_s3_file_upload');

const {
    getAllTopicAssignment,
    getTopicAssignmentById,
    createNewTopicAssignment,
    updateTopicAssignmentById,
    deleteTopicAssignmentById
} = topic_assignment;

const router = Router();

router.get('/getAllTopicAssignment', requireSignIn, adminMiddleware, getAllTopicAssignment);

router.get('/getTopicAssignmentById/:id', requireSignIn, adminMiddleware, getTopicAssignmentById);

router.post('/createTopicAssignment', requireSignIn, adminMiddleware, upload.single('url'), createNewTopicAssignment);

router.patch('/updateTopicAssignmentById/:id', requireSignIn, adminMiddleware, upload.single('url'), updateTopicAssignmentById);

router.delete('/deleteTopicAssignmentById/:id', requireSignIn, adminMiddleware, deleteTopicAssignmentById);

module.exports = router;
