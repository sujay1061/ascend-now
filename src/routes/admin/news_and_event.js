const Router = require('express');
const { news_and_event } = require('../../controllers/admin');

const {
    getAllNewsAndEvents, 
    getNewsAndEventById, 
    createNewNewsAndEvent, 
    updateNewsAndEventById, 
    deleteNewsAndEventById,
    getNewsAndEventsForAWeek
} = news_and_event;

const router = Router();

router.get('/getAllNewsAndEvents', getAllNewsAndEvents);

router.get('/getNewsAndEventById/:id', getNewsAndEventById);

router.get('/getNewsAndEventsForAWeek', getNewsAndEventsForAWeek);

router.post('/createNewsAndEvent', createNewNewsAndEvent);

router.patch('/updateNewsAndEventById/:id', updateNewsAndEventById);

router.delete('/deleteNewsAndEventById/:id', deleteNewsAndEventById);

module.exports = router;
