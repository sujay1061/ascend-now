const Router = require('express');
const { requireSignIn, adminMiddleware } = require('../../common-middleware');
const { role } = require('../../controllers/admin');

const {
    getAllRoles, 
    getRoleById, 
    createNewRole, 
    updateRoleById, 
    deleteRoleById
} = role;

const router = Router();

router.get('/getAllRoles', requireSignIn, adminMiddleware, getAllRoles);

router.get('/getRoleById/:id', requireSignIn, adminMiddleware, getRoleById);

router.post('/createRole', createNewRole);

router.patch('/updateRoleById/:id', requireSignIn, adminMiddleware, updateRoleById);

router.delete('/deleteRoleById/:id', requireSignIn, adminMiddleware, deleteRoleById);

module.exports = router;
