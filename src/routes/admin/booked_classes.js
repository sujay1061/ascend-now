const Router = require('express');
const { booked_classes } = require('../../controllers/admin');

const { 
    requireSignIn,
    adminMiddleware
} = require('../../common-middleware');

const {
    bookClassWithStudentTeacher,
    cancelClassWithStudentTeacher,
    getAllTeachersByDeliveryChannel,
    getAllStudentsByDeliveryChannel,
    getAllSchedules
} = booked_classes;

const router = Router();

router.post('/bookClass', requireSignIn, adminMiddleware, bookClassWithStudentTeacher);

router.patch('/cancelClass/:id', requireSignIn, adminMiddleware, cancelClassWithStudentTeacher);

router.get('/getAllTeachersByDeliveryChannel/:channel', requireSignIn, adminMiddleware, getAllTeachersByDeliveryChannel);

router.get('/getAllStudentsByDeliveryChannel/:channel', requireSignIn, adminMiddleware, getAllStudentsByDeliveryChannel);

router.get('/getAllSchedules', requireSignIn, adminMiddleware, getAllSchedules);

module.exports = router;
