const Router = require('express');
const { requireSignIn, adminMiddleware } = require('../../common-middleware');
const { quiz_question } = require('../../controllers/admin');
const { quizValidator, isRequestValidated } = require('../../validators');
const upload = require('../../_helpers/aws_s3_file_upload');

const {
    getAllQuizQuestions, 
    getQuizQuestionById,
    getQuizQuestionByTopicId,
    getAllQuizes,
    createQuiz,
    updateQuizById,
    createQuizQuestion, 
    updateQuizQuestionById, 
    deleteQuizQuestionById,
    getAllQuizQuestionByQuizId,
    publishQuiz
} = quiz_question;

const router = Router();

router.get('/getAllQuizQuestions', requireSignIn, adminMiddleware, getAllQuizQuestions);

router.get('/getQuizQuestionById/:id', requireSignIn, adminMiddleware, getQuizQuestionById);

router.get('/getQuizQuestionByTopicId/:topicId', requireSignIn, adminMiddleware, getQuizQuestionByTopicId);

router.get('/getAllQuizes', requireSignIn, adminMiddleware, getAllQuizes);

router.post('/createQuiz', requireSignIn, adminMiddleware, quizValidator, isRequestValidated, createQuiz);

router.patch('/updateQuizById/:id', requireSignIn, adminMiddleware, updateQuizById);

router.patch('/publishQuiz/:id', requireSignIn, adminMiddleware, publishQuiz);

router.post('/createQuizQuestion', requireSignIn, adminMiddleware, upload.single('url'), createQuizQuestion);

router.patch('/updateQuizQuestionById/:id', requireSignIn, adminMiddleware, upload.single('url'), updateQuizQuestionById);

router.delete('/deleteQuizQuestionById/:id', requireSignIn, adminMiddleware, deleteQuizQuestionById);

router.get('/getAllQuizQuestionByQuizId/:id', requireSignIn, adminMiddleware, getAllQuizQuestionByQuizId);

module.exports = router;
