const Router = require('express');
const { institute } = require('../../controllers/admin');

const { 
    requireSignIn, 
    adminMiddleware
} = require('../../common-middleware');

const { instituteValidator, isRequestValidated, locationValidator } = require('../../validators');

const {
    getAllInstitutes,
    getInstituteById,
    createNewInstitute,
    updateInstituteById,
    deleteInstituteById
} = institute;

const router = Router();

router.get('/getAllInstitutes', requireSignIn, adminMiddleware, getAllInstitutes);

router.post('/createInstitute',requireSignIn, adminMiddleware, instituteValidator, locationValidator, isRequestValidated, createNewInstitute);

router.get('/getInstituteById/:id', requireSignIn, adminMiddleware, getInstituteById);

router.patch('/updateInstituteById/:id', requireSignIn, adminMiddleware, instituteValidator, locationValidator, isRequestValidated, updateInstituteById);

router.delete('/deleteInstituteById/:id', requireSignIn, adminMiddleware, deleteInstituteById);

module.exports = router;
