const Router = require('express');
const { teacher_topic } = require('../../controllers/teacher');

const { 
    requireSignIn, 
    teacherMiddleware
} = require('../../common-middleware');

const {
    getTopicsById,
    getAllTopicsByCourseId
} = teacher_topic;

const router = Router();

router.get('/getTopicById/:id', requireSignIn, teacherMiddleware, getTopicsById);

router.get('/getAllTopicsByCourseId/:id', requireSignIn, teacherMiddleware, getAllTopicsByCourseId);

module.exports = router;
