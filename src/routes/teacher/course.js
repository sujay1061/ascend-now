const Router = require('express');
const { teacher_course } = require('../../controllers/teacher');

const {
    requireSignIn,
    teacherMiddleware
} = require('../../common-middleware');

const {
    getAllCourses
} = teacher_course;

const router = Router();

router.get('/getAllCourses', requireSignIn, teacherMiddleware, getAllCourses);

module.exports = router;
