const Router = require('express');
const { teacher_profile } = require('../../controllers/teacher');

const {
    requireSignIn,
    teacherMiddleware
} = require('../../common-middleware');
const upload = require('../../_helpers/aws_s3_file_upload');

const {
    updateTeacherAccountById
} = teacher_profile;

const router = Router();

router.patch('/updateTeacherAccount', requireSignIn, teacherMiddleware, upload.single('imageUrl'), updateTeacherAccountById);

module.exports = router;
