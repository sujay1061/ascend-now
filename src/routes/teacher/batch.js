const Router = require('express');
const { teacher_batch } = require('../../controllers/teacher');

const { 
    requireSignIn, 
    teacherMiddleware
} = require('../../common-middleware');

const {
    getAllStudentsByBatchId,
} = teacher_batch;

const router = Router();

router.get('/getAllStudentsByBatchId/:id', requireSignIn, teacherMiddleware, getAllStudentsByBatchId);

module.exports = router;
