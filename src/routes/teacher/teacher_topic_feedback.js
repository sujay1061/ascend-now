const Router = require('express');
const { teacher_topic_feedback } = require('../../controllers/teacher');

const { 
    requireSignIn, 
    teacherMiddleware
} = require('../../common-middleware');

const {
    getAllTopicFeedbacks,
    createTopicFeedback,
    getTeacherTopicFeedbackByStudentCredentials
} = teacher_topic_feedback;

const router = Router();

router.get('/getAllTopicFeedbacks', requireSignIn, teacherMiddleware, getAllTopicFeedbacks);

router.post('/createTopicFeedback', requireSignIn, teacherMiddleware, createTopicFeedback);

router.get('/getTeacherTopicFeedbackByStudentCredentials/:studentPassword', requireSignIn ,getTeacherTopicFeedbackByStudentCredentials);

module.exports = router;
