const Router = require('express');
const { teacher_student } = require('../../controllers/teacher');

const {
    requireSignIn,
    teacherMiddleware
} = require('../../common-middleware');

const {
    getStudentsByDeliveryChannel
} = teacher_student;

const router = Router();

router.get('/getStudentsByDeliveryChannel/:channel', requireSignIn, teacherMiddleware, getStudentsByDeliveryChannel);

module.exports = router;
