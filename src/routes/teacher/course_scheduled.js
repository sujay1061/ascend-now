const Router = require('express');
const { teacher_course_scheduled } = require('../../controllers/teacher');

const { 
    requireSignIn, 
    teacherMiddleware
} = require('../../common-middleware');

const {
    finishScheduledClass,
    fetchNextScheduledClass,
    getMyStudentsInformation,
    MyStudentsDetailsById,
    monthlyCalendar,
    getTeacherTotalClasses,
    startClass,
    myCompletedClasses,
    myScheduledClassForTheDate,
    enableQuiz,
    getInClassQuizQuestionByTopicId,
    getAllCourseSchedules,
    getLatestScheduleds,
    getPastScheduleds
} = teacher_course_scheduled;

const router = Router();

// Start Class
router.get('/nextClass', requireSignIn, teacherMiddleware, fetchNextScheduledClass);

// course_scheduled_id as id FINISH CLASS
router.patch('/finish/:id', requireSignIn, teacherMiddleware, finishScheduledClass);

router.get('/myStudents', requireSignIn, teacherMiddleware, getMyStudentsInformation);

router.get('/myStudentsDetailsById/:id', requireSignIn, teacherMiddleware, MyStudentsDetailsById);

router.get('/monthlyCalendar', requireSignIn, teacherMiddleware, monthlyCalendar);

router.get('/getTeacherTotalClasses', requireSignIn, teacherMiddleware, getTeacherTotalClasses);

router.post('/startClass', requireSignIn, teacherMiddleware, startClass);

router.get('/myCompletedClasses', requireSignIn, teacherMiddleware, myCompletedClasses);

router.get('/myScheduledClassForTheDate', requireSignIn, teacherMiddleware, myScheduledClassForTheDate);

router.post('/enableQuiz', requireSignIn, teacherMiddleware, enableQuiz);

router.get('/getInClassQuizQuestionByTopicId/:id', requireSignIn, teacherMiddleware, getInClassQuizQuestionByTopicId);

router.get('/getAllCourseSchedules', requireSignIn, teacherMiddleware, getAllCourseSchedules);

router.get('/getLatestScheduleds', requireSignIn, teacherMiddleware, getLatestScheduleds);

router.get('/getPastScheduleds', requireSignIn, teacherMiddleware, getPastScheduleds);

module.exports = router;
