const Router = require('express');
const { teacher_student_point } = require('../../controllers/teacher');

const { 
    requireSignIn, 
    teacherMiddleware
} = require('../../common-middleware');

const {
    createNewPoints,
} = teacher_student_point;

const router = Router();

router.post('/createNewPoints', requireSignIn, teacherMiddleware, createNewPoints);

module.exports = router;
