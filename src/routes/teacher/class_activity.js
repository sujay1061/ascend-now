const Router = require('express');
const { teacher_class_activities } = require('../../controllers/teacher');

const { 
    requireSignIn, 
    teacherMiddleware
} = require('../../common-middleware');

const {
    getAllActivitiesByTopicId
} = teacher_class_activities;

const router = Router();

// Start Class
router.get('/getAllActivitiesByTopicId/:id', requireSignIn, teacherMiddleware, getAllActivitiesByTopicId);

module.exports = router;
