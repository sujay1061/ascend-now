const Router = require('express');
const { teacher_class_attendance } = require('../../controllers/teacher');

const { 
    requireSignIn, 
    teacherMiddleware
} = require('../../common-middleware');

const {
    createNewClassAttendance,
    addMarksToStudentByTopic,
    getAllfeedbackQuestion,
    addStudentFeedBack,
    addAssignmentFeedback
} = teacher_class_attendance;

const router = Router();

router.post('/createAttendance', requireSignIn, teacherMiddleware, createNewClassAttendance);

router.post('/addMarksToStudentByTopic', requireSignIn, teacherMiddleware, addMarksToStudentByTopic);

router.get('/getAllfeedbackQuestion', requireSignIn, teacherMiddleware, getAllfeedbackQuestion);

router.post('/addStudentFeedBack', requireSignIn, teacherMiddleware, addStudentFeedBack);

router.post('/addAssignmentFeedback', requireSignIn, teacherMiddleware, addAssignmentFeedback);

module.exports = router;
