const Router = require('express');
const { teacher_booked_classes } = require('../../controllers/teacher');

const { 
    requireSignIn, 
    teacherMiddleware
} = require('../../common-middleware');
const { 
    confirmBookedClassValidator, 
    isRequestValidated,
    rejectBookedClassValidator
} = require('../../validators/booked_class_validators');

const {
    getMyBookedClasses,
    confirmBookedClass,
    rejectBookedClass,
    getMyConfirmedClasses,
    bookClassWithStudent,
    getBookedClassesByDate,
    fetchNextBookedClass,
    startMyBookedClass,
    finishBookedClass,
    getAllBookedClasses,
    getLatestBookings,
    getPastBookings
} = teacher_booked_classes;

const router = Router();

router.get('/getMyBookedClasses', requireSignIn, teacherMiddleware, getMyBookedClasses);

router.post('/confirmBookedClass', requireSignIn, teacherMiddleware, confirmBookedClassValidator, isRequestValidated, confirmBookedClass);

router.post('/rejectBookedClass', requireSignIn, teacherMiddleware, rejectBookedClassValidator, isRequestValidated, rejectBookedClass);

router.get('/getMyConfirmedClasses', requireSignIn, teacherMiddleware, getMyConfirmedClasses);

router.post('/bookClassWithStudent', requireSignIn, teacherMiddleware, bookClassWithStudent);

router.get('/getBookedClassesByDate', requireSignIn, teacherMiddleware, getBookedClassesByDate);

router.get('/fetchNextBookedClass', requireSignIn, teacherMiddleware, fetchNextBookedClass);

router.get('/startMyBookedClasss/:id', requireSignIn, teacherMiddleware, startMyBookedClass);

//id here refers to booked_id
router.patch('/finishBookedClass/:id', requireSignIn, teacherMiddleware, finishBookedClass);

router.get('/getAllBookedClasses', requireSignIn, teacherMiddleware, getAllBookedClasses);

router.get('/getLatestBookings', requireSignIn, teacherMiddleware, getLatestBookings);

router.get('/getPastBookings', requireSignIn, teacherMiddleware, getPastBookings);

module.exports = router;
