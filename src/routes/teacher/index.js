const teacher_class_attendance = require('./class_attendance');
const teacher_course_scheduled = require('./course_scheduled');
const teacher_batch = require('./batch');
const teacher_topic = require('./topic');
const teacher_profile = require('./teacher');
const teacher_class_activities = require('./class_activity');
const teacher_topic_feedback = require('./teacher_topic_feedback');
const teacher_student_point = require('./student_point');
const teacher_booked_classes = require('./booked_classes');
const teacher_student = require('./student');
const teacher_course = require('./course');

module.exports = {
  teacher_class_attendance,
  teacher_course_scheduled,
  teacher_batch,
  teacher_topic,
  teacher_profile,
  teacher_class_activities,
  teacher_topic_feedback,
  teacher_student_point,
  teacher_booked_classes,
  teacher_student,
  teacher_course
};
